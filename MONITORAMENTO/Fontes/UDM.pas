{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UDM.pas
  Descricao:   Data Module Principal
  Author   :   Cristina
  Date:        11-out-2016
  Last Update: 27-fev-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UDM;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Data.DB, Datasnap.DBClient,
  Datasnap.Provider, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Dialogs,
  JvBaseDlg, JvProgressDialog, FireDAC.Comp.UI, FireDAC.Phys.IBBase, IniFiles,
  Vcl.Forms, Winapi.Windows, Winapi.Messages, System.Generics.Collections,
  System.StrUtils, System.DateUtils, System.Variants, Winapi.TlHelp32;

type
  { LANCAMENTO }
  TDadosClasseLancamento = class(TObject)
  public
    CodLancamento:     Integer;
    AnoLancamento:     Integer;
    DataLancamento:    TDate;
    TipoLancamento:    String;
    TipoCadastro:      String;
    IdCategoria:       Integer;
    DescrCategoria:    String;
    IdSubCategoria:    Integer;
    DescrSubCategoria: String;
    IdNatureza:        Integer;
    DescrNatureza:     String;
    IdCliFor:          Integer;
    NomeCliFor:        String;
    FlgIR:             String;
    FlgPessoal:        String;
    FlgAuxiliar:       String;
    FlgReal:           String;
    FlgFlutuante:      String;
    FlgForaFechCaixa:  String;
    QtdParcelas:       Integer;
    VlrTotalPrev:      Currency;
    VlrTotalJuros:     Currency;
    VlrTotalDesconto:  Currency;
    VlrTotalPago:      Currency;
    CadIdUsuario:      Integer;
    DataCadastro:      TDate;
    FlgStatus:         String;
    FlgCancelado:      String;
    DataCancelamento:  TDate;
    CancelIdUsuario:   Integer;
    FlgRecorrente:     String;
    FlgReplicado:      String;
    NumRecibo:         Integer;
    IdOrigem:          Integer;
    IdSistema:         Integer;
    NomeSistema:       String;
    Observacao:        String;
    IdEquivalencia:    Integer;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseLancamento = class(TDadosClasseLancamento)
  public
    CodLancamento:     Integer;
    AnoLancamento:     Integer;
    DataLancamento:    TDate;
    TipoLancamento:    String;
    TipoCadastro:      String;
    IdCategoria:       Integer;
    DescrCategoria:    String;
    IdSubCategoria:    Integer;
    DescrSubCategoria: String;
    IdNatureza:        Integer;
    DescrNatureza:     String;
    IdCliFor:          Integer;
    NomeCliFor:        String;
    FlgIR:             String;
    FlgPessoal:        String;
    FlgAuxiliar:       String;
    FlgForaFechCaixa:  String;
    FlgReal:           String;
    FlgFlutuante:      String;
    QtdParcelas:       Integer;
    VlrTotalPrev:      Currency;
    VlrTotalJuros:     Currency;
    VlrTotalDesconto:  Currency;
    VlrTotalPago:      Currency;
    CadIdUsuario:      Integer;
    DataCadastro:      TDate;
    FlgStatus:         String;
    FlgCancelado:      String;
    DataCancelamento:  TDate;
    CancelIdUsuario:   Integer;
    FlgRecorrente:     String;
    FlgReplicado:      String;
    NumRecibo:         Integer;
    IdOrigem:          Integer;
    DescrTipoOrigem:   String;
    IdSistema:         Integer;
    NomeSistema:       String;
    Observacao:        String;
    IdEquivalencia:    Integer;
  end;

  TLancamento = class of TDadosClasseLancamento;

  { LANCAMENTO - DETALHE }
  TDadosClasseLancamentoDet = class(TObject)
  public
    IdLancamentoDet:    Integer;
    CodLancamento:      Integer;
    AnoLancamento:      Integer;
    IdItem:             Integer;
    DescrItem:          String;
    VlrUnitario:        Currency;
    Quantidade:         Integer;
    VlrTotal:           Currency;
    SeloOrigem:         String;
    AleatorioOrigem:    String;
    TipoCobranca:       String;
    CodAdicional:       Integer;
    Emolumentos:        Currency;
    FETJ:               Currency;
    FUNDPERJ:           Currency;
    FUNPERJ:            Currency;
    FUNARPEN:           Currency;
    PMCMV:              Currency;
    ISS:                Currency;
    Mutua:              Currency;
    Acoterj:            Currency;
    NumProtocolo:       Integer;
    FlgCancelado:       String;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseLancamentoDet = class(TDadosClasseLancamentoDet)
  public
    IdLancamentoDet:    Integer;
    CodLancamento:      Integer;
    AnoLancamento:      Integer;
    IdItem:             Integer;
    DescrItem:          String;
    VlrUnitario:        Currency;
    Quantidade:         Integer;
    VlrTotal:           Currency;
    SeloOrigem:         String;
    AleatorioOrigem:    String;
    TipoCobranca:       String;
    CodAdicional:       Integer;
    Emolumentos:        Currency;
    FETJ:               Currency;
    FUNDPERJ:           Currency;
    FUNPERJ:            Currency;
    FUNARPEN:           Currency;
    PMCMV:              Currency;
    ISS:                Currency;
    Mutua:              Currency;
    Acoterj:            Currency;
    NumProtocolo:       Integer;
    FlgCancelado:       String;
  end;

  TLancamentoDet = class of TDadosClasseLancamentoDet;

  { LACAMENTO - PARCELA }
  TDadosClasseLancamentoParc = class(TObject)
  public
    IdLancamentoParc: Integer;
    CodLancamento:    Integer;
    AnoLancamento:    Integer;
    DataLancParc:     TDate;
    NumParcela:       Integer;
    DataVencimento:   TDate;
    VlrPrevisto:      Currency;
    VlrJuros:         Currency;
    VlrDesconto:      Currency;
    VlrPago:          Currency;
    IdFormaPagtoOrig: Integer;
    IdFormaPagto:     Integer;
    Agencia:          String;
    Conta:            String;
    IdBanco:          Integer;
    NumCheque:        String;
    NumCodDeposito:   String;
    FlgStatus:        String;
    CadIdUsuario:     Integer;
    DataCadastro:     TDate;
    CancelIdUsuario:  Integer;
    DataCancelamento: TDate;
    PagIdUsuario:     Integer;
    DataPagamento:    TDate;
    Observacao:       String;

    constructor Create(); virtual;
    destructor  Destroy; override;
  end;

  TClasseLancamentoParc = class(TDadosClasseLancamentoParc)
  public
    IdLancamentoParc: Integer;
    CodLancamento:    Integer;
    AnoLancamento:    Integer;
    DataLancParc:     TDate;
    NumParcela:       Integer;
    DataVencimento:   TDate;
    VlrPrevisto:      Currency;
    VlrJuros:         Currency;
    VlrDesconto:      Currency;
    VlrPago:          Currency;
    IdFormaPagtoOrig: Integer;
    IdFormaPagto:     Integer;
    Agencia:          String;
    Conta:            String;
    IdBanco:          Integer;
    NumCheque:        String;
    NumCodDeposito:   String;
    FlgStatus:        String;
    CadIdUsuario:     Integer;
    DataCadastro:     TDate;
    CancelIdUsuario:  Integer;
    DataCancelamento: TDate;
    PagIdUsuario:     Integer;
    DataPagamento:    TDate;
    Observacao:       String;
  end;

  TLancamentoParc = class of TDadosClasseLancamentoParc;

  TdmPrincipal = class(TDataModule)
    conSISTEMA: TFDConnection;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysFBDriverLink2: TFDPhysFBDriverLink;
    FDGUIxWaitCursor2: TFDGUIxWaitCursor;
    conFIRMAS: TFDConnection;
    FDPhysFBDriverLink3: TFDPhysFBDriverLink;
    FDGUIxWaitCursor3: TFDGUIxWaitCursor;
    conFIRMAS_ANTIGO: TFDConnection;
    FDPhysFBDriverLink4: TFDPhysFBDriverLink;
    FDGUIxWaitCursor4: TFDGUIxWaitCursor;
    conRECIBO: TFDConnection;
    qryFunc1: TFDQuery;
    qryFunc2: TFDQuery;
    dspFunc2: TDataSetProvider;
    cdsFunc2: TClientDataSet;
    cdsFunc2ID_FUNCIONARIO: TIntegerField;
    cdsFunc2NOME_FUNCIONARIO: TStringField;
    cdsFunc2DATA_NASCIMENTO: TDateField;
    cdsFunc2NUM_IDENTIDADE: TStringField;
    cdsFunc2ORGAO_EMISSOR: TStringField;
    cdsFunc2DATA_EMISSAO_IDENTIDADE: TDateField;
    cdsFunc2CPF: TStringField;
    cdsFunc2NUM_CTPS: TStringField;
    cdsFunc2SERIE_CTPS: TStringField;
    cdsFunc2DATA_CTPS: TDateField;
    cdsFunc2NUM_PIS: TStringField;
    cdsFunc2SIGLA_ESTADO_CTPS: TStringField;
    cdsFunc2NUM_TITULO_ELEITOR: TStringField;
    cdsFunc2ZONA_TE: TStringField;
    cdsFunc2SECAO_TE: TStringField;
    cdsFunc2COD_MUNICIPIO_TE: TStringField;
    cdsFunc2SIGLA_ESTADO_TE: TStringField;
    cdsFunc2DATA_EMISSAO_TE: TDateField;
    cdsFunc2TIPO_SANGUINEO: TStringField;
    cdsFunc2FATOR_RH: TStringField;
    cdsFunc2LOGRADOURO: TStringField;
    cdsFunc2NUM_LOGRADOURO: TStringField;
    cdsFunc2COMPLEMENTO_LOGRADOURO: TStringField;
    cdsFunc2BAIRRO_LOGRADOURO: TStringField;
    cdsFunc2CEP_LOGR: TStringField;
    cdsFunc2COD_MUNICIPIO_LOGR: TStringField;
    cdsFunc2SIGLA_ESTADO_LOGR: TStringField;
    cdsFunc2TELEFONE: TStringField;
    cdsFunc2CELULAR: TStringField;
    cdsFunc2EMAIL: TStringField;
    cdsFunc2ID_CARGO_FK: TIntegerField;
    cdsFunc2COD_ESCOLARIDADE: TIntegerField;
    cdsFunc2COD_ESTADOCIVIL: TIntegerField;
    cdsFunc2QTD_FILHOS: TIntegerField;
    cdsFunc2QTD_DEPENDENTES: TIntegerField;
    cdsFunc2HORA_ENT_TRAB: TTimeField;
    cdsFunc2HORA_SAI_ALMOCO: TTimeField;
    cdsFunc2HORA_ENT_ALMOCO: TTimeField;
    cdsFunc2HORA_SAI_TRAB: TTimeField;
    cdsFunc2VR_SALARIO: TBCDField;
    cdsFunc2VR_MAXIMO_VALE: TBCDField;
    cdsFunc2SEXO: TStringField;
    cdsFunc2DATA_ADMISSAO: TDateField;
    cdsFunc2DATA_CADASTRO: TDateField;
    cdsFunc2DATA_DESLIGAMENTO: TDateField;
    cdsFunc2FLG_ATIVO: TStringField;
    dsFunc1: TDataSource;
    dsFunc2: TDataSource;
    qryCar1: TFDQuery;
    qryCar2: TFDQuery;
    qryFuncCom1: TFDQuery;
    qryFuncCom2: TFDQuery;
    dspCar2: TDataSetProvider;
    cdsCar2: TClientDataSet;
    cdsCar2ID_CARGO: TIntegerField;
    cdsCar2DESCR_CARGO: TStringField;
    cdsCar2COD_CARGO: TIntegerField;
    dsCar2: TDataSource;
    dspFuncCom2: TDataSetProvider;
    cdsFuncCom2: TClientDataSet;
    cdsFuncCom2ID_FUNCIONARIO_COMISSAO: TIntegerField;
    cdsFuncCom2ID_FUNCIONARIO_FK: TIntegerField;
    cdsFuncCom2PERCENT_COMISSAO: TBCDField;
    cdsFuncCom2ID_SISTEMA_FK: TIntegerField;
    cdsFuncCom2COD_ADICIONAL: TIntegerField;
    dsFuncCom2: TDataSource;
    dsCar1: TDataSource;
    dsFuncCom1: TDataSource;
    qryUsu1: TFDQuery;
    qryUsu2: TFDQuery;
    dspUsu2: TDataSetProvider;
    cdsUsu2: TClientDataSet;
    cdsUsu2ID_USUARIO: TIntegerField;
    cdsUsu2NOME: TStringField;
    cdsUsu2CPF: TStringField;
    cdsUsu2SENHA: TStringField;
    cdsUsu2QUALIFICACAO: TStringField;
    cdsUsu2FLG_MASTER: TStringField;
    cdsUsu2MATRICULA: TStringField;
    cdsUsu2FLG_SUPORTE: TStringField;
    cdsUsu2FLG_VISUALIZOU_ATU: TStringField;
    cdsUsu2FLG_SEXO: TStringField;
    cdsUsu2ID_FUNCIONARIO_FK: TIntegerField;
    cdsUsu2FLG_ATIVO: TStringField;
    cdsUsu2DATA_CADASTRO: TDateField;
    cdsUsu2DATA_INATIVACAO: TDateField;
    dsUsu1: TDataSource;
    dsUsu2: TDataSource;
    qryConfiguracao: TFDQuery;
    dsConfiguracao: TDataSource;
    qryPermissao: TFDQuery;
    dsPermissao: TDataSource;
    dsAuxLanc: TDataSource;
    qryAuxLanc: TFDQuery;
    dspAuxLanc: TDataSetProvider;
    cdsAuxLanc: TClientDataSet;
    cdsAuxLancCOD_LANCAMENTO: TIntegerField;
    cdsAuxLancANO_LANCAMENTO: TIntegerField;
    cdsAuxLancTIPO_LANCAMENTO: TStringField;
    cdsAuxLancTIPO_CADASTRO: TStringField;
    cdsAuxLancID_CLIENTE_FORNECEDOR_FK: TIntegerField;
    cdsAuxLancID_CATEGORIA_DESPREC_FK: TIntegerField;
    cdsAuxLancID_SUBCATEGORIA_DESPREC_FK: TIntegerField;
    cdsAuxLancFLG_IMPOSTORENDA: TStringField;
    cdsAuxLancFLG_PESSOAL: TStringField;
    cdsAuxLancFLG_AUXILIAR: TStringField;
    cdsAuxLancFLG_REAL: TStringField;
    cdsAuxLancID_NATUREZA_FK: TIntegerField;
    cdsAuxLancQTD_PARCELAS: TIntegerField;
    cdsAuxLancVR_TOTAL_PREV: TBCDField;
    cdsAuxLancVR_TOTAL_JUROS: TBCDField;
    cdsAuxLancVR_TOTAL_DESCONTO: TBCDField;
    cdsAuxLancVR_TOTAL_PAGO: TBCDField;
    cdsAuxLancCAD_ID_USUARIO: TIntegerField;
    cdsAuxLancFLG_STATUS: TStringField;
    cdsAuxLancFLG_CANCELADO: TStringField;
    cdsAuxLancCANCEL_ID_USUARIO: TIntegerField;
    cdsAuxLancFLG_RECORRENTE: TStringField;
    cdsAuxLancFLG_REPLICADO: TStringField;
    cdsAuxLancNUM_RECIBO: TIntegerField;
    cdsAuxLancOBSERVACAO: TStringField;
    cdsAuxLancID_ORIGEM: TIntegerField;
    cdsAuxLancID_SISTEMA_FK: TIntegerField;
    cdsAuxLancFLG_FLUTUANTE: TStringField;
    conFIRMAS_ACENTRAL: TFDConnection;
    FDPhysFBDriverLink5: TFDPhysFBDriverLink;
    FDGUIxWaitCursor5: TFDGUIxWaitCursor;
    cdsAuxLancDATA_CADASTRO: TSQLTimeStampField;
    cdsAuxLancDATA_CANCELAMENTO: TDateField;
    cdsAuxLancDATA_LANCAMENTO: TDateField;
    cdsFuncCom2ID_ITEM_FK: TIntegerField;
    cdsAuxLancID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField;
    cdsAuxLancFLG_FORAFECHCAIXA: TStringField;
    qryRemetente: TFDQuery;
    dsRemetente: TDataSource;
    dspRemetente: TDataSetProvider;
    cdsRemetente: TClientDataSet;
    cdsRemetenteID_EMAIL_REMETENTE: TIntegerField;
    cdsRemetenteNOME_REMETENTE: TStringField;
    cdsRemetenteEMAIL_REMETENTE: TStringField;
    cdsRemetenteSENHA_REMETENTE: TStringField;
    cdsRemetenteSERVIDOR_SMTP: TStringField;
    cdsRemetentePORTA_SMTP: TIntegerField;
    cdsRemetenteFLG_SSL_SMTP: TStringField;
    cdsRemetenteFLG_ATIVO: TStringField;
    cdsRemetenteUSERNAME_REMETENTE: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    procedure LoginSistema;
    procedure RetornarGenerico(Tipo: String; var Id, IdFK, Codigo: Integer;
      var Descricao, Observacao: String);

    function ExistemRecibosPendentesTalao: Integer;
    function ExistemRecibosPendentesBalcao: Integer;
  public
    { Public declarations }

    procedure AtualizarDespesasRecorrentes;
    procedure AtualizaTabelasGerencialMonitoramento;
    procedure RetornaCargo(var Id, Codigo: Integer; var Descricao, Observacao: String);
    procedure RetornaCategoria(var Id, IdCategoria, Codigo: Integer; var Descricao, Observacao: String);
    procedure RetornaNatureza(var Id, Codigo: Integer; var Descricao,
                              Observacao: String);
    procedure RetornaSubCategoria(var Id, IdSubCategoria, Codigo: Integer; var Descricao, Observacao: String);
    procedure RetornarUsuario(var Id, IdFuncionario: Integer; var Nome, CPF,
      Matricula, Cargo, Senha, Observacao: String);
    procedure RetornarFuncionario(var Id, IdCargo: Integer; var Nome, CPF, Observacao: String);
    procedure RetornarClienteFornecedor(Tipo: String; var Id: Integer;
      var Documento, Observacao, NomeFantasia, RazaoSocial: String);
    procedure RetornarItem(Tipo: String; var Id: Integer;
      var Descricao, Abreviacao, Observacao, UMedida: String; var Medida: Real);
    procedure RetornarClassificacaoNatureza(IdNat: Integer; var IR, CP, LA: String);

    procedure InicializarComponenteLancamento;
    procedure FinalizarComponenteLancamento;

    procedure InsertLancamento(Indice: Integer);
    procedure InsertLancamentoDet(Indice, IndiceL: Integer);
    procedure InsertLancamentoParc(Indice, IndiceL: Integer);

    procedure UpdateLancamento(Indice: Integer);
    procedure UpdateLancamentoDet(Indice, IndiceL: Integer);
    procedure UpdateLancamentoParc(Indice, IndiceL: Integer);

    procedure VerificarSituacaoCaixa;

    procedure SincronizarDadosSistemaGerencialMonitoramento(REC, FIR: Boolean);

    function AtualizarEstoqueProduto(IdItem: Integer; UltValor: Currency): String;

    function ExistemFirmasCanceladas: String;
    function ExistemFirmasAntigasPendentes: Integer;
    function ExistemFirmasPendentes: Integer;

    function ExistemRecibosCanceladosTalao: String;
    function ExistemRecibosCanceladosBalcao: String;

    function GerarDataDespesaRecorrente(Data: TDateTime): TDateTime;

    function ProcessoSistemaAtivo(NomeExe: String): Boolean;

    function RetornaEquivalenciaCarneLeao(IdCat, IdSubCat, IdNat: Integer): Integer;
  end;

var
  dmPrincipal: TdmPrincipal;

  iQtdParc,
  iQtdTot: Integer;

  { LANCAMENTO }
  Lancamento: TLancamento;
  DadosLancamento: TDadosClasseLancamento;
  ListaLancamentos: TList<TDadosClasseLancamento>;

  { LANCAMENTO - DETALHE }
  LancamentoDet: TLancamentoDet;
  DadosLancamentoDet: TDadosClasseLancamentoDet;
  ListaLancamentoDets: TList<TDadosClasseLancamentoDet>;

  { LANCAMENTO - PARCELA }
  LancamentoParc: TLancamentoParc;
  DadosLancamentoParc: TDadosClasseLancamentoParc;
  ListaLancamentoParcs: TList<TDadosClasseLancamentoParc>;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses UGDM, UPrincipal, UVariaveisGlobais, UBibliotecaSistema, UDMRecibo,
  UDMFirmas;

{$R *.dfm}

procedure TdmPrincipal.AtualizarDespesasRecorrentes;
var
  QryDesp, QryAuxDet, QryAuxParc, QryAuxLog: TFDQuery;
  IdDetalhe, IdParcela, NumParc, k, m: Integer;
  lAtualizar: Boolean;
  Msg, sQtd: String;
begin
  //Essa rotina serve para gerar as Despesas Recorrentes automaticamente
(*  sQtd := '';
  lAtualizar := False;

  IdDetalhe := 0;
  IdParcela := 0;
  NumParc   := 0;

  QryDesp    := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxDet  := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxParc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);
  QryAuxLog  := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryDesp, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT DATA_ULT_ATUDESPESAS FROM CONFIGURACAO';
    Open;

    lAtualizar := (QryDesp.FieldByName('DATA_ULT_ATUDESPESAS').AsDateTime < Date);

    if lAtualizar then
    begin
      //Pesquisa todas as Despesas Recorrentes que n�o possuem replica no m�s atual
      cdsAuxLanc.Close;

      if MonthOf(Date) = 1 then
      begin
        cdsAuxLanc.Params.ParamByName('MES_CADASTRO').Value  := 12;
        cdsAuxLanc.Params.ParamByName('ANO_CADASTRO').Value  := (YearOf(Date) - 1);
      end
      else
      begin
        cdsAuxLanc.Params.ParamByName('MES_CADASTRO').Value  := (MonthOf(Date) - 1);
        cdsAuxLanc.Params.ParamByName('ANO_CADASTRO').Value  := YearOf(Date);
      end;

      cdsAuxLanc.Open;
      cdsAuxLanc.Last;

      sQtd := IntToStr(cdsAuxLanc.RecordCount);

      lAtualizar := cdsAuxLanc.RecordCount > 0;
    end;

    if lAtualizar then
    begin
      FPrincipal.mmProcessos.Lines.Add('===============================================');
      FPrincipal.mmProcessos.Lines.Add('=             DESPESAS RECORRENTES            =');
      FPrincipal.mmProcessos.Lines.Add('===============================================');

      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        if vgConGER.Connected then
          vgConGER.StartTransaction;

        cdsAuxLanc.First;

        vgLanc_Codigo := dmGerencial.ProximoCodigoLancamento;
        vgLanc_Ano := YearOf(Date);

        IdDetalhe := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');
        IdParcela := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');

        while not cdsAuxLanc.Eof do
        begin
          InicializarComponenteLancamento;

          FPrincipal.mmProcessos.Lines.Add('Atualizando Despesas Recorrentes... ' +
                                           IntToStr(cdsAuxLanc.RecNo) + '/' + sQtd);

          //Copia as Despesas
          { LANCAMENTO }
          DadosLancamento := Lancamento.Create;

          DadosLancamento.TipoLancamento     := 'D';
          DadosLancamento.TipoCadastro       := 'A';

          if cdsAuxLanc.FieldByName('ID_CLIENTE_FORNECEDOR_FK').IsNull then
            DadosLancamento.IdCliFor         := 0
          else
            DadosLancamento.IdCliFor         := cdsAuxLanc.FieldByName('ID_CLIENTE_FORNECEDOR_FK').AsInteger;

            if cdsAuxLanc.FieldByName('ID_CATEGORIA_DESPREC_FK').IsNull then
              DadosLancamento.IdCategoria    := 0
            else
              DadosLancamento.IdCategoria    := cdsAuxLanc.FieldByName('ID_CATEGORIA_DESPREC_FK').AsInteger;

            if cdsAuxLanc.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').IsNull then
              DadosLancamento.IdSubCategoria := 0
            else
              DadosLancamento.IdSubCategoria := cdsAuxLanc.FieldByName('ID_SUBCATEGORIA_DESPREC_FK').AsInteger;

          DadosLancamento.FlgIR              := cdsAuxLanc.FieldByName('FLG_IMPOSTORENDA').AsString;
          DadosLancamento.FlgPessoal         := cdsAuxLanc.FieldByName('FLG_PESSOAL').AsString;
          DadosLancamento.FlgAuxiliar        := cdsAuxLanc.FieldByName('FLG_AUXILIAR').AsString;
          DadosLancamento.FlgFlutuante       := cdsAuxLanc.FieldByName('FLG_FLUTUANTE').AsString;
          DadosLancamento.FlgForaFechCaixa   := cdsAuxLanc.FieldByName('FLG_FORAFECHCAIXA').AsString;
          DadosLancamento.IdNatureza         := cdsAuxLanc.FieldByName('ID_NATUREZA_FK').AsInteger;
          DadosLancamento.QtdParcelas        := cdsAuxLanc.FieldByName('QTD_PARCELAS').AsInteger;
          DadosLancamento.VlrTotalPrev       := cdsAuxLanc.FieldByName('VR_TOTAL_PREV').AsCurrency;
          DadosLancamento.VlrTotalJuros      := 0;
          DadosLancamento.VlrTotalDesconto   := 0;
          DadosLancamento.VlrTotalPago       := 0;
          DadosLancamento.CadIdUsuario       := vgUsu_Id;
          DadosLancamento.DataCadastro       := Now;
          DadosLancamento.DataLancamento     := GerarDataDespesaRecorrente(cdsAuxLanc.FieldByName('DATA_LANCAMENTO').AsDateTime);
          DadosLancamento.FlgRecorrente      := 'S';
          DadosLancamento.FlgReplicado       := 'N';
          DadosLancamento.IdEquivalencia     := cdsAuxLanc.FieldByName('ID_CARNELEAO_EQUIVALENCIA_FK').AsInteger;
          DadosLancamento.Observacao         := cdsAuxLanc.FieldByName('OBSERVACAO').AsString;

          ListaLancamentos.Add(DadosLancamento);

          dmPrincipal.InsertLancamento(0);

          vgLanc_Codigo := ListaLancamentos[0].CodLancamento;
          vgLanc_Ano    := ListaLancamentos[0].AnoLancamento;

          { DETALHE }
          QryAuxDet.Close;
          QryAuxDet.SQL.Clear;

          QryAuxDet.SQL.Text := 'SELECT * FROM LANCAMENTO_DET ' +
                                 '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                 '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                 '    AND FLG_CANCELADO = ' + QuotedStr('N') +
                                 'ORDER BY ID_LANCAMENTO_DET';

          QryAuxDet.Params.ParamByName('COD_LANCAMENTO').Value := cdsAuxLanc.FieldByName('COD_LANCAMENTO').AsInteger;
          QryAuxDet.Params.ParamByName('ANO_LANCAMENTO').Value := cdsAuxLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
          QryAuxDet.Open;

          QryAuxDet.First;
          m := 0;

          while not QryAuxDet.Eof do
          begin
            DadosLancamentoDet := LancamentoDet.Create;

            DadosLancamentoDet.IdLancamentoDet   := IdDetalhe;
            DadosLancamentoDet.IdItem            := QryAuxDet.FieldByName('ID_ITEM_FK').AsInteger;
            DadosLancamentoDet.Quantidade        := QryAuxDet.FieldByName('QUANTIDADE').AsInteger;
            DadosLancamentoDet.VlrUnitario       := QryAuxDet.FieldByName('VR_UNITARIO').AsCurrency;
            DadosLancamentoDet.VlrTotal          := QryAuxDet.FieldByName('VR_TOTAL').AsCurrency;

            if Trim(QryAuxDet.FieldByName('SELO_ORIGEM').AsString) <> '' then
              DadosLancamentoDet.SeloOrigem      := QryAuxDet.FieldByName('SELO_ORIGEM').AsString;

            if Trim(QryAuxDet.FieldByName('ALEATORIO_ORIGEM').AsString) <> '' then
              DadosLancamentoDet.AleatorioOrigem := QryAuxDet.FieldByName('ALEATORIO_ORIGEM').AsString;

            if Trim(QryAuxDet.FieldByName('TIPO_COBRANCA').AsString) <> '' then
              DadosLancamentoDet.TipoCobranca    := QryAuxDet.FieldByName('TIPO_COBRANCA').AsString;

            if not QryAuxDet.FieldByName('COD_ADICIONAL').IsNull then
              DadosLancamentoDet.CodAdicional    := QryAuxDet.FieldByName('COD_ADICIONAL').AsInteger;

            if not QryAuxDet.FieldByName('EMOLUMENTOS').IsNull then
              DadosLancamentoDet.Emolumentos     := QryAuxDet.FieldByName('EMOLUMENTOS').AsCurrency;

            if not QryAuxDet.FieldByName('FETJ').IsNull then
              DadosLancamentoDet.FETJ            := QryAuxDet.FieldByName('FETJ').AsCurrency;

            if not QryAuxDet.FieldByName('FUNDPERJ').IsNull then
              DadosLancamentoDet.FUNDPERJ        := QryAuxDet.FieldByName('FUNDPERJ').AsCurrency;

            if not QryAuxDet.FieldByName('FUNPERJ').IsNull then
              DadosLancamentoDet.FUNPERJ         := QryAuxDet.FieldByName('FUNPERJ').AsCurrency;

            if not QryAuxDet.FieldByName('FUNARPEN').IsNull then
              DadosLancamentoDet.FUNARPEN        := QryAuxDet.FieldByName('FUNARPEN').AsCurrency;

            if not QryAuxDet.FieldByName('PMCMV').IsNull then
              DadosLancamentoDet.PMCMV           := QryAuxDet.FieldByName('PMCMV').AsCurrency;

            if not QryAuxDet.FieldByName('ISS').IsNull then
              DadosLancamentoDet.ISS             := QryAuxDet.FieldByName('ISS').AsCurrency;

            if not QryAuxDet.FieldByName('MUTUA').IsNull then
              DadosLancamentoDet.Mutua           := QryAuxDet.FieldByName('MUTUA').AsCurrency;

            if not QryAuxDet.FieldByName('ACOTERJ').IsNull then
              DadosLancamentoDet.Acoterj         := QryAuxDet.FieldByName('ACOTERJ').AsCurrency;

            if not QryAuxDet.FieldByName('NUM_PROTOCOLO').IsNull then
              DadosLancamentoDet.NumProtocolo    := QryAuxDet.FieldByName('NUM_PROTOCOLO').AsInteger;

            ListaLancamentoDets.Add(DadosLancamentoDet);

            dmPrincipal.InsertLancamentoDet(m, 0);

            IdDetalhe := ListaLancamentoDets[m].IdLancamentoDet;

            Inc(IdDetalhe);
            Inc(m);

            QryAuxDet.Next;
          end;

          { PARCELAS }
          QryAuxParc.Close;
          QryAuxParc.SQL.Clear;

          QryAuxParc.SQL.Text := 'SELECT * FROM LANCAMENTO_PARC ' +
                                 '  WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
                                 '    AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ' +
                                 '    AND FLG_STATUS <> ' + QuotedStr('C') +
                                 'ORDER BY ID_LANCAMENTO_PARC';

          QryAuxParc.Params.ParamByName('COD_LANCAMENTO').Value := cdsAuxLanc.FieldByName('COD_LANCAMENTO').AsInteger;
          QryAuxParc.Params.ParamByName('ANO_LANCAMENTO').Value := cdsAuxLanc.FieldByName('ANO_LANCAMENTO').AsInteger;
          QryAuxParc.Open;

          NumParc   := 1;

          QryAuxParc.First;
          k := 0;

          while not QryAuxParc.Eof do
          begin
            DadosLancamentoParc := LancamentoParc.Create;

            DadosLancamentoParc.IdLancamentoParc := IdParcela;
            DadosLancamentoParc.IdFormaPagto     := QryAuxParc.FieldByName('ID_FORMAPAGAMENTO_FK').AsInteger;
            DadosLancamentoParc.DataLancParc     := GerarDataDespesaRecorrente(QryAuxParc.FieldByName('DATA_LANCAMENTO_PARC').AsDateTime);
            DadosLancamentoParc.NumParcela       := NumParc;
            DadosLancamentoParc.DataVencimento   := GerarDataDespesaRecorrente(QryAuxParc.FieldByName('DATA_VENCIMENTO').AsDateTime);
            DadosLancamentoParc.VlrPrevisto      := QryAuxParc.FieldByName('VR_PARCELA_PREV').AsCurrency;
            DadosLancamentoParc.Observacao       := QryAuxParc.FieldByName('OBS_LANCAMENTO_PARC').AsString;
            DadosLancamentoParc.CadIdUsuario     := vgUsu_Id;
            DadosLancamentoParc.DataCadastro     := Now;

            ListaLancamentoParcs.Add(DadosLancamentoParc);

            dmPrincipal.InsertLancamentoParc(k, 0);

            Inc(IdParcela);
            Inc(NumParc);
            Inc(k);

            FPrincipal.mmProcessos.Lines.Add('Lan�amento inclu�do => C�digo: ' +
                                             IntToStr(vgLanc_Codigo) + ' / Ano: ' +
                                             IntToStr(vgLanc_Ano));

            QryAuxParc.Next;
          end;

          { LOG }
          QryAuxLog.Close;
          QryAuxLog.SQL.Clear;
          QryAuxLog.SQL.Text := 'INSERT INTO USUARIO_LOGSYNC (ID_USUARIO_LOGSYNC, ID_USUARIO, ID_MODULO_FK, ' +
                                '                             TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
                                '                             TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOGSYNC) ' +
                                '                     VALUES (:ID_USUARIO_LOGSYNC, :ID_USUARIO, :ID_MODULO, ' +
                                '                             :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
                                '                             :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOGSYNC)';

          QryAuxLog.Params.ParamByName('ID_USUARIO_LOGSYNC').Value  := BS.ProximoId('ID_USUARIO_LOGSYNC', 'USUARIO_LOGSYNC');
          QryAuxLog.Params.ParamByName('ID_USUARIO').Value          := vgUsu_Id;
          QryAuxLog.Params.ParamByName('ID_MODULO').Value           := 2;
          QryAuxLog.Params.ParamByName('TIPO_LANCAMENTO').Value     := 'D';
          QryAuxLog.Params.ParamByName('CODIGO_LAN').Value          := vgLanc_Codigo;
          QryAuxLog.Params.ParamByName('ANO_LAN').Value             := vgLanc_Ano;
          QryAuxLog.Params.ParamByName('ID_ORIGEM').Value           := Null;
          QryAuxLog.Params.ParamByName('TABELA_ORIGEM').Value       := Null;
          QryAuxLog.Params.ParamByName('TIPO_ACAO').Value           := I;
          QryAuxLog.Params.ParamByName('OBS_USUARIO_LOGSYNC').Value := 'Inclus�o autom�tica de Despesas Recorrentes.';

          QryAuxLog.ExecSQL;

          { ATUALIZA FLG_REPLICADO DA DESPESA ORIGINAL }
          cdsAuxLanc.Edit;
          cdsAuxLanc.FieldByName('FLG_REPLICADO').AsString := 'S';
          cdsAuxLanc.Post;

          FinalizarComponenteLancamento;

          cdsAuxLanc.Next;
        end;

        cdsAuxLanc.ApplyUpdates(0);

        Close;
        Clear;
        Text := 'UPDATE CONFIGURACAO SET DATA_ULT_ATUDESPESAS = :DATA_ULT_ATUDESPESAS';
        Params.ParamByName('DATA_ULT_ATUDESPESAS').Value := Date;
        ExecSQL;

        if vgConGER.InTransaction then
          vgConGER.Commit;

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;
      except
        on E: Exception do
        begin
          if vgConGER.InTransaction then
            vgConGER.Rollback;

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao atualizar as DESPESAS RECORRENTES. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;
          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
        end;
      end;

      FinalizarComponenteLancamento;
    end;
  end;

//  pdlgDespesas.Hide;

  FreeAndNil(QryAuxLog);
  FreeAndNil(QryAuxDet);
  FreeAndNil(QryAuxParc);
  FreeAndNil(QryDesp);  *)
end;

function TdmPrincipal.AtualizarEstoqueProduto(IdItem: Integer; UltValor: Currency): String;
var
  Qtd: Integer;
  QryAux: TFDQuery;
begin
  Result := '';

  Qtd := 0;

  QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //Atualiza Estoque Atual do Item
  try
    if vgConSISTEMA.Connected then
      vgConSISTEMA.StartTransaction;

    QryAux.Close;
    QryAux.SQL.Clear;
    QryAux.SQL.Text := 'SELECT I.ID_ITEM AS ID, ' +
                       '       (SELECT SUM(HI1.QUANTIDADE) AS ENTRADAS ' +
                       '          FROM HISTORICO_ITEM HI1 ' +
                       '         WHERE HI1.ID_ITEM_FK = I.ID_ITEM ' +
                       '           AND HI1.FLG_TIPO_HISTORICO = ' + QuotedStr('E') +
                       '        GROUP BY HI1.ID_ITEM_FK) AS TOT_ENTRADAS, ' +
                       '       (SELECT SUM(HI2.QUANTIDADE) AS SAIDAS ' +
                       '          FROM HISTORICO_ITEM HI2 ' +
                       '         WHERE HI2.ID_ITEM_FK = I.ID_ITEM ' +
                       '           AND HI2.FLG_TIPO_HISTORICO = ' + QuotedStr('S') +
                       '        GROUP BY HI2.ID_ITEM_FK) AS TOT_SAIDAS ' +
                       '  FROM ITEM I ' +
                       ' WHERE I.ID_ITEM = :ID_ITEM ' +
                       'GROUP BY ID_ITEM';
    QryAux.Params.ParamByName('ID_ITEM').Value := IdItem;
    QryAux.Open;

    //Calculo do Estoque
    Qtd := (QryAux.FieldByName('TOT_ENTRADAS').AsInteger - QryAux.FieldByName('TOT_SAIDAS').AsInteger);

    QryAux.Close;
    QryAux.SQL.Clear;
    QryAux.SQL.Text := 'UPDATE ITEM ' +
                       '   SET ESTOQUE_ATUAL   = :ESTOQUE_ATUAL ';

    if UltValor >= 0 then
    begin
      QryAux.SQL.Add(', ULT_VALOR_CUSTO = :ULT_VALOR_CUSTO ');

      QryAux.Params.ParamByName('ULT_VALOR_CUSTO').Value := UltValor;
    end;

    QryAux.SQL.Add(' WHERE ID_ITEM = :ID_ITEM');

    QryAux.Params.ParamByName('ESTOQUE_ATUAL').Value := Qtd;
    QryAux.Params.ParamByName('ID_ITEM').Value       := IdItem;
    QryAux.ExecSQL;

    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Commit;
  except
    if vgConSISTEMA.InTransaction then
      vgConSISTEMA.Rollback;

    Result := '';
  end;

  FreeAndNil(QryAux);
end;

procedure TdmPrincipal.AtualizaTabelasGerencialMonitoramento;
var
  sId, Msg, sQtd: String;
  Op: TOperacao;
  iId: Integer;
begin
  { Essa procedure compara algumas tabelas do banco de dados do GERENCIAL e do
    MONITORAMENTO e atualiza as tabelas no banco de dados do MONITORAMENTO.
    Tratam-se de tabelas que s�o manipuladas somente no GERENCIAL, mas que possuem
    tabelas filhas que fazem referencia a elas e que sao manipuladas somente no
    MONITORAMENTO }
  Op := vgOperacao;

  sId  := '';
  Msg  := '';
  sQtd := '';

  iId := 0;

  { CARGO }
  qryCar1.Close;
  qryCar1.Open;
  qryCar1.Last;

  sQtd := IntToStr(qryCar1.RecordCount);
  qryCar1.First;

  cdsCar2.Close;
  cdsCar2.Open;
  cdsCar2.First;

  { Verifica se o registro existe no MONITORAMENTO e inclui, caso nao exista }
  while not qryCar1.Eof do
  begin
{    FPrincipal.mmProcessos.Lines.Add('Sincronizando as tabelas gerais do GERENCIAL... ' +
                                     IntToStr(qryCar1.RecNo) + '/' + sQtd +
                                     ' (Tabela: CARGO)');  }

    vgTextoMemo := vgTextoMemo + #13#10 + 'Sincronizando as tabelas gerais do GERENCIAL... ' +
                                          IntToStr(qryCar1.RecNo) + '/' + sQtd +
                                          ' (Tabela: CARGO)';

    sId := qryCar1.FieldByName('ID_CARGO').AsString;

    if not cdsCar2.Locate('ID_CARGO',
                           qryCar1.FieldByName('ID_CARGO').AsInteger,
                           [loCaseInsensitive, loPartialKey]) then
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        cdsCar2.Append;
        cdsCar2.FieldByName('ID_CARGO').Value    := qryCar1.FieldByName('ID_CARGO').AsInteger;
        cdsCar2.FieldByName('COD_CARGO').Value   := qryCar1.FieldByName('COD_CARGO').AsInteger;
        cdsCar2.FieldByName('DESCR_CARGO').Value := qryCar1.FieldByName('DESCR_CARGO').AsString;
        cdsCar2.Post;
        cdsCar2.ApplyUpdates(0);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        Op := vgOperacao;
        vgOperacao := I;
        BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Cargo',
                            qryCar1.FieldByName('ID_CARGO').AsInteger,
                            'CARGO');
        vgOperacao := Op;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao incluir o ID ' + sId + ' da tabela CARGO do GERENCIAL ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end
    else  { Traz os registros que existem em ambos os bancos, compara os campos e atualiza no MONITORAMENTO }
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        cdsCar2.Edit;
        if qryCar1.FieldByName('COD_CARGO').AsInteger <> cdsCar2.FieldByName('COD_CARGO').AsInteger then
          cdsCar2.FieldByName('COD_CARGO').Value := qryCar1.FieldByName('COD_CARGO').AsInteger;

        if qryCar1.FieldByName('DESCR_CARGO').AsString <> cdsCar2.FieldByName('DESCR_CARGO').AsString then
          cdsCar2.FieldByName('DESCR_CARGO').AsString := qryCar1.FieldByName('DESCR_CARGO').AsString;

        cdsCar2.Post;
        cdsCar2.ApplyUpdates(0);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        Op := vgOperacao;
        vgOperacao := E;
        BS.GravarUsuarioLog(80, '', 'Edi��o AUTOM�TICA de Cargo',
                            qryCar1.FieldByName('ID_CARGO').AsInteger,
                            'CARGO');
        vgOperacao := Op;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao alterar os dados do registro de ID ' + sId + ' da tabela CARGO do GERENCIAL ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end;

    qryCar1.Next;
  end;

  { FUNCIONARIO }
  qryFunc1.Close;
  qryFunc1.Open;
  qryFunc1.Last;

  sQtd := IntToStr(qryFunc1.RecordCount);

  qryFunc1.First;

  cdsFunc2.Close;
  cdsFunc2.Open;
  cdsFunc2.First;

  { Verifica se o registro existe no MONITORAMENTO e inclui, caso nao exista }
  while not qryFunc1.Eof do
  begin
{    FPrincipal.mmProcessos.Lines.Add('Sincronizando as tabelas gerais do GERENCIAL... ' +
                                     IntToStr(qryFunc1.RecNo) + '/' + sQtd +
                                     ' (Tabela: FUNCIONARIO)');  }

    vgTextoMemo := vgTextoMemo + #13#10 + 'Sincronizando as tabelas gerais do GERENCIAL... ' +
                                          IntToStr(qryFunc1.RecNo) + '/' + sQtd +
                                          ' (Tabela: FUNCIONARIO)';

    sId := qryFunc1.FieldByName('ID_FUNCIONARIO').AsString;

    if not cdsFunc2.Locate('ID_FUNCIONARIO',
                           qryFunc1.FieldByName('ID_FUNCIONARIO').AsInteger,
                           [loCaseInsensitive, loPartialKey]) then
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        cdsFunc2.Append;

        cdsFunc2.FieldByName('ID_FUNCIONARIO').Value := qryFunc1.FieldByName('ID_FUNCIONARIO').AsInteger;

        if not qryFunc1.FieldByName('NOME_FUNCIONARIO').IsNull then
          cdsFunc2.FieldByName('NOME_FUNCIONARIO').Value := qryFunc1.FieldByName('NOME_FUNCIONARIO').AsString;

        if not qryFunc1.FieldByName('DATA_NASCIMENTO').IsNull then
          cdsFunc2.FieldByName('DATA_NASCIMENTO').Value := qryFunc1.FieldByName('DATA_NASCIMENTO').AsDateTime;

        if not qryFunc1.FieldByName('NUM_IDENTIDADE').IsNull then
          cdsFunc2.FieldByName('NUM_IDENTIDADE').Value := qryFunc1.FieldByName('NUM_IDENTIDADE').AsString;

        if not qryFunc1.FieldByName('ORGAO_EMISSOR').IsNull then
          cdsFunc2.FieldByName('ORGAO_EMISSOR').Value := qryFunc1.FieldByName('ORGAO_EMISSOR').AsString;

        if not qryFunc1.FieldByName('DATA_EMISSAO_IDENTIDADE').IsNull then
          cdsFunc2.FieldByName('DATA_EMISSAO_IDENTIDADE').Value := qryFunc1.FieldByName('DATA_EMISSAO_IDENTIDADE').AsDateTime;

        if not qryFunc1.FieldByName('CPF').IsNull then
          cdsFunc2.FieldByName('CPF').Value := qryFunc1.FieldByName('CPF').AsString;

        if not qryFunc1.FieldByName('NUM_CTPS').IsNull then
          cdsFunc2.FieldByName('NUM_CTPS').Value := qryFunc1.FieldByName('NUM_CTPS').AsString;

        if not qryFunc1.FieldByName('SERIE_CTPS').IsNull then
          cdsFunc2.FieldByName('SERIE_CTPS').Value := qryFunc1.FieldByName('SERIE_CTPS').AsString;

        if not qryFunc1.FieldByName('DATA_CTPS').IsNull then
          cdsFunc2.FieldByName('DATA_CTPS').Value := qryFunc1.FieldByName('DATA_CTPS').AsDateTime;

        if not qryFunc1.FieldByName('NUM_PIS').IsNull then
          cdsFunc2.FieldByName('NUM_PIS').Value := qryFunc1.FieldByName('NUM_PIS').AsString;

        if not qryFunc1.FieldByName('SIGLA_ESTADO_CTPS').IsNull then
          cdsFunc2.FieldByName('SIGLA_ESTADO_CTPS').Value := qryFunc1.FieldByName('SIGLA_ESTADO_CTPS').AsString;

        if not qryFunc1.FieldByName('NUM_TITULO_ELEITOR').IsNull then
          cdsFunc2.FieldByName('NUM_TITULO_ELEITOR').Value := qryFunc1.FieldByName('NUM_TITULO_ELEITOR').AsString;

        if not qryFunc1.FieldByName('ZONA_TE').IsNull then
          cdsFunc2.FieldByName('ZONA_TE').Value := qryFunc1.FieldByName('ZONA_TE').AsString;

        if not qryFunc1.FieldByName('SECAO_TE').IsNull then
          cdsFunc2.FieldByName('SECAO_TE').Value := qryFunc1.FieldByName('SECAO_TE').AsString;

        if not qryFunc1.FieldByName('COD_MUNICIPIO_TE').IsNull then
          cdsFunc2.FieldByName('COD_MUNICIPIO_TE').Value := qryFunc1.FieldByName('COD_MUNICIPIO_TE').AsString;

        if not qryFunc1.FieldByName('SIGLA_ESTADO_TE').IsNull then
          cdsFunc2.FieldByName('SIGLA_ESTADO_TE').Value := qryFunc1.FieldByName('SIGLA_ESTADO_TE').AsString;

        if not qryFunc1.FieldByName('DATA_EMISSAO_TE').IsNull then
          cdsFunc2.FieldByName('DATA_EMISSAO_TE').Value := qryFunc1.FieldByName('DATA_EMISSAO_TE').AsDateTime;

        if not qryFunc1.FieldByName('TIPO_SANGUINEO').IsNull then
          cdsFunc2.FieldByName('TIPO_SANGUINEO').Value := qryFunc1.FieldByName('TIPO_SANGUINEO').AsString;

        if not qryFunc1.FieldByName('FATOR_RH').IsNull then
          cdsFunc2.FieldByName('FATOR_RH').Value := qryFunc1.FieldByName('FATOR_RH').AsString;

        if not qryFunc1.FieldByName('LOGRADOURO').IsNull then
          cdsFunc2.FieldByName('LOGRADOURO').Value := qryFunc1.FieldByName('LOGRADOURO').AsString;

        if not qryFunc1.FieldByName('NUM_LOGRADOURO').IsNull then
          cdsFunc2.FieldByName('NUM_LOGRADOURO').Value := qryFunc1.FieldByName('NUM_LOGRADOURO').AsString;

        if not qryFunc1.FieldByName('COMPLEMENTO_LOGRADOURO').IsNull then
          cdsFunc2.FieldByName('COMPLEMENTO_LOGRADOURO').Value := qryFunc1.FieldByName('COMPLEMENTO_LOGRADOURO').AsString;

        if not qryFunc1.FieldByName('BAIRRO_LOGRADOURO').IsNull then
          cdsFunc2.FieldByName('BAIRRO_LOGRADOURO').Value := qryFunc1.FieldByName('BAIRRO_LOGRADOURO').AsString;

        if not qryFunc1.FieldByName('CEP_LOGR').IsNull then
          cdsFunc2.FieldByName('CEP_LOGR').Value := qryFunc1.FieldByName('CEP_LOGR').AsString;

        if not qryFunc1.FieldByName('COD_MUNICIPIO_LOGR').IsNull then
          cdsFunc2.FieldByName('COD_MUNICIPIO_LOGR').Value := qryFunc1.FieldByName('COD_MUNICIPIO_LOGR').AsString;

        if not qryFunc1.FieldByName('SIGLA_ESTADO_LOGR').IsNull then
          cdsFunc2.FieldByName('SIGLA_ESTADO_LOGR').Value := qryFunc1.FieldByName('SIGLA_ESTADO_LOGR').AsString;

        if not qryFunc1.FieldByName('TELEFONE').IsNull then
          cdsFunc2.FieldByName('TELEFONE').Value := qryFunc1.FieldByName('TELEFONE').AsString;

        if not qryFunc1.FieldByName('CELULAR').IsNull then
          cdsFunc2.FieldByName('CELULAR').Value := qryFunc1.FieldByName('CELULAR').AsString;

        if not qryFunc1.FieldByName('EMAIL').IsNull then
          cdsFunc2.FieldByName('EMAIL').Value := qryFunc1.FieldByName('EMAIL').AsString;

        if not qryFunc1.FieldByName('ID_CARGO_FK').IsNull then
          cdsFunc2.FieldByName('ID_CARGO_FK').Value := qryFunc1.FieldByName('ID_CARGO_FK').AsInteger;

        if not qryFunc1.FieldByName('COD_ESCOLARIDADE').IsNull then
          cdsFunc2.FieldByName('COD_ESCOLARIDADE').Value := qryFunc1.FieldByName('COD_ESCOLARIDADE').AsInteger;

        if not qryFunc1.FieldByName('COD_ESTADOCIVIL').IsNull then
          cdsFunc2.FieldByName('COD_ESTADOCIVIL').Value := qryFunc1.FieldByName('COD_ESTADOCIVIL').AsInteger;

        if not qryFunc1.FieldByName('QTD_FILHOS').IsNull then
          cdsFunc2.FieldByName('QTD_FILHOS').Value := qryFunc1.FieldByName('QTD_FILHOS').AsInteger;

        if not qryFunc1.FieldByName('QTD_DEPENDENTES').IsNull then
          cdsFunc2.FieldByName('QTD_DEPENDENTES').Value := qryFunc1.FieldByName('QTD_DEPENDENTES').AsInteger;

        if not qryFunc1.FieldByName('HORA_ENT_TRAB').IsNull then
          cdsFunc2.FieldByName('HORA_ENT_TRAB').AsString := qryFunc1.FieldByName('HORA_ENT_TRAB').AsString;

        if not qryFunc1.FieldByName('HORA_SAI_ALMOCO').IsNull then
           cdsFunc2.FieldByName('HORA_SAI_ALMOCO').AsString := qryFunc1.FieldByName('HORA_SAI_ALMOCO').AsString;

        if not qryFunc1.FieldByName('HORA_ENT_ALMOCO').IsNull then
          cdsFunc2.FieldByName('HORA_ENT_ALMOCO').AsString := qryFunc1.FieldByName('HORA_ENT_ALMOCO').AsString;

        if not qryFunc1.FieldByName('HORA_SAI_TRAB').IsNull then
          cdsFunc2.FieldByName('HORA_SAI_TRAB').AsString := qryFunc1.FieldByName('HORA_SAI_TRAB').AsString;

        if not qryFunc1.FieldByName('VR_SALARIO').IsNull then
          cdsFunc2.FieldByName('VR_SALARIO').Value := qryFunc1.FieldByName('VR_SALARIO').AsCurrency;

        if not qryFunc1.FieldByName('VR_MAXIMO_VALE').IsNull then
          cdsFunc2.FieldByName('VR_MAXIMO_VALE').Value := qryFunc1.FieldByName('VR_MAXIMO_VALE').AsCurrency;

        if not qryFunc1.FieldByName('SEXO').IsNull then
          cdsFunc2.FieldByName('SEXO').Value := qryFunc1.FieldByName('SEXO').AsString;

        if not qryFunc1.FieldByName('DATA_ADMISSAO').IsNull then
          cdsFunc2.FieldByName('DATA_ADMISSAO').Value := qryFunc1.FieldByName('DATA_ADMISSAO').AsDateTime;

        if not qryFunc1.FieldByName('DATA_CADASTRO').IsNull then
          cdsFunc2.FieldByName('DATA_CADASTRO').Value := qryFunc1.FieldByName('DATA_CADASTRO').AsDateTime;

        if not qryFunc1.FieldByName('DATA_DESLIGAMENTO').IsNull then
          cdsFunc2.FieldByName('DATA_DESLIGAMENTO').Value := qryFunc1.FieldByName('DATA_DESLIGAMENTO').AsDateTime;

        if not qryFunc1.FieldByName('FLG_ATIVO').IsNull then
          cdsFunc2.FieldByName('FLG_ATIVO').Value := qryFunc1.FieldByName('FLG_ATIVO').AsString;

        cdsFunc2.Post;
        cdsFunc2.ApplyUpdates(0);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        Op := vgOperacao;
        vgOperacao := I;
        BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Colaborador',
                            qryFunc1.FieldByName('ID_FUNCIONARIO').AsInteger,
                            'FUNCIONARIO');
        vgOperacao := Op;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao incluir o ID ' + sId + ' da tabela FUNCIONARIO do GERENCIAL ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end
    else  { Traz os registros que existem em ambos os bancos, compara os campos e atualiza no MONITORAMENTO }
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        cdsFunc2.Edit;
        if qryFunc1.FieldByName('NOME_FUNCIONARIO').AsString <> cdsFunc2.FieldByName('NOME_FUNCIONARIO').AsString then
          cdsFunc2.FieldByName('NOME_FUNCIONARIO').Value := qryFunc1.FieldByName('NOME_FUNCIONARIO').AsString;

        if qryFunc1.FieldByName('DATA_NASCIMENTO').AsDateTime <> cdsFunc2.FieldByName('DATA_NASCIMENTO').AsDateTime then
          cdsFunc2.FieldByName('DATA_NASCIMENTO').Value := qryFunc1.FieldByName('DATA_NASCIMENTO').AsDateTime;

        if qryFunc1.FieldByName('NUM_IDENTIDADE').AsString <> cdsFunc2.FieldByName('NUM_IDENTIDADE').AsString then
          cdsFunc2.FieldByName('NUM_IDENTIDADE').Value := qryFunc1.FieldByName('NUM_IDENTIDADE').AsString;

        if qryFunc1.FieldByName('ORGAO_EMISSOR').AsString <> cdsFunc2.FieldByName('ORGAO_EMISSOR').AsString then
          cdsFunc2.FieldByName('ORGAO_EMISSOR').Value := qryFunc1.FieldByName('ORGAO_EMISSOR').AsString;

        if qryFunc1.FieldByName('DATA_EMISSAO_IDENTIDADE').AsDateTime <> cdsFunc2.FieldByName('DATA_EMISSAO_IDENTIDADE').AsDateTime then
          cdsFunc2.FieldByName('DATA_EMISSAO_IDENTIDADE').Value := qryFunc1.FieldByName('DATA_EMISSAO_IDENTIDADE').AsDateTime;

        if qryFunc1.FieldByName('CPF').AsString <> cdsFunc2.FieldByName('CPF').AsString then
          cdsFunc2.FieldByName('CPF').Value := qryFunc1.FieldByName('CPF').AsString;

        if qryFunc1.FieldByName('NUM_CTPS').AsString <> cdsFunc2.FieldByName('NUM_CTPS').AsString then
          cdsFunc2.FieldByName('NUM_CTPS').Value := qryFunc1.FieldByName('NUM_CTPS').AsString;

        if qryFunc1.FieldByName('SERIE_CTPS').AsString <> cdsFunc2.FieldByName('SERIE_CTPS').AsString then
          cdsFunc2.FieldByName('SERIE_CTPS').Value := qryFunc1.FieldByName('SERIE_CTPS').AsString;

        if qryFunc1.FieldByName('DATA_CTPS').AsDateTime <> cdsFunc2.FieldByName('DATA_CTPS').AsDateTime then
          cdsFunc2.FieldByName('DATA_CTPS').Value := qryFunc1.FieldByName('DATA_CTPS').AsDateTime;

        if qryFunc1.FieldByName('NUM_PIS').AsString <> cdsFunc2.FieldByName('NUM_PIS').AsString then
          cdsFunc2.FieldByName('NUM_PIS').Value := qryFunc1.FieldByName('NUM_PIS').AsString;

        if qryFunc1.FieldByName('SIGLA_ESTADO_CTPS').AsString <> cdsFunc2.FieldByName('SIGLA_ESTADO_CTPS').AsString then
          cdsFunc2.FieldByName('SIGLA_ESTADO_CTPS').Value := qryFunc1.FieldByName('SIGLA_ESTADO_CTPS').AsString;

        if qryFunc1.FieldByName('NUM_TITULO_ELEITOR').AsString <> cdsFunc2.FieldByName('NUM_TITULO_ELEITOR').AsString then
          cdsFunc2.FieldByName('NUM_TITULO_ELEITOR').Value := qryFunc1.FieldByName('NUM_TITULO_ELEITOR').AsString;

        if qryFunc1.FieldByName('ZONA_TE').AsString <> cdsFunc2.FieldByName('ZONA_TE').AsString then
          cdsFunc2.FieldByName('ZONA_TE').Value := qryFunc1.FieldByName('ZONA_TE').AsString;

        if qryFunc1.FieldByName('SECAO_TE').AsString <> cdsFunc2.FieldByName('SECAO_TE').AsString then
          cdsFunc2.FieldByName('SECAO_TE').Value := qryFunc1.FieldByName('SECAO_TE').AsString;

        if qryFunc1.FieldByName('COD_MUNICIPIO_TE').AsString <> cdsFunc2.FieldByName('COD_MUNICIPIO_TE').AsString then
          cdsFunc2.FieldByName('COD_MUNICIPIO_TE').Value := qryFunc1.FieldByName('COD_MUNICIPIO_TE').AsString;

        if qryFunc1.FieldByName('SIGLA_ESTADO_TE').AsString <> cdsFunc2.FieldByName('SIGLA_ESTADO_TE').AsString then
          cdsFunc2.FieldByName('SIGLA_ESTADO_TE').Value := qryFunc1.FieldByName('SIGLA_ESTADO_TE').AsString;

        if qryFunc1.FieldByName('DATA_EMISSAO_TE').AsDateTime <> cdsFunc2.FieldByName('DATA_EMISSAO_TE').AsDateTime then
          cdsFunc2.FieldByName('DATA_EMISSAO_TE').Value := qryFunc1.FieldByName('DATA_EMISSAO_TE').AsDateTime;

        if qryFunc1.FieldByName('TIPO_SANGUINEO').AsString <> cdsFunc2.FieldByName('TIPO_SANGUINEO').AsString then
          cdsFunc2.FieldByName('TIPO_SANGUINEO').Value := qryFunc1.FieldByName('TIPO_SANGUINEO').AsString;

        if qryFunc1.FieldByName('FATOR_RH').AsString <> cdsFunc2.FieldByName('FATOR_RH').AsString then
          cdsFunc2.FieldByName('FATOR_RH').Value := qryFunc1.FieldByName('FATOR_RH').AsString;

        if qryFunc1.FieldByName('LOGRADOURO').AsString <> cdsFunc2.FieldByName('LOGRADOURO').AsString then
          cdsFunc2.FieldByName('LOGRADOURO').Value := qryFunc1.FieldByName('LOGRADOURO').AsString;

        if qryFunc1.FieldByName('NUM_LOGRADOURO').AsString <> cdsFunc2.FieldByName('NUM_LOGRADOURO').AsString then
          cdsFunc2.FieldByName('NUM_LOGRADOURO').Value := qryFunc1.FieldByName('NUM_LOGRADOURO').AsString;

        if qryFunc1.FieldByName('COMPLEMENTO_LOGRADOURO').AsString <> cdsFunc2.FieldByName('COMPLEMENTO_LOGRADOURO').AsString then
          cdsFunc2.FieldByName('COMPLEMENTO_LOGRADOURO').Value := qryFunc1.FieldByName('COMPLEMENTO_LOGRADOURO').AsString;

        if qryFunc1.FieldByName('BAIRRO_LOGRADOURO').AsString <> cdsFunc2.FieldByName('BAIRRO_LOGRADOURO').AsString then
          cdsFunc2.FieldByName('BAIRRO_LOGRADOURO').Value := qryFunc1.FieldByName('BAIRRO_LOGRADOURO').AsString;

        if qryFunc1.FieldByName('CEP_LOGR').AsString <> cdsFunc2.FieldByName('CEP_LOGR').AsString then
          cdsFunc2.FieldByName('CEP_LOGR').Value := qryFunc1.FieldByName('CEP_LOGR').AsString;

        if qryFunc1.FieldByName('COD_MUNICIPIO_LOGR').AsString <> cdsFunc2.FieldByName('COD_MUNICIPIO_LOGR').AsString then
          cdsFunc2.FieldByName('COD_MUNICIPIO_LOGR').Value := qryFunc1.FieldByName('COD_MUNICIPIO_LOGR').AsString;

        if qryFunc1.FieldByName('SIGLA_ESTADO_LOGR').AsString <> cdsFunc2.FieldByName('SIGLA_ESTADO_LOGR').AsString then
          cdsFunc2.FieldByName('SIGLA_ESTADO_LOGR').Value := qryFunc1.FieldByName('SIGLA_ESTADO_LOGR').AsString;

        if qryFunc1.FieldByName('TELEFONE').AsString <> cdsFunc2.FieldByName('TELEFONE').AsString then
          cdsFunc2.FieldByName('TELEFONE').Value := qryFunc1.FieldByName('TELEFONE').AsString;

        if qryFunc1.FieldByName('CELULAR').AsString <> cdsFunc2.FieldByName('CELULAR').AsString then
          cdsFunc2.FieldByName('CELULAR').Value := qryFunc1.FieldByName('CELULAR').AsString;

        if qryFunc1.FieldByName('EMAIL').AsString <> cdsFunc2.FieldByName('EMAIL').AsString then
          cdsFunc2.FieldByName('EMAIL').Value := qryFunc1.FieldByName('EMAIL').AsString;

        if qryFunc1.FieldByName('ID_CARGO_FK').AsInteger <> cdsFunc2.FieldByName('ID_CARGO_FK').AsInteger then
          cdsFunc2.FieldByName('ID_CARGO_FK').Value := qryFunc1.FieldByName('ID_CARGO_FK').AsInteger;

        if qryFunc1.FieldByName('COD_ESCOLARIDADE').AsInteger <> cdsFunc2.FieldByName('COD_ESCOLARIDADE').AsInteger then
          cdsFunc2.FieldByName('COD_ESCOLARIDADE').Value := qryFunc1.FieldByName('COD_ESCOLARIDADE').AsInteger;

        if qryFunc1.FieldByName('COD_ESTADOCIVIL').AsInteger <> cdsFunc2.FieldByName('COD_ESTADOCIVIL').AsInteger then
          cdsFunc2.FieldByName('COD_ESTADOCIVIL').Value := qryFunc1.FieldByName('COD_ESTADOCIVIL').AsInteger;

        if qryFunc1.FieldByName('QTD_FILHOS').AsInteger <> cdsFunc2.FieldByName('QTD_FILHOS').AsInteger then
          cdsFunc2.FieldByName('QTD_FILHOS').Value := qryFunc1.FieldByName('QTD_FILHOS').AsInteger;

        if qryFunc1.FieldByName('QTD_DEPENDENTES').AsInteger <> cdsFunc2.FieldByName('QTD_DEPENDENTES').AsInteger then
          cdsFunc2.FieldByName('QTD_DEPENDENTES').Value := qryFunc1.FieldByName('QTD_DEPENDENTES').AsInteger;

        if qryFunc1.FieldByName('HORA_ENT_TRAB').AsString <> cdsFunc2.FieldByName('HORA_ENT_TRAB').AsString then
          cdsFunc2.FieldByName('HORA_ENT_TRAB').AsString := qryFunc1.FieldByName('HORA_ENT_TRAB').AsString;

        if qryFunc1.FieldByName('HORA_SAI_ALMOCO').AsString <> cdsFunc2.FieldByName('HORA_SAI_ALMOCO').AsString then
           cdsFunc2.FieldByName('HORA_SAI_ALMOCO').AsString := qryFunc1.FieldByName('HORA_SAI_ALMOCO').AsString;

        if qryFunc1.FieldByName('HORA_ENT_ALMOCO').AsString <> cdsFunc2.FieldByName('HORA_ENT_ALMOCO').AsString then
          cdsFunc2.FieldByName('HORA_ENT_ALMOCO').AsString := qryFunc1.FieldByName('HORA_ENT_ALMOCO').AsString;

        if qryFunc1.FieldByName('HORA_SAI_TRAB').AsString <> cdsFunc2.FieldByName('HORA_SAI_TRAB').AsString then
          cdsFunc2.FieldByName('HORA_SAI_TRAB').AsString := qryFunc1.FieldByName('HORA_SAI_TRAB').AsString;

        if qryFunc1.FieldByName('VR_SALARIO').AsCurrency <> cdsFunc2.FieldByName('VR_SALARIO').AsCurrency then
          cdsFunc2.FieldByName('VR_SALARIO').Value := qryFunc1.FieldByName('VR_SALARIO').AsCurrency;

        if qryFunc1.FieldByName('VR_MAXIMO_VALE').AsCurrency <> cdsFunc2.FieldByName('VR_MAXIMO_VALE').AsCurrency then
          cdsFunc2.FieldByName('VR_MAXIMO_VALE').Value := qryFunc1.FieldByName('VR_MAXIMO_VALE').AsCurrency;

        if qryFunc1.FieldByName('SEXO').AsString <> cdsFunc2.FieldByName('SEXO').AsString then
          cdsFunc2.FieldByName('SEXO').Value := qryFunc1.FieldByName('SEXO').AsString;

        if qryFunc1.FieldByName('DATA_ADMISSAO').AsDateTime <> cdsFunc2.FieldByName('DATA_ADMISSAO').AsDateTime then
          cdsFunc2.FieldByName('DATA_ADMISSAO').Value := qryFunc1.FieldByName('DATA_ADMISSAO').AsDateTime;

        if qryFunc1.FieldByName('DATA_CADASTRO').AsDateTime <> cdsFunc2.FieldByName('DATA_CADASTRO').AsDateTime then
          cdsFunc2.FieldByName('DATA_CADASTRO').Value := qryFunc1.FieldByName('DATA_CADASTRO').AsDateTime;

        if qryFunc1.FieldByName('DATA_DESLIGAMENTO').AsDateTime <> cdsFunc2.FieldByName('DATA_DESLIGAMENTO').AsDateTime then
          cdsFunc2.FieldByName('DATA_DESLIGAMENTO').Value := qryFunc1.FieldByName('DATA_DESLIGAMENTO').AsDateTime;

        if qryFunc1.FieldByName('FLG_ATIVO').AsString <> cdsFunc2.FieldByName('FLG_ATIVO').AsString then
          cdsFunc2.FieldByName('FLG_ATIVO').Value := qryFunc1.FieldByName('FLG_ATIVO').AsString;

        cdsFunc2.Post;
        cdsFunc2.ApplyUpdates(0);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        Op := vgOperacao;
        vgOperacao := E;
        BS.GravarUsuarioLog(80, '', 'Edi��o AUTOM�TICA de Colaborador',
                            qryFunc1.FieldByName('ID_FUNCIONARIO').AsInteger,
                            'FUNCIONARIO');
        vgOperacao := Op;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao alterar os dados do registro de ID ' + sId + ' da tabela FUNCIONARIO do GERENCIAL ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end;

    qryFunc1.Next;
  end;

  { FUNCIONARIO_COMISSAO }
  if vgConf_FlgTrabalhaComissao = 'S' then
  begin
    qryFuncCom1.Close;
    qryFuncCom1.Open;
    qryFuncCom1.Last;

    sQtd := IntToStr(qryFuncCom1.RecordCount);

    qryFuncCom1.First;

    cdsFuncCom2.Close;
    cdsFuncCom2.Open;
    cdsFuncCom2.First;

    { Verifica se o registro existe no MONITORAMENTO e inclui, caso nao exista }
    while not qryFuncCom1.Eof do
    begin
{      FPrincipal.mmProcessos.Lines.Add('Sincronizando as tabelas gerais do GERENCIAL... ' +
                                       IntToStr(qryFuncCom1.RecNo) + '/' + sQtd +
                                       ' (Tabela: FUNCIONARIO_COMISSAO)');  }

    vgTextoMemo := vgTextoMemo + #13#10 + 'Sincronizando as tabelas gerais do GERENCIAL... ' +
                                          IntToStr(qryFuncCom1.RecNo) + '/' + sQtd +
                                          ' (Tabela: FUNCIONARIO_COMISSAO)';

      sId := qryFuncCom1.FieldByName('ID_FUNCIONARIO_COMISSAO').AsString;

      if not cdsFuncCom2.Locate('ID_FUNCIONARIO_COMISSAO',
                                qryFuncCom1.FieldByName('ID_FUNCIONARIO_COMISSAO').AsInteger,
                                [loCaseInsensitive, loPartialKey]) then
      begin
        try
          if vgConSISTEMA.Connected then
            vgConSISTEMA.StartTransaction;

          cdsFuncCom2.Append;
          cdsFuncCom2.FieldByName('ID_FUNCIONARIO_COMISSAO').Value := qryFuncCom1.FieldByName('ID_FUNCIONARIO_COMISSAO').AsInteger;
          cdsFuncCom2.FieldByName('ID_FUNCIONARIO_FK').Value       := qryFuncCom1.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
          cdsFuncCom2.FieldByName('PERCENT_COMISSAO').Value        := qryFuncCom1.FieldByName('PERCENT_COMISSAO').AsCurrency;
          cdsFuncCom2.FieldByName('ID_SISTEMA_FK').Value           := qryFuncCom1.FieldByName('ID_SISTEMA_FK').AsInteger;
          cdsFuncCom2.FieldByName('ID_ITEM_FK').Value              := qryFuncCom1.FieldByName('ID_ITEM_FK').AsInteger;
          cdsFuncCom2.FieldByName('COD_ADICIONAL').Value           := qryFuncCom1.FieldByName('COD_ADICIONAL').AsInteger;
          cdsFuncCom2.Post;
          cdsFuncCom2.ApplyUpdates(0);

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Commit;

          Op := vgOperacao;
          vgOperacao := I;
          BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Comiss�o de Colaborador',
                              qryFuncCom1.FieldByName('ID_FUNCIONARIO_COMISSAO').AsInteger,
                              'FUNCIONARIO_COMISSAO');
          vgOperacao := Op;
        except
          on E: Exception do
          begin
            if vgConSISTEMA.InTransaction then
              vgConSISTEMA.Rollback;

            GravarLogErro(E.Message);

            Msg := 'Erro ao incluir o ID ' + sId + ' da tabela FUNCIONARIO_COMISSAO do GERENCIAL ' +
                   'para o MONITORAMENTO. ' + #13#10 +
                   'Por favor, entre em contato com o Suporte. ' + #13#10 +
                   E.Message;

            Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
            Application.ProcessMessages;
          end;
        end;
      end
      else  { Traz os registros que existem em ambos os bancos, compara os campos e atualiza no MONITORAMENTO }
      begin
        try
          if vgConSISTEMA.Connected then
            vgConSISTEMA.StartTransaction;

          cdsFuncCom2.Edit;
          if qryFuncCom1.FieldByName('ID_FUNCIONARIO_FK').AsInteger <> cdsFuncCom2.FieldByName('ID_FUNCIONARIO_FK').AsInteger then
            cdsFuncCom2.FieldByName('ID_FUNCIONARIO_FK').Value := qryFuncCom1.FieldByName('ID_FUNCIONARIO_FK').AsInteger;

          if qryFuncCom1.FieldByName('PERCENT_COMISSAO').AsCurrency <> cdsFuncCom2.FieldByName('PERCENT_COMISSAO').AsCurrency then
            cdsFuncCom2.FieldByName('PERCENT_COMISSAO').AsCurrency := qryFuncCom1.FieldByName('PERCENT_COMISSAO').AsCurrency;

          if qryFuncCom1.FieldByName('ID_SISTEMA_FK').AsInteger <> cdsFuncCom2.FieldByName('ID_SISTEMA_FK').AsInteger then
            cdsFuncCom2.FieldByName('ID_SISTEMA_FK').AsInteger := qryFuncCom1.FieldByName('ID_SISTEMA_FK').AsInteger;

          if qryFuncCom1.FieldByName('ID_ITEM_FK').AsInteger <> cdsFuncCom2.FieldByName('ID_ITEM_FK').AsInteger then
            cdsFuncCom2.FieldByName('ID_ITEM_FK').AsInteger := qryFuncCom1.FieldByName('ID_ITEM_FK').AsInteger;

          if qryFuncCom1.FieldByName('COD_ADICIONAL').AsInteger <> cdsFuncCom2.FieldByName('COD_ADICIONAL').AsInteger then
            cdsFuncCom2.FieldByName('COD_ADICIONAL').AsInteger := qryFuncCom1.FieldByName('COD_ADICIONAL').AsInteger;

          cdsFuncCom2.Post;
          cdsFuncCom2.ApplyUpdates(0);

          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Commit;

          Op := vgOperacao;
          vgOperacao := E;
          BS.GravarUsuarioLog(80, '', 'Edi��o AUTOM�TICA de Comiss�o de Colaborador',
                              qryFuncCom1.FieldByName('ID_FUNCIONARIO_COMISSAO').AsInteger,
                              'FUNCIONARIO_COMISSAO');
          vgOperacao := Op;
        except
          on E: Exception do
          begin
            if vgConSISTEMA.InTransaction then
              vgConSISTEMA.Rollback;

            GravarLogErro(E.Message);

            Msg := 'Erro ao alterar os dados do registro de ID ' + sId + ' da tabela FUNCIONARIO_COMISSAO do GERENCIAL ' +
                   'para o MONITORAMENTO. ' + #13#10 +
                   'Por favor, entre em contato com o Suporte. ' + #13#10 +
                   E.Message;

            Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
            Application.ProcessMessages;
          end;
        end;
      end;

      qryFuncCom1.Next;
    end;
  end;

  { USUARIO }
  qryUsu1.Close;
  qryUsu1.Open;
  qryUsu1.Last;

  sQtd := IntToStr(qryUsu1.RecordCount);

  qryUsu1.First;

  cdsUsu2.Close;
  cdsUsu2.Open;
  cdsUsu2.First;

  { Verifica se o registro existe no MONITORAMENTO e inclui, caso nao exista }
  while not qryUsu1.Eof do
  begin
{    FPrincipal.mmProcessos.Lines.Add('Sincronizando as tabelas gerais do GERENCIAL... ' +
                                     IntToStr(qryUsu1.RecNo) + '/' + sQtd +
                                     ' (Tabela: USUARIO)');  }

    vgTextoMemo := vgTextoMemo + #13#10 + 'Sincronizando as tabelas gerais do GERENCIAL... ' +
                                          IntToStr(qryUsu1.RecNo) + '/' + sQtd +
                                          ' (Tabela: USUARIO)';

    sId := qryUsu1.FieldByName('ID_USUARIO').AsString;

    if not cdsUsu2.Locate('ID_USUARIO',
                          qryUsu1.FieldByName('ID_USUARIO').AsInteger,
                          [loCaseInsensitive, loPartialKey]) then
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        cdsUsu2.Append;

        cdsUsu2.FieldByName('ID_USUARIO').Value := qryUsu1.FieldByName('ID_USUARIO').AsInteger;

        if not qryUsu1.FieldByName('NOME').IsNull then
          cdsUsu2.FieldByName('NOME').Value := qryUsu1.FieldByName('NOME').AsString;

        if not qryUsu1.FieldByName('CPF').IsNull then
          cdsUsu2.FieldByName('CPF').Value := qryUsu1.FieldByName('CPF').AsString;

        if not qryUsu1.FieldByName('SENHA').IsNull then
          cdsUsu2.FieldByName('SENHA').Value := qryUsu1.FieldByName('SENHA').AsString;

        if (not qryUsu1.FieldByName('QUALIFICACAO').IsNull) and
          (qryUsu1.FieldByName('QUALIFICACAO').AsString <> '') then
          cdsUsu2.FieldByName('QUALIFICACAO').Value := qryUsu1.FieldByName('QUALIFICACAO').AsString
        else
          cdsUsu2.FieldByName('QUALIFICACAO').Value := 'N�O INFORMADA';

        if not qryUsu1.FieldByName('FLG_MASTER').IsNull then
          cdsUsu2.FieldByName('FLG_MASTER').Value := qryUsu1.FieldByName('FLG_MASTER').AsString;

        if not qryUsu1.FieldByName('MATRICULA').IsNull then
          cdsUsu2.FieldByName('MATRICULA').Value := qryUsu1.FieldByName('MATRICULA').AsString;

        if not qryUsu1.FieldByName('FLG_SUPORTE').IsNull then
          cdsUsu2.FieldByName('FLG_SUPORTE').Value := qryUsu1.FieldByName('FLG_SUPORTE').AsString;

        if not qryUsu1.FieldByName('FLG_VISUALIZOU_ATU').IsNull then
          cdsUsu2.FieldByName('FLG_VISUALIZOU_ATU').Value := qryUsu1.FieldByName('FLG_VISUALIZOU_ATU').AsString;

        if not qryUsu1.FieldByName('FLG_SEXO').IsNull then
          cdsUsu2.FieldByName('FLG_SEXO').Value := qryUsu1.FieldByName('FLG_SEXO').AsString;

        if not qryUsu1.FieldByName('ID_FUNCIONARIO_FK').IsNull then
          cdsUsu2.FieldByName('ID_FUNCIONARIO_FK').Value := qryUsu1.FieldByName('ID_FUNCIONARIO_FK').AsInteger;

        if not qryUsu1.FieldByName('FLG_ATIVO').IsNull then
          cdsUsu2.FieldByName('FLG_ATIVO').Value := qryUsu1.FieldByName('FLG_ATIVO').AsString;

        if not qryUsu1.FieldByName('DATA_CADASTRO').IsNull then
          cdsUsu2.FieldByName('DATA_CADASTRO').Value := qryUsu1.FieldByName('DATA_CADASTRO').AsDateTime;

        if not qryUsu1.FieldByName('DATA_INATIVACAO').IsNull then
          cdsUsu2.FieldByName('DATA_INATIVACAO').Value := qryUsu1.FieldByName('DATA_INATIVACAO').AsDateTime;

        cdsUsu2.Post;
        cdsUsu2.ApplyUpdates(0);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        Op := vgOperacao;
        vgOperacao := I;
        BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Usu�rio',
                            qryUsu1.FieldByName('ID_USUARIO').AsInteger,
                            'USUARIO');
        vgOperacao := Op;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao incluir o ID ' + sId + ' da tabela USUARIO do GERENCIAL ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end
    else  { Traz os registros que existem em ambos os bancos, compara os campos e atualiza no MONITORAMENTO }
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        cdsUsu2.Edit;
        if qryUsu1.FieldByName('NOME').AsString <> cdsUsu2.FieldByName('NOME').AsString then
          cdsUsu2.FieldByName('NOME').Value := qryUsu1.FieldByName('NOME').AsString;

        if qryUsu1.FieldByName('CPF').AsString <> cdsUsu2.FieldByName('CPF').AsString then
          cdsUsu2.FieldByName('CPF').Value := qryUsu1.FieldByName('CPF').AsString;

        if qryUsu1.FieldByName('SENHA').AsString <> cdsUsu2.FieldByName('SENHA').AsString then
          cdsUsu2.FieldByName('SENHA').Value := qryUsu1.FieldByName('SENHA').AsString;

        if (qryUsu1.FieldByName('QUALIFICACAO').AsString <> cdsUsu2.FieldByName('QUALIFICACAO').AsString) and
          (qryUsu1.FieldByName('QUALIFICACAO').AsString = '') then
          cdsUsu2.FieldByName('QUALIFICACAO').Value := qryUsu1.FieldByName('QUALIFICACAO').AsString;

        if qryUsu1.FieldByName('FLG_MASTER').AsString <> cdsUsu2.FieldByName('FLG_MASTER').AsString then
          cdsUsu2.FieldByName('FLG_MASTER').Value := qryUsu1.FieldByName('FLG_MASTER').AsString;

        if qryUsu1.FieldByName('MATRICULA').AsString <> cdsUsu2.FieldByName('MATRICULA').AsString then
          cdsUsu2.FieldByName('MATRICULA').Value := qryUsu1.FieldByName('MATRICULA').AsString;

        if qryUsu1.FieldByName('FLG_SUPORTE').AsString <> cdsUsu2.FieldByName('FLG_SUPORTE').AsString then
          cdsUsu2.FieldByName('FLG_SUPORTE').Value := qryUsu1.FieldByName('FLG_SUPORTE').AsString;

        if qryUsu1.FieldByName('FLG_VISUALIZOU_ATU').AsString <> cdsUsu2.FieldByName('FLG_VISUALIZOU_ATU').AsString then
          cdsUsu2.FieldByName('FLG_VISUALIZOU_ATU').Value := qryUsu1.FieldByName('FLG_VISUALIZOU_ATU').AsString;

        if qryUsu1.FieldByName('FLG_SEXO').AsString <> cdsUsu2.FieldByName('FLG_SEXO').AsString then
          cdsUsu2.FieldByName('FLG_SEXO').Value := qryUsu1.FieldByName('FLG_SEXO').AsString;

        if (qryUsu1.FieldByName('ID_FUNCIONARIO_FK').AsInteger <> cdsUsu2.FieldByName('ID_FUNCIONARIO_FK').AsInteger) and
          (qryUsu1.FieldByName('ID_FUNCIONARIO_FK').AsInteger > 0) then
          cdsUsu2.FieldByName('ID_FUNCIONARIO_FK').Value := qryUsu1.FieldByName('ID_FUNCIONARIO_FK').AsInteger;

        if qryUsu1.FieldByName('FLG_ATIVO').AsString <> cdsUsu2.FieldByName('FLG_ATIVO').AsString then
          cdsUsu2.FieldByName('FLG_ATIVO').Value := qryUsu1.FieldByName('FLG_ATIVO').AsString;

        if qryUsu1.FieldByName('DATA_CADASTRO').AsDateTime <> cdsUsu2.FieldByName('DATA_CADASTRO').AsDateTime then
          cdsUsu2.FieldByName('DATA_CADASTRO').Value := qryUsu1.FieldByName('DATA_CADASTRO').AsDateTime;

        if qryUsu1.FieldByName('DATA_INATIVACAO').AsDateTime <> cdsUsu2.FieldByName('DATA_INATIVACAO').AsDateTime then
          cdsUsu2.FieldByName('DATA_INATIVACAO').Value := qryUsu1.FieldByName('DATA_INATIVACAO').AsDateTime;

        cdsUsu2.Post;
        cdsUsu2.ApplyUpdates(0);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;

        Op := vgOperacao;
        vgOperacao := E;
        BS.GravarUsuarioLog(80, '', 'Edi��o AUTOM�TICA de Usu�rio',
                            qryUsu1.FieldByName('ID_USUARIO').AsInteger,
                            'USUARIO');
        vgOperacao := Op;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          GravarLogErro(E.Message);

          Msg := 'Erro ao alterar os dados do registro de ID ' + sId + ' da tabela USUARIO do GERENCIAL ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end;

    qryUsu1.Next;
  end;

  { As seguintes tabelas do GERENCIAL n�o s�o manipuladas pelo usuario, portanto,
    as atualizacoes que vierem a ser realizadas nas mesma, deverao ser realizadas
    tamb�m no MONITORAMENTO (caso o GERENCIAL susbstitua o MONITORAMENTO elas nao
    precisarao ser modificadas):
    MUNICIPIO, ESTADO, PAIS, OCUPACAO, SERV, ESTADOCIVIL, ESCOLARIDADE, TIPOFILIACAO,
    TIPODOCUMENTO, JUSTIFICATIVA_AUSENC_CPF, BANCO, SISTEMA, TIPO_ATO, SISTEMA, TIPO_ATO

    As seguintes tabelas do GERENCIAL devem ser copiadas integralmente para o MONITORAMENTO
    SOMENTE se o MONITORAMENTO for substituir o GERENCIAL eventualmente (no momento, sao
    utilizadas em mais de um programa, porem sempre dentro do banco de dados GERENCIAL e
    manipuladas somente nele):

    SELO_CCT, SERIE, COMPRA, PESSOA, PESSOADOCUMENTO, PESSOAFILIACAO', PESSOAGEMEO,
    HISTORICO_PESSOA }

  vgOperacao := Op;
end;

procedure TdmPrincipal.DataModuleCreate(Sender: TObject);
var
  Ini: TIniFile;
  sServ, sDB, Msg, Modulos: String;
  lAcaoOk, lMonitoramentoFechado: Boolean;
begin
  vgCorSistema := $007E7E7E;

  //Verifica se existe atualizacao disponivel
  dmGerencial.AtualizaExecutavelSistema('www\cartorios\StatusTotal\Sync',
                                        'SYNC_MONITORAMENTO.exe',
                                        'SYNC_MONITORAMENTO-update.exe',
                                        2);

  lAcaoOk   := False;

  Msg     := '';
  sServ   := '';
  sDB     := '';
  Modulos := '';

  vgImport_Rec  := False;
  vgImport_Fir  := False;
  vgImport_FirA := False;

  { VERIFICA SE O SISTEMA MONITORAMENTO ESTA EM EXECUCAO }
  try
    lMonitoramentoFechado := not ProcessoSistemaAtivo('MONITORAMENTO.exe');
  except
    lMonitoramentoFechado := False;
  end;

  if lMonitoramentoFechado then
  begin
    Application.MessageBox(PChar('N�o � poss�vel executar o SYNC_MONITORAMENTO ' +
                                 'sem que o sistema MONITORAMENTO esteja em ' +
                                 'funcionamento.'),
                           'Aviso',
                           MB_OK + MB_ICONWARNING);

    lAcaoOk := False;
  end
  else
    lAcaoOk := True;

  { CONEXAO COM OS BANCOS DE DADOS }
  if lAcaoOk then
  begin
    Ini := TIniFile.Create(vgDirExec + 'Config.ini');

    try
      { BANCO DE DADOS DE MONITORAMENTO }
      conSISTEMA.Close;

      sDB := Ini.ReadString('DB', 'PATH', '*');
      conSISTEMA.Params.Values['Database']     := dmGerencial.DecryptStr(sDB);

      sServ := Ini.ReadString('DB', 'SERV', '*');
      conSISTEMA.Params.Values['Server']       := dmGerencial.DecryptStr(sServ);  //localhost OU o nome do servidor OU o IP do servidor

      conSISTEMA.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
      conSISTEMA.Params.Values['SQLDialect']   := '3';
      conSISTEMA.Params.Values['CharacterSet'] := 'ISO8859_1';

      conSISTEMA.Connected := True;

      { Preencher as variaveis do Sistema }
      //GERAL
      vgConSISTEMA    := conSISTEMA;
      vgConSISTEMAAux := conSISTEMA;

      lAcaoOk := True;
    except
      on E: Exception do
      begin
        GravarLogErro(E.Message);

        Msg := 'Erro ao tentar conectar ao Banco de Dados MONITORAMENTO. ' + #13#10 +
               'Por favor, entre em contato com o Suporte. ' + #13#10 +
               E.Message;
        Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
        Ini.Free;
        Application.Terminate;
      end;
    end;
  end;

  { BANCO DE DADOS DE RECIBOS }
  if lAcaoOk then
  begin
    sDB := Ini.ReadString('DBR', 'PATH', '*');

    if Trim(sDB) = dmGerencial.EncryptStr('NAO*HA') then
      lAcaoOk := True
    else
    begin
      try
        conRECIBO.Close;

        sDB := Ini.ReadString('DBR', 'PATH', '*');
        conRECIBO.Params.Values['Database']     := dmGerencial.DecryptStr(sDB);

        sServ := Ini.ReadString('DBR', 'SERV', '*');
        conRECIBO.Params.Values['Server']       := dmGerencial.DecryptStr(sServ);  //localhost OU o nome do servidor OU o IP do servidor

        conRECIBO.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
        conRECIBO.Params.Values['SQLDialect']   := '3';
        conRECIBO.Params.Values['CharacterSet'] := 'ISO8859_1';

        conRECIBO.Connected := True;

        { Preencher as variaveis do Sistema }
        //GERAL
        vgConRECIBO    := conRECIBO;
        vgConRECIBOAux := conRECIBO;

        vgImport_Rec := True;
        lAcaoOk      := True;
      except
        on E: Exception do
        begin
          lAcaoOk := False;

          GravarLogErro(E.Message);

          Msg := 'Erro ao tentar conectar ao Banco de Dados RECIBO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;
          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Ini.Free;
          Application.Terminate;
        end;
      end;
    end;
  end;

  { BANCO DE DADOS DE FIRMAS }
  if lAcaoOk then
  begin
    sDB := Ini.ReadString('DBF', 'PATH', '*');

    if Trim(sDB) = dmGerencial.EncryptStr('NAO*HA') then
      lAcaoOk := True
    else
    begin
      try
        conFIRMAS.Close;

        sDB := Ini.ReadString('DBF', 'PATH', '*');
        conFIRMAS.Params.Values['Database']     := dmGerencial.DecryptStr(sDB);

        sServ := Ini.ReadString('DBF', 'SERV', '*');
        conFIRMAS.Params.Values['Server']       := dmGerencial.DecryptStr(sServ);  //localhost OU o nome do servidor OU o IP do servidor

        conFIRMAS.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
        conFIRMAS.Params.Values['SQLDialect']   := '3';
        conFIRMAS.Params.Values['CharacterSet'] := 'ISO8859_1';

        conFIRMAS.Connected := True;

        { Preencher as variaveis do Sistema }
        //GERAL
        vgConFIRMAS := conFIRMAS;

        vgImport_Fir := True;
        lAcaoOk      := True;
      except
        on E: Exception do
        begin
          lAcaoOk := False;

          GravarLogErro(E.Message);

          Msg := 'Erro ao tentar conectar ao Banco de Dados FIRMAS. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;
          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Ini.Free;
          Application.Terminate;
        end;
      end;
    end;
  end;

  { BANCO DE DADOS DE FIRMAS ANTIGO }
  if lAcaoOk then
  begin
    sDB := Ini.ReadString('DBFA', 'PATH', '*');

    if Trim(sDB) = dmGerencial.EncryptStr('NAO*HA') then
      lAcaoOk := True
    else
    begin
      try
        conFIRMAS_ANTIGO.Close;

        sDB := Ini.ReadString('DBFA', 'PATH', '*');
        conFIRMAS_ANTIGO.Params.Values['Database']     := dmGerencial.DecryptStr(sDB);

        sServ := Ini.ReadString('DBFA', 'SERV', '*');
        conFIRMAS_ANTIGO.Params.Values['Server']       := dmGerencial.DecryptStr(sServ);  //localhost OU o nome do servidor OU o IP do servidor

        conFIRMAS_ANTIGO.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
        conFIRMAS_ANTIGO.Params.Values['SQLDialect']   := '3';
        conFIRMAS_ANTIGO.Params.Values['CharacterSet'] := 'ISO8859_1';

        conFIRMAS_ANTIGO.Connected := True;

        { Preencher as variaveis do Sistema }
        //GERAL
        vgConFIRMASANT := conFIRMAS_ANTIGO;

        lAcaoOk := True;
      except
        on E: Exception do
        begin
          lAcaoOk := False;

          GravarLogErro(E.Message);

          Msg := 'Erro ao tentar conectar ao Banco de Dados FIRMAS (ANTIGO). ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;
          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Ini.Free;
          Application.Terminate;
        end;
      end;
    end;
  end;

  if lAcaoOk then
  begin
    sDB := Ini.ReadString('DBFAC', 'PATH', '*');

    if Trim(sDB) = dmGerencial.EncryptStr('NAO*HA') then
      lAcaoOk := True
    else
    begin
      try
        conFIRMAS_ACENTRAL.Close;

        sDB := Ini.ReadString('DBFAC', 'PATH', '*');
        conFIRMAS_ACENTRAL.Params.Values['Database']     := dmGerencial.DecryptStr(sDB);

        sServ := Ini.ReadString('DBFAC', 'SERV', '*');
        conFIRMAS_ACENTRAL.Params.Values['Server']       := dmGerencial.DecryptStr(sServ);  //localhost OU o nome do servidor OU o IP do servidor

        conFIRMAS_ACENTRAL.Params.Values['Protocol']     := 'Remote';  //Local ou Remote
        conFIRMAS_ACENTRAL.Params.Values['SQLDialect']   := '3';
        conFIRMAS_ACENTRAL.Params.Values['CharacterSet'] := 'ISO8859_1';

        conFIRMAS_ACENTRAL.Connected := True;

        { Preencher as variaveis do Sistema }
        //GERAL
        vgConFIRMASANTC := conFIRMAS_ACENTRAL;

        vgImport_FirA := True;
        lAcaoOk      := True;
      except
        on E: Exception do
        begin
          lAcaoOk := False;

          GravarLogErro(E.Message);

          Msg := 'Erro ao tentar conectar ao Banco de Dados TOTAL CENTRAL (FIRMAS ANTIGO). ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;
          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Ini.Free;
          Application.Terminate;
        end;
      end;
    end;
  end;

  if lAcaoOk then
  begin
    if FileExists('C:\FNMASTOTAL') then
    begin
      { Preencher as variaveis do Sistema }
      //USUARIO
      vgUsu_Id               := 1;
      vgUsu_Nome             := 'SUPORTE';
      vgUsu_CPF              := '999.999.999-99';
      vgUsu_Qualificacao     := 'SUPORTE';
      vgUsu_FlgMaster        := 'S';
      vgUsu_Matricula        := '';
      vgUsu_FlgSuporte       := 'S';
      vgUsu_FlgVisualizouAtu := 'S';
      vgUsu_FlgSexo          := 'I';
      vgUsu_IdFuncionario    := 0;
      vgUsu_FlgAtivo         := 'S';
      vgUsu_DataCadastro     := StrToDate('01/01/2016');
      vgUsu_DataInativacao   := 0;

      vgLoginOk := True;
    end
    else
      LoginSistema;
  end;

  if lAcaoOk then
  begin
    if vgLoginOk then
    begin
      BS.PreencherVariaveisSistema;
      Ini.Free;
    end
    else
    begin
      vgConf_DataIniImportacao := 0;
      Ini.Free;
      Application.Terminate;
    end;
  end
  else
  begin
    vgConf_DataIniImportacao := 0;
    Ini.Free;
    Application.Terminate;
  end;
end;

function TdmPrincipal.ExistemFirmasAntigasPendentes: Integer;
var
  QryConf, QryVerif: TFDQuery;
begin
  Result := 0;

  QryConf := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConf, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ULTRECIBOATUALF_A, ID_USUATUALIZAFIR_A ' +
            '  FROM CONFIGURACAO';
    Open;
  end;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConFIRMASANT);

  if (QryConf.FieldByName('ID_USUATUALIZAFIR_A').IsNull) or
    (QryConf.FieldByName('ID_USUATUALIZAFIR_A').AsInteger = vgUsu_Id) then
  begin
    with QryVerif, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT COUNT(TR.ID_ATO) AS QTD ' +
              '  FROM TREC TR ' +
              ' WHERE TR.ID_ATO > :ID_ATO ' +
              '   AND TR.DATA >= :DATA';
      Params.ParamByName('DATA').Value := vgConf_DataIniImportacao;
      Params.ParamByName('ID_ATO').Value := QryConf.FieldByName('ID_ULTRECIBOATUALF_A').AsInteger;
      Open;

      if QryVerif.RecordCount > 0 then
        Result := QryVerif.FieldByName('QTD').AsInteger;
    end;
  end;

  FreeAndNil(QryConf);
  FreeAndNil(QryVerif);
end;

function TdmPrincipal.ExistemFirmasCanceladas: String;
var
  QryConf, QryVerif, QryMon: TFDQuery;
  sRec: String;
begin
  Result := '';
  sRec   := '';

  QryConf := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConf, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ULTRECIBOATUALF, ID_USUATUALIZAFIR ' +
            '  FROM CONFIGURACAO';
    Open;
  end;

  if (QryConf.FieldByName('ID_ULTRECIBOATUALF').IsNull) or
    (QryConf.FieldByName('ID_ULTRECIBOATUALF').AsInteger = 0) then
    Exit;


  QryVerif := dmGerencial.CriarFDQuery(nil, vgConFIRMAS);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT S.ID_SERVICO, ' +
            '       S.ANO ' +
            '  FROM SERVICOS S ' +
            '  LEFT JOIN ATOS A ' +
            '  ON S.ID_SERVICO = A.ID_SERVICO ' +
            ' WHERE S.ID_SERVICO <= :ID_SERVICO ' +
            '   AND S.ANO <= :ANO ' +
            '   AND S.TOTAL IS NOT NULL ' +
            '   AND S.DATA >= :DATA ' +
            '   AND (TRIM(S.STATUS) = ' + QuotedStr('CANCELADO') +
            '    OR  TRIM(A.STATUS) = ' + QuotedStr('CANCELADO') + ')';
    Params.ParamByName('DATA').Value       := vgConf_DataIniImportacao;
    Params.ParamByName('ID_SERVICO').Value := StrToInt(Copy(QryConf.FieldByName('ID_ULTRECIBOATUALF').AsString,
                                                            5,
                                                            (Length(QryConf.FieldByName('ID_ULTRECIBOATUALF').AsString) - 4)));
    Params.ParamByName('ANO').Value        := StrToInt(Copy(QryConf.FieldByName('ID_ULTRECIBOATUALF').AsString,
                                                            1,
                                                            4));
    Open;
  end;

  if QryVerif.RecordCount > 0 then
  begin
    while not QryVerif.Eof do
    begin
      if Trim(sRec) = '' then
        sRec := (QryVerif.FieldByName('ANO').AsString + QryVerif.FieldByName('ID_SERVICO').AsString)
      else
        sRec := (sRec + ', ' + (QryVerif.FieldByName('ANO').AsString + QryVerif.FieldByName('ID_SERVICO').AsString));

      QryVerif.Next;
    end;

    QryMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    with QryMon, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ORIGEM ' +
              '  FROM LANCAMENTO ' +
              ' WHERE ID_NATUREZA_FK = 4 ' +
              '   AND FLG_CANCELADO <> ' + QuotedStr('S') +
              '   AND ID_ORIGEM IN (' + sRec + ')';
      Open;
    end;

    QryMon.First;

    while not QryMon.Eof do
    begin
      if Trim(Result) = '' then
        Result := QryMon.FieldByName('ID_ORIGEM').AsString
      else
        Result := (Result + ', ' + QryMon.FieldByName('ID_ORIGEM').AsString);

      QryMon.Next;
    end;

    FreeAndNil(QryMon);
  end;

  FreeAndNil(QryConf);
  FreeAndNil(QryVerif);
end;

function TdmPrincipal.ExistemFirmasPendentes: Integer;
var
  QryConf, QryVerif: TFDQuery;
begin
  Result := 0;

  QryConf := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConf, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ULTRECIBOATUALF, ID_USUATUALIZAFIR ' +
            '  FROM CONFIGURACAO';
    Open;
  end;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConFIRMAS);

  if (QryConf.FieldByName('ID_USUATUALIZAFIR').IsNull) or
    (QryConf.FieldByName('ID_USUATUALIZAFIR').AsInteger = vgUsu_Id) then
  begin
    with QryVerif, SQL do
    begin
      Close;
      Clear;

      Text := 'SELECT COUNT(S.ID_SERVICO) AS QTD ' +
              '  FROM SERVICOS S ' +
              ' WHERE S.ID_SERVICO > :ID_SERVICO ' +
              '   AND S.ANO >= :ANO ' +
              '   AND S.DATA >= :DATA ' +
              '   AND S.TOTAL IS NOT NULL';

      Params.ParamByName('DATA').Value := vgConf_DataIniImportacao;

      if (QryConf.FieldByName('ID_ULTRECIBOATUALF').IsNull) or
        (QryConf.FieldByName('ID_ULTRECIBOATUALF').AsInteger = 0) then
      begin
        Params.ParamByName('ID_SERVICO').AsInteger := 0;
        Params.ParamByName('ANO').AsInteger        := 0;
      end
      else
      begin
        Params.ParamByName('ID_SERVICO').AsInteger := StrToInt(Copy(QryConf.FieldByName('ID_ULTRECIBOATUALF').AsString,
                                                                    5,
                                                                    (Length(QryConf.FieldByName('ID_ULTRECIBOATUALF').AsString) - 4)));
        Params.ParamByName('ANO').AsInteger        := StrToInt(Copy(QryConf.FieldByName('ID_ULTRECIBOATUALF').AsString,
                                                                    1,
                                                                    4));
      end;

      Open;

      if QryVerif.RecordCount > 0 then
        Result := QryVerif.FieldByName('QTD').AsInteger;
    end;
  end;

  FreeAndNil(QryConf);
  FreeAndNil(QryVerif);
end;

function TdmPrincipal.ExistemRecibosCanceladosBalcao: String;
var
  QryConf, QryVerif, QryMon: TFDQuery;
  sBalcao: String;
begin
  Result  := '';
  sBalcao := '';

  QryConf := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConf, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ULTRECIBOATUAL_B, ID_USUATUALIZAREC ' +
            '  FROM CONFIGURACAO';
    Open;
  end;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConRECIBO);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT B.ID_BALCAO ' +
            '  FROM BALCAO B ' +
            ' WHERE B.ID_BALCAO <= :ID_BALCAO ' +
            '   AND B.VALOR_TOTAL IS NOT NULL ' +
            '   AND B.DATA >= :DATA ' +
            '   AND B.STATUS = ' + QuotedStr('CA');
    Params.ParamByName('DATA').Value      := vgConf_DataIniImportacao;
    Params.ParamByName('ID_BALCAO').Value := QryConf.FieldByName('ID_ULTRECIBOATUAL_B').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
  begin
    while not QryVerif.Eof do
    begin
      if Trim(sBalcao) = '' then
        sBalcao := QryVerif.FieldByName('ID_BALCAO').AsString
      else
        sBalcao := (sBalcao + ', ' + QryVerif.FieldByName('ID_BALCAO').AsString);

      QryVerif.Next;
    end;

    QryMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    with QryMon, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ORIGEM ' +
              '  FROM LANCAMENTO ' +
              ' WHERE ID_NATUREZA_FK = 6 ' +
              '   AND ID_SISTEMA_FK = 1 ' +
              '   AND FLG_CANCELADO <> ' + QuotedStr('S') +
              '   AND ID_ORIGEM IN (' + sBalcao + ')';
      Open;
    end;

    QryMon.First;

    while not QryMon.Eof do
    begin
      if Trim(Result) = '' then
        Result := QryMon.FieldByName('ID_ORIGEM').AsString
      else
        Result := (Result + ', ' + QryMon.FieldByName('ID_ORIGEM').AsString);

      QryMon.Next;
    end;

    FreeAndNil(QryMon);
  end;

  FreeAndNil(QryConf);
  FreeAndNil(QryVerif);
end;

function TdmPrincipal.ExistemRecibosCanceladosTalao: String;
var
  QryConf, QryVerif, QryMon: TFDQuery;
  sTalao: String;
begin
  Result := '';
  sTalao := '';

  QryConf := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConf, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ULTRECIBOATUAL_T, ID_USUATUALIZAREC ' +
            '  FROM CONFIGURACAO';
    Open;
  end;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConRECIBO);

  with QryVerif, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT T.ID_TALAO ' +
            '  FROM TALAO T ' +
            ' WHERE T.ID_TALAO <= :ID_TALAO ' +
            '   AND T.VALOR_TOTAL IS NOT NULL ' +
            '   AND T.DT_ENTRADA >= :DATA ' +
            '   AND T.STATUS = ' + QuotedStr('CA');
    Params.ParamByName('DATA').Value     := vgConf_DataIniImportacao;
    Params.ParamByName('ID_TALAO').Value := QryConf.FieldByName('ID_ULTRECIBOATUAL_T').AsInteger;
    Open;
  end;

  if QryVerif.RecordCount > 0 then
  begin
    while not QryVerif.Eof do
    begin
      if Trim(sTalao) = '' then
        sTalao := QryVerif.FieldByName('ID_TALAO').AsString
      else
        sTalao := (sTalao + ', ' + QryVerif.FieldByName('ID_TALAO').AsString);

      QryVerif.Next;
    end;

    QryMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

    with QryMon, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT ID_ORIGEM ' +
              '  FROM LANCAMENTO ' +
              ' WHERE ID_NATUREZA_FK = 6 ' +
              '   AND ID_SISTEMA_FK <> 1 ' +
              '   AND FLG_CANCELADO <> ' + QuotedStr('S') +
              '   AND ID_ORIGEM IN (' + sTalao + ')';
      Open;
    end;

    QryMon.First;

    while not QryMon.Eof do
    begin
      if Trim(Result) = '' then
        Result := QryMon.FieldByName('ID_ORIGEM').AsString
      else
        Result := (Result + ', ' + QryMon.FieldByName('ID_ORIGEM').AsString);

      QryMon.Next;
    end;

    FreeAndNil(QryMon);
  end;

  FreeAndNil(QryConf);
  FreeAndNil(QryVerif);
end;

function TdmPrincipal.ExistemRecibosPendentesBalcao: Integer;
var
  QryConf, QryVerif: TFDQuery;
begin
  Result := 0;

  QryConf := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConf, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ULTRECIBOATUAL_B, ID_USUATUALIZAREC ' +
            '  FROM CONFIGURACAO';
    Open;
  end;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConRECIBO);

  if (QryConf.FieldByName('ID_USUATUALIZAREC').IsNull) or
    (QryConf.FieldByName('ID_USUATUALIZAREC').AsInteger = vgUsu_Id) then
  begin
    with QryVerif, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT COUNT(ID_BALCAO) AS QTD ' +
              '  FROM BALCAO ' +
              ' WHERE ID_BALCAO > :ID_BALCAO ' +
              '   AND DATA >= :DATA';
      Params.ParamByName('DATA').Value      := vgConf_DataIniImportacao;
      Params.ParamByName('ID_BALCAO').Value := QryConf.FieldByName('ID_ULTRECIBOATUAL_B').AsInteger;
      Open;

      Result := QryVerif.FieldByName('QTD').AsInteger;
    end;
  end;

  FreeAndNil(QryConf);
  FreeAndNil(QryVerif);
end;

function TdmPrincipal.ExistemRecibosPendentesTalao: Integer;
var
  QryConf, QryVerif: TFDQuery;
begin
  Result := 0;

  QryConf := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryConf, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT ID_ULTRECIBOATUAL_T, ID_USUATUALIZAREC ' +
            '  FROM CONFIGURACAO';
    Open;
  end;

  QryVerif := dmGerencial.CriarFDQuery(nil, vgConRECIBO);

  if (QryConf.FieldByName('ID_USUATUALIZAREC').IsNull) or
    (QryConf.FieldByName('ID_USUATUALIZAREC').AsInteger = vgUsu_Id) then
  begin
    with QryVerif, SQL do
    begin
      Close;
      Clear;
      Text := 'SELECT COUNT(ID_TALAO) AS QTD ' +
              '  FROM TALAO ' +
              ' WHERE ID_TALAO > :ID_TALAO ' +
              '   AND DT_ENTRADA >= :DATA';
      Params.ParamByName('DATA').Value      := vgConf_DataIniImportacao;
      Params.ParamByName('ID_TALAO').Value  := QryConf.FieldByName('ID_ULTRECIBOATUAL_T').AsInteger;
      Open;

      Result := QryVerif.FieldByName('QTD').AsInteger;
    end;
  end;

  FreeAndNil(QryConf);
  FreeAndNil(QryVerif);
end;

procedure TdmPrincipal.FinalizarComponenteLancamento;
begin
  FreeAndNil(ListaLancamentos);
  FreeAndNil(DadosLancamento);

  FreeAndNil(ListaLancamentoDets);
  FreeAndNil(DadosLancamentoDet);

  FreeAndNil(ListaLancamentoParcs);
  FreeAndNil(DadosLancamentoParc);
end;

function TdmPrincipal.GerarDataDespesaRecorrente(Data: TDateTime): TDateTime;
var
  Dia, Mes, Ano: Word;
  iDia, iMes, iAno: Integer;
  sData: String;
  dData: TDatetime;
begin
  iDia := 0;
  iMes := 0;
  iAno := 0;

  DecodeDate(Data, Ano, Mes, Dia);

  case Mes of
    1:  //Janeiro para Fevereiro
    begin
      iMes := (Mes + 1);
      iAno := Ano;

      if Dia > 28 then
      begin
        if Dia = 29 then
        begin
          if IsLeapYear(Ano) then
            iDia := Dia
          else
            iDia := 28;
        end
        else
          iDia := 28;
      end
      else
        iDia := Dia;
    end;
    12:  //Dezembro para Janeiro
    begin
      iDia := Dia;
      iMes := 1;
      iAno := (Ano + 1);
    end;
    2, 4, 6,  //Fevereiro para Marco - Abril para Maio - Junho para Julho
    7, 9, 11:  //Julho para Agosto - Setembro para Outubro - Novembro para Dezembro
    begin
      iDia := Dia;
      iMes := (Mes + 1);
      iAno := Ano;
    end;
    3, 5, 8, 10:  //Marco para Abril - Maio para Junho - Agosto para Setembro - Outubro para Novembro
    begin
      if Dia = 31 then
        iDia := 30
      else
        iDia := Dia;

      iMes := (Mes + 1);
      iAno := Ano;
    end;
  end;

  sData := dmGerencial.CompletaString(IntToStr(iDia), '0', 'E', 2) +
           '/' +
           dmGerencial.CompletaString(IntToStr(iMes), '0', 'E', 2) +
           '/' +
           dmGerencial.CompletaString(IntToStr(iAno), '0', 'E', 4);

  dData := StrToDateTime(sData);

  if dData < vgDataLancVigente then
  begin
    if Trim(vgSituacaoCaixa) = 'N' then
      Result := dData
    else
      Result := vgDataLancVigente;
  end
  else
    Result := dData;
end;

function TdmPrincipal.ProcessoSistemaAtivo(NomeExe: String): Boolean;
var
  Name: String;
  Proc: PROCESSENTRY32;
  HSnap: HWND;
  Looper: BOOL;
begin
  Result := False;

  Proc.dwSize := SizeOf(Proc);
  HSnap := CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
  Looper := Process32First(HSnap, Proc);

  while Integer(Looper) <> 0 do
  begin
    Name := ExtractFileName(Proc.szExeFile);

    if NomeExe = Name then
      Result := True;

    Looper := Process32Next(HSnap, Proc);
  end;

  CloseHandle(HSnap);
end;

procedure TdmPrincipal.InicializarComponenteLancamento;
begin
  { LANCAMENTO }
  Lancamento       := TClasseLancamento;
  ListaLancamentos := TList<TDadosClasseLancamento>.Create;

  { LANCAMENTO - DETALHE }
  LancamentoDet       := TClasseLancamentoDet;
  ListaLancamentoDets := TList<TDadosClasseLancamentoDet>.Create;

  { LANCAMENTO - PARCELA }
  LancamentoParc       := TClasseLancamentoParc;
  ListaLancamentoParcs := TList<TDadosClasseLancamentoParc>.Create;
end;

procedure TdmPrincipal.InsertLancamento(Indice: Integer);
var
  QryLanc: TFDQuery;
begin
  QryLanc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLanc, SQL do
  begin
    { LANCAMENTO }
    Close;
    Clear;

    Text := 'INSERT INTO LANCAMENTO (COD_LANCAMENTO, ANO_LANCAMENTO, DATA_LANCAMENTO, TIPO_LANCAMENTO, ' +
            '                        TIPO_CADASTRO, ID_CATEGORIA_DESPREC_FK, ID_CLIENTE_FORNECEDOR_FK, ' +
            '                        ID_SUBCATEGORIA_DESPREC_FK, FLG_IMPOSTORENDA, FLG_FLUTUANTE, ' +
            '                        FLG_PESSOAL, FLG_AUXILIAR, FLG_FORAFECHCAIXA, FLG_REAL, ' +
            '                        ID_NATUREZA_FK, QTD_PARCELAS, ' +
            '                        VR_TOTAL_PREV, VR_TOTAL_JUROS, VR_TOTAL_DESCONTO, ' +
            '                        VR_TOTAL_PAGO, CAD_ID_USUARIO, DATA_CADASTRO, FLG_STATUS, ' +
            '                        FLG_CANCELADO, DATA_CANCELAMENTO, CANCEL_ID_USUARIO, ' +
            '                        FLG_RECORRENTE, FLG_REPLICADO, NUM_RECIBO, ID_ORIGEM, ' +
            '                        ID_SISTEMA_FK, OBSERVACAO, ID_CARNELEAO_EQUIVALENCIA_FK) ' +
            '                VALUES (:COD_LANCAMENTO, :ANO_LANCAMENTO, :DATA_LANCAMENTO, :TIPO_LANCAMENTO, ' +
            '                        :TIPO_CADASTRO, :ID_CATEGORIA_DESPREC, :ID_CLIENTE_FORNECEDOR, ' +
            '                        :ID_SUBCATEGORIA_DESPREC, :FLG_IMPOSTORENDA, :FLG_FLUTUANTE, ' +
            '                        :FLG_PESSOAL, :FLG_AUXILIAR, :FLG_FORAFECHCAIXA, :FLG_REAL, ' +
            '                        :ID_NATUREZA, :QTD_PARCELAS, ' +
            '                        :VR_TOTAL_PREV, :VR_TOTAL_JUROS, :VR_TOTAL_DESCONTO, ' +
            '                        :VR_TOTAL_PAGO, :CAD_ID_USUARIO, :DATA_CADASTRO, :FLG_STATUS, ' +
            '                        :FLG_CANCELADO, :DATA_CANCELAMENTO, :CANCEL_ID_USUARIO, ' +
            '                        :FLG_RECORRENTE, :FLG_REPLICADO, :NUM_RECIBO, :ID_ORIGEM, ' +
            '                        :ID_SISTEMA, :OBSERVACAO, :ID_CARNELEAO_EQUIVALENCIA)';

    if ListaLancamentos[Indice].CodLancamento = 0 then
    begin
      ListaLancamentos[Indice].CodLancamento := dmGerencial.ProximoCodigoLancamento;
      ListaLancamentos[Indice].AnoLancamento := YearOf(Date);
    end;

    Params.ParamByName('COD_LANCAMENTO').AsInteger            := ListaLancamentos[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').AsInteger            := ListaLancamentos[Indice].AnoLancamento;
    Params.ParamByName('DATA_LANCAMENTO').AsDateTime          := ListaLancamentos[Indice].DataLancamento;
    Params.ParamByName('TIPO_LANCAMENTO').AsString            := ListaLancamentos[Indice].TipoLancamento;
    Params.ParamByName('TIPO_CADASTRO').AsString              := ListaLancamentos[Indice].TipoCadastro;
    Params.ParamByName('ID_NATUREZA').AsInteger               := ListaLancamentos[Indice].IdNatureza;
    Params.ParamByName('QTD_PARCELAS').AsInteger              := ListaLancamentos[Indice].QtdParcelas;
    Params.ParamByName('CAD_ID_USUARIO').AsInteger            := ListaLancamentos[Indice].CadIdUsuario;
    Params.ParamByName('DATA_CADASTRO').AsDateTime            := ListaLancamentos[Indice].DataCadastro;
    Params.ParamByName('FLG_STATUS').AsString                 := ListaLancamentos[Indice].FlgStatus;
    Params.ParamByName('FLG_CANCELADO').AsString              := ListaLancamentos[Indice].FlgCancelado;
    Params.ParamByName('FLG_RECORRENTE').AsString             := ListaLancamentos[Indice].FlgRecorrente;
    Params.ParamByName('FLG_REPLICADO').AsString              := ListaLancamentos[Indice].FlgReplicado;
    Params.ParamByName('VR_TOTAL_PREV').AsCurrency            := ListaLancamentos[Indice].VlrTotalPrev;
    Params.ParamByName('VR_TOTAL_JUROS').AsCurrency           := ListaLancamentos[Indice].VlrTotalJuros;
    Params.ParamByName('VR_TOTAL_DESCONTO').AsCurrency        := ListaLancamentos[Indice].VlrTotalDesconto;
    Params.ParamByName('VR_TOTAL_PAGO').AsCurrency            := ListaLancamentos[Indice].VlrTotalPago;

    if ListaLancamentos[Indice].IdCliFor <> 0 then
      Params.ParamByName('ID_CLIENTE_FORNECEDOR').AsInteger   := ListaLancamentos[Indice].IdCliFor;

    if ListaLancamentos[Indice].IdCategoria <> 0 then
      Params.ParamByName('ID_CATEGORIA_DESPREC').AsInteger    := ListaLancamentos[Indice].IdCategoria;

    if ListaLancamentos[Indice].IdSubCategoria <> 0 then
      Params.ParamByName('ID_SUBCATEGORIA_DESPREC').AsInteger := ListaLancamentos[Indice].IdSubCategoria;

    if ListaLancamentos[Indice].FlgIR <> '' then
      Params.ParamByName('FLG_IMPOSTORENDA').AsString         := ListaLancamentos[Indice].FlgIR;

    if ListaLancamentos[Indice].FlgPessoal <> '' then
      Params.ParamByName('FLG_PESSOAL').AsString              := ListaLancamentos[Indice].FlgPessoal;

    if ListaLancamentos[Indice].FlgAuxiliar <> '' then
      Params.ParamByName('FLG_AUXILIAR').AsString             := ListaLancamentos[Indice].FlgAuxiliar;

    if ListaLancamentos[Indice].FlgForaFechCaixa <> '' then
      Params.ParamByName('FLG_FORAFECHCAIXA').AsString        := ListaLancamentos[Indice].FlgForaFechCaixa;

    if ListaLancamentos[Indice].FlgReal <> '' then
      Params.ParamByName('FLG_REAL').AsString                 := ListaLancamentos[Indice].FlgReal;

    if ListaLancamentos[Indice].FlgFlutuante <> '' then
      Params.ParamByName('FLG_FLUTUANTE').AsString            := ListaLancamentos[Indice].FlgFlutuante;

    if ListaLancamentos[Indice].DataCancelamento <> 0 then
      Params.ParamByName('DATA_CANCELAMENTO').AsDateTime      := ListaLancamentos[Indice].DataCancelamento;

    if ListaLancamentos[Indice].CancelIdUsuario <> 0 then
      Params.ParamByName('CANCEL_ID_USUARIO').AsInteger       := ListaLancamentos[Indice].CancelIdUsuario;

    if ListaLancamentos[Indice].NumRecibo > -1 then
      Params.ParamByName('NUM_RECIBO').AsInteger              := ListaLancamentos[Indice].NumRecibo;

    if ListaLancamentos[Indice].IdOrigem <> 0 then
      Params.ParamByName('ID_ORIGEM').AsInteger               := ListaLancamentos[Indice].IdOrigem;

    if ListaLancamentos[Indice].IdSistema <> 0 then
      Params.ParamByName('ID_SISTEMA').AsInteger              := ListaLancamentos[Indice].IdSistema;

    if ListaLancamentos[Indice].Observacao <> '' then
      Params.ParamByName('OBSERVACAO').AsString               := ListaLancamentos[Indice].Observacao;

    if ListaLancamentos[Indice].IdEquivalencia <> 0 then
      Params.ParamByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger := ListaLancamentos[Indice].IdEquivalencia;

    try
      ExecSQL;
    except
      ListaLancamentos[Indice].CodLancamento := dmGerencial.ProximoCodigoLancamento;
      InsertLancamento(Indice);
    end;
  end;

  FreeAndNil(QryLanc);
end;

procedure TdmPrincipal.InsertLancamentoDet(Indice, IndiceL: Integer);
var
  QryLancD: TFDQuery;
begin
  QryLancD := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLancD, SQL do
  begin
    { LANCAMENTO - DETALHE }
    Close;
    Clear;

    Text := 'INSERT INTO LANCAMENTO_DET (ID_LANCAMENTO_DET, COD_LANCAMENTO_FK, ANO_LANCAMENTO_FK, ' +
            '                            ID_ITEM_FK, VR_UNITARIO, QUANTIDADE, ' +
            '                            VR_TOTAL, SELO_ORIGEM, ALEATORIO_ORIGEM, ' +
            '                            TIPO_COBRANCA, COD_ADICIONAL, EMOLUMENTOS, ' +
            '                            FETJ, FUNDPERJ, FUNPERJ, FUNARPEN, PMCMV, ' +
            '                            ISS, MUTUA, ACOTERJ, ' +
            '                            NUM_PROTOCOLO, FLG_CANCELADO) ' +
            '                    VALUES (:ID_LANCAMENTO_DET, :COD_LANCAMENTO, :ANO_LANCAMENTO, ' +
            '                            :ID_ITEM, :VR_UNITARIO, :QUANTIDADE, ' +
            '                            :VR_TOTAL, :SELO_ORIGEM, :ALEATORIO_ORIGEM, ' +
            '                            :TIPO_COBRANCA, :COD_ADICIONAL, :EMOLUMENTOS, ' +
            '                            :FETJ, :FUNDPERJ, :FUNPERJ, :FUNARPEN, :PMCMV, ' +
            '                            :ISS, :MUTUA, :ACOTERJ, ' +
            '                            :NUM_PROTOCOLO, :FLG_CANCELADO)';

    if ListaLancamentoDets[Indice].IdLancamentoDet = 0 then
      ListaLancamentoDets[Indice].IdLancamentoDet  := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');

    ListaLancamentoDets[Indice].CodLancamento      := ListaLancamentos[IndiceL].CodLancamento;
    ListaLancamentoDets[Indice].AnoLancamento      := ListaLancamentos[IndiceL].AnoLancamento;

    Params.ParamByName('ID_LANCAMENTO_DET').Value  := ListaLancamentoDets[Indice].IdLancamentoDet;
    Params.ParamByName('COD_LANCAMENTO').Value     := ListaLancamentoDets[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value     := ListaLancamentoDets[Indice].AnoLancamento;
    Params.ParamByName('VR_UNITARIO').Value        := ListaLancamentoDets[Indice].VlrUnitario;
    Params.ParamByName('VR_TOTAL').Value           := ListaLancamentoDets[Indice].VlrTotal;
    Params.ParamByName('EMOLUMENTOS').Value        := ListaLancamentoDets[Indice].Emolumentos;
    Params.ParamByName('FETJ').Value               := ListaLancamentoDets[Indice].FETJ;
    Params.ParamByName('FUNDPERJ').Value           := ListaLancamentoDets[Indice].FUNDPERJ;
    Params.ParamByName('FUNPERJ').Value            := ListaLancamentoDets[Indice].FUNPERJ;
    Params.ParamByName('FUNARPEN').Value           := ListaLancamentoDets[Indice].FUNARPEN;
    Params.ParamByName('PMCMV').Value              := ListaLancamentoDets[Indice].PMCMV;
    Params.ParamByName('ISS').Value                := ListaLancamentoDets[Indice].ISS;
    Params.ParamByName('MUTUA').Value              := ListaLancamentoDets[Indice].Mutua;
    Params.ParamByName('ACOTERJ').Value            := ListaLancamentoDets[Indice].Acoterj;

    if ListaLancamentoDets[Indice].IdItem <> 0 then
      Params.ParamByName('ID_ITEM').Value          := ListaLancamentoDets[Indice].IdItem;

    if ListaLancamentoDets[Indice].Quantidade <> 0 then
      Params.ParamByName('QUANTIDADE').Value       := ListaLancamentoDets[Indice].Quantidade;

    if ListaLancamentoDets[Indice].SeloOrigem <> '' then
      Params.ParamByName('SELO_ORIGEM').Value      := ListaLancamentoDets[Indice].SeloOrigem;

    if ListaLancamentoDets[Indice].AleatorioOrigem <> '' then
      Params.ParamByName('ALEATORIO_ORIGEM').Value := ListaLancamentoDets[Indice].AleatorioOrigem;

    if ListaLancamentoDets[Indice].TipoCobranca <> '' then
      Params.ParamByName('TIPO_COBRANCA').Value    := ListaLancamentoDets[Indice].TipoCobranca;

    if ListaLancamentoDets[Indice].CodAdicional <> 0 then
      Params.ParamByName('COD_ADICIONAL').Value    := ListaLancamentoDets[Indice].CodAdicional;

    if ListaLancamentoDets[Indice].NumProtocolo <> 0 then
      Params.ParamByName('NUM_PROTOCOLO').Value    := ListaLancamentoDets[Indice].NumProtocolo;

    if ListaLancamentoDets[Indice].FlgCancelado <> '' then
      Params.ParamByName('FLG_CANCELADO').Value    := ListaLancamentoDets[Indice].FlgCancelado;

    try
      ExecSQL;
    except
      ListaLancamentoDets[Indice].IdLancamentoDet := BS.ProximoId('ID_LANCAMENTO_DET', 'LANCAMENTO_DET');
      InsertLancamentoDet(Indice, IndiceL);
    end;
  end;

  FreeAndNil(QryLancD);
end;

procedure TdmPrincipal.InsertLancamentoParc(Indice, IndiceL: Integer);
var
  QryLancP: TFDQuery;
begin
  QryLancP := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLancP, SQL do
  begin
    { LANCAMENTO - PARCELA }
    Close;
    Clear;

    Text := 'INSERT INTO LANCAMENTO_PARC (ID_LANCAMENTO_PARC, COD_LANCAMENTO_FK, ANO_LANCAMENTO_FK, ' +
            '                             NUM_PARCELA, DATA_VENCIMENTO, VR_PARCELA_PREV, ID_FORMAPAGAMENTO_ORIG_FK, ' +
            '                             VR_PARCELA_JUROS, VR_PARCELA_DESCONTO, VR_PARCELA_PAGO, ID_FORMAPAGAMENTO_FK, ' +
            '                             AGENCIA, CONTA, ID_BANCO_FK, NUM_CHEQUE, FLG_STATUS, DATA_CANCELAMENTO, ' +
            '                             CANCEL_ID_USUARIO, DATA_PAGAMENTO, NUM_COD_DEPOSITO, ' +
            '                             PAG_ID_USUARIO, OBS_LANCAMENTO_PARC, CAD_ID_USUARIO, ' +
            '                             DATA_CADASTRO, DATA_LANCAMENTO_PARC) ' +
            '                     VALUES (:ID_LANCAMENTO_PARC, :COD_LANCAMENTO, :ANO_LANCAMENTO, ' +
            '                             :NUM_PARCELA, :DATA_VENCIMENTO, :VR_PARCELA_PREV, :ID_FORMAPAGAMENTO_ORIG, ' +
            '                             :VR_PARCELA_JUROS, :VR_PARCELA_DESCONTO, :VR_PARCELA_PAGO, :ID_FORMAPAGAMENTO, ' +
            '                             :AGENCIA, :CONTA, :ID_BANCO, :NUM_CHEQUE, :FLG_STATUS, :DATA_CANCELAMENTO, ' +
            '                             :CANCEL_ID_USUARIO, :DATA_PAGAMENTO, :NUM_COD_DEPOSITO, ' +
            '                             :PAG_ID_USUARIO, :OBS_LANCAMENTO_PARC, :CAD_ID_USUARIO, ' +
            '                             :DATA_CADASTRO, :DATA_LANCAMENTO_PARC)';

    if ListaLancamentoParcs[Indice].IdLancamentoParc = 0 then
      ListaLancamentoParcs[Indice].IdLancamentoParc := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');

    ListaLancamentoParcs[Indice].CodLancamento    := ListaLancamentos[IndiceL].CodLancamento;
    ListaLancamentoParcs[Indice].AnoLancamento    := ListaLancamentos[IndiceL].AnoLancamento;

    Params.ParamByName('ID_LANCAMENTO_PARC').AsInteger     := ListaLancamentoParcs[Indice].IdLancamentoParc;
    Params.ParamByName('COD_LANCAMENTO').AsInteger         := ListaLancamentoParcs[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').AsInteger         := ListaLancamentoParcs[Indice].AnoLancamento;
    Params.ParamByName('VR_PARCELA_PREV').AsCurrency       := ListaLancamentoParcs[Indice].VlrPrevisto;
    Params.ParamByName('VR_PARCELA_JUROS').AsCurrency      := ListaLancamentoParcs[Indice].VlrJuros;
    Params.ParamByName('VR_PARCELA_DESCONTO').AsCurrency   := ListaLancamentoParcs[Indice].VlrDesconto;
    Params.ParamByName('VR_PARCELA_PAGO').AsCurrency       := ListaLancamentoParcs[Indice].VlrPago;

    if ListaLancamentoParcs[Indice].DataLancParc <> 0 then
      Params.ParamByName('DATA_LANCAMENTO_PARC').AsDate    := ListaLancamentoParcs[Indice].DataLancParc;

    if ListaLancamentoParcs[Indice].NumParcela <> 0 then
      Params.ParamByName('NUM_PARCELA').AsInteger          := ListaLancamentoParcs[Indice].NumParcela;

    if ListaLancamentoParcs[Indice].DataVencimento <> 0 then
      Params.ParamByName('DATA_VENCIMENTO').AsDate         := ListaLancamentoParcs[Indice].DataVencimento;

    if ListaLancamentoParcs[Indice].IdFormaPagto <> 0 then
      Params.ParamByName('ID_FORMAPAGAMENTO_ORIG').AsInteger := ListaLancamentoParcs[Indice].IdFormaPagto;

    if ListaLancamentoParcs[Indice].IdFormaPagto <> 0 then
      Params.ParamByName('ID_FORMAPAGAMENTO').AsInteger    := ListaLancamentoParcs[Indice].IdFormaPagto;

    if ListaLancamentoParcs[Indice].Agencia <> '' then
      Params.ParamByName('AGENCIA').AsString               := ListaLancamentoParcs[Indice].Agencia;

    if ListaLancamentoParcs[Indice].Conta <> '' then
      Params.ParamByName('CONTA').AsString                 := ListaLancamentoParcs[Indice].Conta;

    if ListaLancamentoParcs[Indice].IdBanco <> 0 then
      Params.ParamByName('ID_BANCO').AsInteger             := ListaLancamentoParcs[Indice].IdBanco;

    if ListaLancamentoParcs[Indice].NumCheque <> '' then
      Params.ParamByName('NUM_CHEQUE').AsString            := ListaLancamentoParcs[Indice].NumCheque;

    if ListaLancamentoParcs[Indice].NumCodDeposito <> '' then
      Params.ParamByName('NUM_COD_DEPOSITO').AsString      := ListaLancamentoParcs[Indice].NumCodDeposito;

    if ListaLancamentoParcs[Indice].FlgStatus <> '' then
      Params.ParamByName('FLG_STATUS').AsString            := ListaLancamentoParcs[Indice].FlgStatus;

    if ListaLancamentoParcs[Indice].DataCancelamento <> 0 then
      Params.ParamByName('DATA_CANCELAMENTO').AsDateTime   := ListaLancamentoParcs[Indice].DataCancelamento;

    if ListaLancamentoParcs[Indice].CancelIdUsuario <> 0 then
      Params.ParamByName('CANCEL_ID_USUARIO').AsInteger    := ListaLancamentoParcs[Indice].CancelIdUsuario;

    if ListaLancamentoParcs[Indice].DataPagamento <> 0 then
      Params.ParamByName('DATA_PAGAMENTO').AsDateTime      := ListaLancamentoParcs[Indice].DataPagamento;

    if ListaLancamentoParcs[Indice].PagIdUsuario <> 0 then
      Params.ParamByName('PAG_ID_USUARIO').AsInteger       := ListaLancamentoParcs[Indice].PagIdUsuario;

    if ListaLancamentoParcs[Indice].CadIdUsuario <> 0 then
      Params.ParamByName('CAD_ID_USUARIO').AsInteger       := ListaLancamentoParcs[Indice].CadIdUsuario;

    if ListaLancamentoParcs[Indice].DataCadastro <> 0 then
      Params.ParamByName('DATA_CADASTRO').AsDate           := ListaLancamentoParcs[Indice].DataCadastro;

    if ListaLancamentoParcs[Indice].Observacao <> '' then
      Params.ParamByName('OBS_LANCAMENTO_PARC').AsString   := ListaLancamentoParcs[Indice].Observacao;

    try
      ExecSQL;
    except
      ListaLancamentoParcs[Indice].IdLancamentoParc := BS.ProximoId('ID_LANCAMENTO_PARC', 'LANCAMENTO_PARC');
      InsertLancamentoParc(Indice, IndiceL);
    end;
  end;

  FreeAndNil(QryLancP);
end;

procedure TdmPrincipal.LoginSistema;
var
  lAcaoOk, lMonitoramentoFechado: Boolean;
  Qry: TFDQuery;
  ComputerName: Array [0 .. 256] of Char;
  Size: DWORD;
  sNomeMaquina: String;
begin
  Size := 256;
  GetComputerName(ComputerName, Size);
  sNomeMaquina := ComputerName;

  Qry := dmGerencial.CriarFDQuery(nil, vgConGER);

  with Qry, SQL do
  begin
    Close;
    Clear;
    Text := 'SELECT * ' +
            '  FROM USUARIO ' +
            ' WHERE NOME_MAQUINA = :NOME_MAQUINA';
    Params.ParamByName('NOME_MAQUINA').Value := Trim(sNomeMaquina);
    Open;
  end;

  if Qry.RecordCount = 0 then
    vgLoginOk := False
  else
  begin
    if Qry.FieldByName('FLG_ATIVO').AsString = 'N' then
      vgLoginOk := False
    else
    begin
      { Preencher as variaveis do Sistema }
      //USUARIO
      vgUsu_Id               := Qry.FieldByName('ID_USUARIO').AsInteger;
      vgUsu_Nome             := Qry.FieldByName('NOME').AsString;
      vgUsu_CPF              := Qry.FieldByName('CPF').AsString;
      vgUsu_Qualificacao     := Qry.FieldByName('QUALIFICACAO').AsString;
      vgUsu_FlgMaster        := Qry.FieldByName('FLG_MASTER').AsString;
      vgUsu_FlgOficial       := 'N';
      vgUsu_FlgJustifEdicao  := Qry.FieldByName('FLG_JUSTIF_EDICAO').AsString;
      vgUsu_Matricula        := Qry.FieldByName('MATRICULA').AsString;
      vgUsu_FlgSuporte       := Qry.FieldByName('FLG_SUPORTE').AsString;
      vgUsu_FlgVisualizouAtu := Qry.FieldByName('FLG_VISUALIZOU_ATU').AsString;
      vgUsu_FlgSexo          := Qry.FieldByName('FLG_SEXO').AsString;
      vgUsu_IdFuncionario    := Qry.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
      vgUsu_FlgAtivo         := Qry.FieldByName('FLG_ATIVO').AsString;
      vgUsu_DataCadastro     := Qry.FieldByName('DATA_CADASTRO').AsDateTime;
      vgUsu_DataInativacao   := Qry.FieldByName('DATA_INATIVACAO').AsDateTime;

      vgLoginOk := True;

      BS.GravarUsuarioLog(100, '', 'Usu�rio logou no Sistema Sync');
    end;
  end;
end;

procedure TdmPrincipal.RetornaCargo(var Id, Codigo: Integer; var Descricao,
  Observacao: String);
var
  Zero: Integer;
begin
  Zero := 0;
  RetornarGenerico('CARGO', Id, Zero, Codigo, Descricao, Observacao);
end;

procedure TdmPrincipal.RetornaCategoria(var Id, IdCategoria, Codigo: Integer; var Descricao,
  Observacao: String);
begin
  RetornarGenerico('CATEGORIA_DESPREC', Id, IdCategoria, Codigo, Descricao, Observacao);
end;

function TdmPrincipal.RetornaEquivalenciaCarneLeao(IdCat, IdSubCat,
  IdNat: Integer): Integer;
var
  QryEquiv, QryAux: TFDQuery;
  IdEquiv: Integer;
begin
  Result  := 0;
  IdEquiv := 0;

  QryEquiv := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  QryEquiv.Close;
  QryEquiv.SQL.Clear;

  QryEquiv.SQL.Text := 'SELECT ID_CARNELEAO_EQUIVALENCIA ' +
                       '  FROM CARNELEAO_EQUIVALENCIA ' +
                       ' WHERE M_TIPO_LANCAMENTO = ' + QuotedStr('R') +
                       '   AND FLG_CANCELADO = ' + QuotedStr('N') +
                       '   AND ID_CARNELEAO_CLASSIFICACAO_FK = 1 ' +
                       '   AND M_ID_NATUREZA_FK = ' + IntToStr(IdNat);

  if IdCat > 0 then
    QryEquiv.SQL.Add(' AND M_ID_CATEGORIA_DESPREC_FK = ' + IntToStr(IdCat));

  if IdSubCat > 0 then
    QryEquiv.SQL.Add(' AND M_ID_SUBCATEGORIA_DESPREC_FK = ' + IntToStr(IdSubCat));

  QryEquiv.Open;

  if QryEquiv.RecordCount = 0 then
  begin
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      QryAux := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryAux.Close;
      QryAux.SQL.Clear;

      QryAux.SQL.Text := 'INSERT INTO CARNELEAO_EQUIVALENCIA (ID_CARNELEAO_EQUIVALENCIA, ' +
                         '                                    M_TIPO_LANCAMENTO, ' +
                         '                                    M_ID_CATEGORIA_DESPREC_FK, ' +
                         '                                    M_ID_SUBCATEGORIA_DESPREC_FK, ' +
                         '                                    M_ID_NATUREZA_FK, ' +
                         '                                    ID_CARNELEAO_CLASSIFICACAO_FK, ' +
                         '                                    OBS_CARNELEAO_EQUIVALENCIA, ' +
                         '                                    DATA_CADASTRO, ' +
                         '                                    CAD_ID_USUARIO, ' +
                         '                                    FLG_CANCELADO) ' +
                         '                            VALUES (:ID_CARNELEAO_EQUIVALENCIA, ' +
                         '                                    :M_TIPO_LANCAMENTO, ' +
                         '                                    :M_ID_CATEGORIA_DESPREC, ' +
                         '                                    :M_ID_SUBCATEGORIA_DESPREC, ' +
                         '                                    :M_ID_NATUREZA, ' +
                         '                                    :ID_CARNELEAO_CLASSIFICACAO, ' +
                         '                                    :OBS_CARNELEAO_EQUIVALENCIA, ' +
                         '                                    :DATA_CADASTRO, ' +
                         '                                    :CAD_ID_USUARIO, ' +
                         '                                    :FLG_CANCELADO)';

      IdEquiv := BS.ProximoId('ID_CARNELEAO_EQUIVALENCIA', 'CARNELEAO_EQUIVALENCIA');

      QryAux.Params.ParamByName('ID_CARNELEAO_EQUIVALENCIA').Value  := IdEquiv;
      QryAux.Params.ParamByName('M_TIPO_LANCAMENTO').Value          := 'R';
      QryAux.Params.ParamByName('M_ID_CATEGORIA_DESPREC').Value     := IdCat;
      QryAux.Params.ParamByName('M_ID_SUBCATEGORIA_DESPREC').Value  := IdSubCat;
      QryAux.Params.ParamByName('M_ID_NATUREZA').Value              := IdNat;
      QryAux.Params.ParamByName('ID_CARNELEAO_CLASSIFICACAO').Value := 1;  //Trabalho Nao Assalariado
      QryAux.Params.ParamByName('OBS_CARNELEAO_EQUIVALENCIA').Value := 'Recibos';
      QryAux.Params.ParamByName('DATA_CADASTRO').Value              := Date;
      QryAux.Params.ParamByName('CAD_ID_USUARIO').Value             := vgUsu_Id;
      QryAux.Params.ParamByName('FLG_CANCELADO').Value              := 'N';

      QryAux.ExecSQL;

      FreeAndNil(QryAux);

      if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
    IdEquiv := QryEquiv.FieldByName('ID_CARNELEAO_EQUIVALENCIA').AsInteger;

  Result := IdEquiv;

  FreeAndNil(QryEquiv);
end;

procedure TdmPrincipal.RetornaNatureza(var Id, Codigo: Integer; var Descricao,
  Observacao: String);
var
  Zero: Integer;
begin
  Zero := 0;
  RetornarGenerico('NATUREZA', Id, Zero, Codigo, Descricao, Observacao);
end;

procedure TdmPrincipal.RetornarClassificacaoNatureza(IdNat: Integer; var IR, CP,
  LA: String);
var
  QryNat: TFDQuery;
begin
  QryNat := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryNat.Close;
  QryNat.SQL.Clear;
  QryNat.SQL.Text := 'SELECT FLG_IMPOSTORENDA, FLG_PESSOAL, FLG_AUXILIAR ' +
                     '  FROM NATUREZA ' +
                     ' WHERE ID_NATUREZA = :ID_NATUREZA';
  QryNat.Params.ParamByName('ID_NATUREZA').Value := IdNat;
  QryNat.Open;

  IR := QryNat.FieldByName('FLG_IMPOSTORENDA').AsString;
  CP := QryNat.FieldByName('FLG_PESSOAL').AsString;
  LA := QryNat.FieldByName('FLG_AUXILIAR').AsString;

  FreeAndNil(QryNat);
end;

procedure TdmPrincipal.RetornarClienteFornecedor(Tipo: String; var Id: Integer;
  var Documento, Observacao, NomeFantasia, RazaoSocial: String);
var
  QryCliForn, QryI, QryApres: TFDQuery;
  Op: TOperacao;
  sCPF_CNPJ: String;
  lRecibo: Boolean;
begin
  { ATENCAO: esta diferente da funcao equivalente na UDM do sistema Monitoramento }

  lRecibo := False;

  QryCliForn := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  //Consulta
  QryCliForn.Close;
  QryCliForn.SQL.Clear;
  QryCliForn.SQL.Text := 'SELECT ID_CLIENTE_FORNECEDOR, ' +
                         '       CPF_CNPJ, ' +
                         '       NOME_FANTASIA, ' +
                         '       RAZAO_SOCIAL ' +
                         '  FROM CLIENTE_FORNECEDOR ';

  if Trim(Documento) <> '' then
  begin
    if Length(dmGerencial.PegarNumeroTexto(Documento)) = 11 then
    begin
      QryCliForn.SQL.Add(' WHERE DOCUMENTO = :DOCUMENTO ' +
                         '   AND REPLACE(REPLACE(CPF_CNPJ, ' +
                                                 QuotedStr('-') + ', ' +
                                                 QuotedStr('') + '), ' +
                                         QuotedStr('.') + ', ' +
                                         QuotedStr('') + ') = :CPF_CNPJ ');

      QryCliForn.Params.ParamByName('CPF_CNPJ').Value  := dmGerencial.PegarNumeroTexto(Documento);
    end
    else if Length(dmGerencial.PegarNumeroTexto(Documento)) = 14 then
    begin
      QryCliForn.SQL.Add(' WHERE DOCUMENTO = :DOCUMENTO ' +
                         '   AND REPLACE(REPLACE(REPLACE(CPF_CNPJ, ' +
                                                         QuotedStr('-') + ', ' +
                                                         QuotedStr('') + '), ' +
                                                 QuotedStr('.') + ', ' +
                                                 QuotedStr('') + '), ' +
                                         QuotedStr('/') + ', ' +
                                         QuotedStr('') + ') = :CPF_CNPJ ');

      QryCliForn.Params.ParamByName('CPF_CNPJ').Value  := dmGerencial.PegarNumeroTexto(Documento);
    end
    else
      QryCliForn.SQL.Add(' WHERE DOCUMENTO = :DOCUMENTO ');

    QryCliForn.Params.ParamByName('DOCUMENTO').Value := Documento;
  end
  else
  begin
    QryCliForn.SQL.Add(' WHERE RAZAO_SOCIAL  = :RAZAO_SOCIAL ' +
                    '   AND NOME_FANTASIA = :NOME_FANTASIA');
    QryCliForn.Params.ParamByName('RAZAO_SOCIAL').Value  := RazaoSocial;
    QryCliForn.Params.ParamByName('NOME_FANTASIA').Value := NomeFantasia;
  end;

  QryCliForn.SQL.Add(' AND TIPO_CLIENTE_FORNECEDOR = :TIPO_CLIENTE_FORNECEDOR');
  QryCliForn.Params.ParamByName('TIPO_CLIENTE_FORNECEDOR').Value := Tipo;

  QryCliForn.Open;

  if QryCliForn.RecordCount = 0 then
  begin
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      if Trim(Observacao) = 'Cadastro realizado automaticamente em inclus�o de RECIBO.' then
      begin
        lRecibo := True;

        QryApres := dmGerencial.CriarFDQuery(nil, vgConRECIBOAux);

        QryApres.Close;
        QryApres.SQL.Clear;
        QryApres.SQL.Text := 'SELECT TELEFONE, CELULAR, EMAIL, ENDERECO ' +
                             '  FROM APRESENTANTES ';

        if Trim(Documento) <> '' then
        begin
          QryApres.SQL.Add(' WHERE DOCUMENTO = :DOCUMENTO');
          QryApres.Params.ParamByName('DOCUMENTO').Value := Documento;
        end
        else
        begin
          QryApres.SQL.Add(' WHERE NOME = :NOME');
          QryApres.Params.ParamByName('NOME').Value := RazaoSocial;
        end;

        QryApres.Open;
      end;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryI.Close;
      QryI.SQL.Clear;

      if lRecibo then
      begin
        QryI.SQL.Text := 'INSERT INTO CLIENTE_FORNECEDOR (ID_CLIENTE_FORNECEDOR, NOME_FANTASIA, RAZAO_SOCIAL, ' +
                         '                                CPF_CNPJ, DOCUMENTO, DATA_CADASTRO, CAD_ID_USUARIO, ' +
                         '                                OBSERVACAO, FLG_ATIVO, FLG_BLOQUEADO, ' +
                         '                                TIPO_CLIENTE_FORNECEDOR, TIPO_PESSOA, TELEFONE_1, CELULAR_1, ' +
                         '                                EMAIL_1, LOGRADOURO) ' +
                         '                        VALUES (:ID_CLIENTE_FORNECEDOR, :NOME_FANTASIA, :RAZAO_SOCIAL, ' +
                         '                                :CPF_CNPJ, :DOCUMENTO, :DATA_CADASTRO, :CAD_ID_USUARIO, ' +
                         '                                :OBSERVACAO, :FLG_ATIVO, :FLG_BLOQUEADO, ' +
                         '                                :TIPO_CLIENTE_FORNECEDOR, :TIPO_PESSOA, :TELEFONE_1, :CELULAR_1, ' +
                         '                                :EMAIL_1, :LOGRADOURO)' ;
      end
      else
      begin
        QryI.SQL.Text := 'INSERT INTO CLIENTE_FORNECEDOR (ID_CLIENTE_FORNECEDOR, NOME_FANTASIA, RAZAO_SOCIAL, ' +
                         '                                CPF_CNPJ, DOCUMENTO, DATA_CADASTRO, CAD_ID_USUARIO, ' +
                         '                                OBSERVACAO, FLG_ATIVO, FLG_BLOQUEADO, ' +
                         '                                TIPO_CLIENTE_FORNECEDOR, TIPO_PESSOA) ' +
                         '                        VALUES (:ID_CLIENTE_FORNECEDOR, :NOME_FANTASIA, :RAZAO_SOCIAL, ' +
                         '                                :CPF_CNPJ, :DOCUMENTO, :DATA_CADASTRO, :CAD_ID_USUARIO, ' +
                         '                                :OBSERVACAO, :FLG_ATIVO, :FLG_BLOQUEADO, ' +
                         '                                :TIPO_CLIENTE_FORNECEDOR, :TIPO_PESSOA)' ;
      end;

      if Id = 0 then
        Id := BS.ProximoId('ID_CLIENTE_FORNECEDOR', 'CLIENTE_FORNECEDOR');

      QryI.Params.ParamByName('ID_CLIENTE_FORNECEDOR').Value   := Id;
      QryI.Params.ParamByName('NOME_FANTASIA').Value           := NomeFantasia;
      QryI.Params.ParamByName('RAZAO_SOCIAL').Value            := RazaoSocial;
      QryI.Params.ParamByName('DOCUMENTO').Value               := Documento;
      QryI.Params.ParamByName('DATA_CADASTRO').Value           := Date;
      QryI.Params.ParamByName('CAD_ID_USUARIO').Value          := vgUsu_Id;
      QryI.Params.ParamByName('OBSERVACAO').Value              := Observacao;
      QryI.Params.ParamByName('FLG_ATIVO').Value               := 'S';
      QryI.Params.ParamByName('FLG_BLOQUEADO').Value           := 'N';
      QryI.Params.ParamByName('TIPO_CLIENTE_FORNECEDOR').Value := Tipo;

      if Length(dmGerencial.PegarNumeroTexto(Documento)) = 11 then
      begin
        sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(Documento), 1, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 4, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 7, 3) + '-' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 10, 2);

        QryI.Params.ParamByName('TIPO_PESSOA').Value            := 'F';
      end
      else if Length(dmGerencial.PegarNumeroTexto(Documento)) = 14 then
      begin
        sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(Documento), 1, 2) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 3, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 6, 3) + '/' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 9, 4) + '-' +
                     Copy(dmGerencial.PegarNumeroTexto(Documento), 13, 2);

        QryI.Params.ParamByName('TIPO_PESSOA').Value            := 'J';
      end
      else
        sCPF_CNPJ := '';

      QryI.Params.ParamByName('CPF_CNPJ').Value                 := sCPF_CNPJ;

      if lRecibo then
      begin
        if QryApres.RecordCount > 0 then
        begin
          if Trim(QryApres.FieldByName('TELEFONE').AsString) <> '' then
          begin
            if Length(QryApres.FieldByName('TELEFONE').AsString) = 10 then
              QryI.Params.ParamByName('TELEFONE_1').Value  := '(' + Copy(QryApres.FieldByName('TELEFONE').AsString, 1, 2) +
                                                              ')' + Copy(QryApres.FieldByName('TELEFONE').AsString, 3, 4) +
                                                              '-' + Copy(QryApres.FieldByName('TELEFONE').AsString, 7, 4)
            else
              QryI.Params.ParamByName('TELEFONE_1').Value  := Null;
          end
          else
            QryI.Params.ParamByName('TELEFONE_1').Value  := Null;

          if Trim(QryApres.FieldByName('CELULAR').AsString) <> '' then
          begin
            if Length(QryApres.FieldByName('CELULAR').AsString) = 11 then
              QryI.Params.ParamByName('CELULAR_1').Value  := '(' + Copy(QryApres.FieldByName('CELULAR').AsString, 1, 2) +
                                                              ')' + Copy(QryApres.FieldByName('CELULAR').AsString, 3, 5) +
                                                              '-' + Copy(QryApres.FieldByName('CELULAR').AsString, 8, 4)
            else
              QryI.Params.ParamByName('CELULAR_1').Value  := Null;
          end
          else
            QryI.Params.ParamByName('CELULAR_1').Value  := Null;

          if Trim(QryApres.FieldByName('EMAIL').AsString) <> '' then
            QryI.Params.ParamByName('EMAIL_1').Value    := QryApres.FieldByName('EMAIL').AsString
          else
            QryI.Params.ParamByName('EMAIL_1').Value    := Null;

          if Trim(QryApres.FieldByName('ENDERECO').AsString) <> '' then
            QryI.Params.ParamByName('LOGRADOURO').Value := QryApres.FieldByName('ENDERECO').AsString
          else
            QryI.Params.ParamByName('LOGRADOURO').Value := Null;
        end;
      end;

      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      if Trim(Tipo) = 'C' then
        BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Cliente',
                            Id, 'CLIENTE_FORNECEDOR')
      else if Trim(Tipo) = 'F' then
        BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Fornecedor',
                            Id, 'CLIENTE_FORNECEDOR');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if lRecibo then
        FreeAndNil(QryApres);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id           := QryCliForn.FieldByName('ID_CLIENTE_FORNECEDOR').AsInteger;
    Documento    := QryCliForn.FieldByName('CPF_CNPJ').AsString;
    NomeFantasia := QryCliForn.FieldByName('NOME_FANTASIA').AsString;
    RazaoSocial  := QryCliForn.FieldByName('RAZAO_SOCIAL').AsString;
  end;

  FreeAndNil(QryCliForn);
end;

procedure TdmPrincipal.RetornarFuncionario(var Id, IdCargo: Integer; var Nome,
  CPF, Observacao: String);
var
  QryFun, QryI, QryIMon: TFDQuery;
  Op: TOperacao;
  sCPF: String;
begin
  QryFun := dmGerencial.CriarFDQuery(nil, vgConGERAux);

  //Consulta
  QryFun.Close;
  QryFun.SQL.Clear;
  QryFun.SQL.Text := 'SELECT ID_FUNCIONARIO, CPF, NOME_FUNCIONARIO ' +
                     '  FROM FUNCIONARIO ';

  if Trim(CPF) <> '' then
  begin
    QryFun.SQL.Add(' WHERE REPLACE(REPLACE(CPF, ' +
                                           QuotedStr('-') + ', ' +
                                           QuotedStr('') + '), ' +
                                   QuotedStr('.') + ', ' +
                                   QuotedStr('') + ') = :CPF ');
    QryFun.Params.ParamByName('CPF').Value  := dmGerencial.PegarNumeroTexto(CPF);
  end
  else if Trim(Nome) <> '' then
  begin
    QryFun.SQL.Add(' WHERE NOME_FUNCIONARIO = :NOME_FUNCIONARIO');
    QryFun.Params.ParamByName('NOME_FUNCIONARIO').Value := Nome;
  end;

  QryFun.Open;

  if QryFun.RecordCount = 0 then
  begin
    //Gerencial
    try
      if vgConGERAux.Connected then
        vgConGERAux.StartTransaction;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConGERAux);

      QryI.Close;
      QryI.SQL.Clear;
      QryI.SQL.Text := 'INSERT INTO FUNCIONARIO (ID_FUNCIONARIO, NOME_FUNCIONARIO, ' +
                       '                         CPF, ID_CARGO_FK, DATA_ADMISSAO, ' +
                       '                         DATA_CADASTRO, FLG_ATIVO) ' +
                       '                 VALUES (:ID_FUNCIONARIO, :NOME_FUNCIONARIO, ' +
                       '                         :CPF, :ID_CARGO, :DATA_ADMISSAO, ' +
                       '                         :DATA_CADASTRO, :FLG_ATIVO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      if Id = 0 then
        Id := BS.ProximoId('ID_FUNCIONARIO', 'FUNCIONARIO');

      QryI.Params.ParamByName('ID_FUNCIONARIO').Value   := Id;
      QryI.Params.ParamByName('NOME_FUNCIONARIO').Value := Nome;
      QryI.Params.ParamByName('CPF').Value              := sCPF;
      QryI.Params.ParamByName('ID_CARGO').Value         := IdCargo;
      QryI.Params.ParamByName('DATA_ADMISSAO').Value    := Null;
      QryI.Params.ParamByName('DATA_CADASTRO').Value    := Date;

      if Trim(Nome) = 'FUNCION�RIO' then
        QryI.Params.ParamByName('FLG_ATIVO').Value      := 'N'
      else
        QryI.Params.ParamByName('FLG_ATIVO').Value      := 'S';

      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'FUNCIONARIO');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if vgConGERAux.InTransaction then
        vgConGERAux.Commit;
    except
      if vgConGERAux.InTransaction then
        vgConGERAux.Rollback;
    end;

    //Monitoramento
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      //Inclusao
      QryIMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryIMon.Close;
      QryIMon.SQL.Clear;
      QryIMon.SQL.Text := 'INSERT INTO FUNCIONARIO (ID_FUNCIONARIO, NOME_FUNCIONARIO, ' +
                       '                         CPF, ID_CARGO_FK, DATA_ADMISSAO, ' +
                       '                         DATA_CADASTRO, FLG_ATIVO) ' +
                       '                 VALUES (:ID_FUNCIONARIO, :NOME_FUNCIONARIO, ' +
                       '                         :CPF, :ID_CARGO, :DATA_ADMISSAO, ' +
                       '                         :DATA_CADASTRO, :FLG_ATIVO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      QryIMon.Params.ParamByName('ID_FUNCIONARIO').Value   := Id;
      QryIMon.Params.ParamByName('NOME_FUNCIONARIO').Value := Nome;
      QryIMon.Params.ParamByName('CPF').Value              := sCPF;
      QryIMon.Params.ParamByName('ID_CARGO').Value         := IdCargo;
      QryIMon.Params.ParamByName('DATA_ADMISSAO').Value    := Null;
      QryIMon.Params.ParamByName('DATA_CADASTRO').Value    := Date;

      if Trim(Nome) = 'FUNCION�RIO' then
        QryIMon.Params.ParamByName('FLG_ATIVO').Value      := 'N'
      else
        QryIMon.Params.ParamByName('FLG_ATIVO').Value      := 'S';

      QryIMon.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'FUNCIONARIO');
      vgOperacao := Op;

      FreeAndNil(QryIMon);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id   := QryFun.FieldByName('ID_FUNCIONARIO').AsInteger;
    CPF  := QryFun.FieldByName('CPF').AsString;
    Nome := QryFun.FieldByName('NOME_FUNCIONARIO').AsString;
  end;

  FreeAndNil(QryFun);
end;

procedure TdmPrincipal.RetornarGenerico(Tipo: String; var Id, IdFK, Codigo: Integer;
  var Descricao, Observacao: String);
var
  QryGen, QryI, QryIMon: TFDQuery;
  Op: TOperacao;
  sCPF_CNPJ: String;
begin
  if Trim(Tipo) = 'CARGO' then
    QryGen := dmGerencial.CriarFDQuery(nil, vgConGERAux)
  else
    QryGen := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  //Consulta
  QryGen.Close;
  QryGen.SQL.Clear;

  case AnsiIndexStr(UpperCase(Tipo), ['CARGO',
                                      'CATEGORIA_DESPREC',
                                      'SUBCATEGORIA_DESPREC',
                                      'NATUREZA']) of
    0:  { CARGO }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM CARGO ';

      if Codigo > 0 then
      begin
        QryGen.SQL.Add(' WHERE COD_CARGO = :COD_CARGO');
        QryGen.Params.ParamByName('COD_CARGO').Value := Codigo;
      end
      else if Trim(Descricao) <> '' then
      begin
        QryGen.SQL.Add(' WHERE DESCR_CARGO = :DESCR_CARGO');
        QryGen.Params.ParamByName('DESCR_CARGO').Value := Descricao;
      end;
    end;
    1:  { CATEGORIA }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM CATEGORIA_DESPREC ' +
                       ' WHERE TIPO = :TIPO ';

      if Codigo = 1 then
        QryGen.Params.ParamByName('TIPO').Value := 'D'  //Despesa
      else if Codigo = 2 then
        QryGen.Params.ParamByName('TIPO').Value := 'R';  //Receita

      QryGen.SQL.Add(' AND DESCR_CATEGORIA_DESPREC = :DESCR_CATEGORIA_DESPREC ' +
                     ' AND ID_NATUREZA_FK    = :ID_NATUREZA');
      QryGen.Params.ParamByName('DESCR_CATEGORIA_DESPREC').Value := Descricao;
      QryGen.Params.ParamByName('ID_NATUREZA').Value             := IdFK;
    end;
    2:  { SUBCATEGORIA }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM SUBCATEGORIA_DESPREC ' +
                       ' WHERE TIPO = :TIPO ';

      if Codigo = 1 then
        QryGen.Params.ParamByName('TIPO').Value := 'D'  //Despesa
      else if Codigo = 2 then
        QryGen.Params.ParamByName('TIPO').Value := 'R';  //Receita

      QryGen.SQL.Add(' AND DESCR_SUBCATEGORIA_DESPREC = :DESCR_SUBCATEGORIA_DESPREC ' +
                     ' AND ID_CATEGORIA_DESPREC_FK    = :ID_CATEGORIA_DESPREC');

      QryGen.Params.ParamByName('DESCR_SUBCATEGORIA_DESPREC').Value := Descricao;
      QryGen.Params.ParamByName('ID_CATEGORIA_DESPREC').Value       := IdFK;
    end;
    3:  { NATUREZA }
    begin
      QryGen.SQL.Text := 'SELECT * ' +
                       '  FROM NATUREZA ' +
                       ' WHERE TIPO_NATUREZA = :TIPO_NATUREZA ' +
                       '   AND DESCR_NATUREZA = :DESCR_NATUREZA';

      if Codigo = 1 then
        QryGen.Params.ParamByName('TIPO_NATUREZA').Value := 'D'  //Despesa
      else if Codigo = 2 then
        QryGen.Params.ParamByName('TIPO_NATUREZA').Value := 'R';  //Receita

      QryGen.Params.ParamByName('DESCR_NATUREZA').Value := Descricao;
    end;
  end;

  QryGen.Open;

  if QryGen.RecordCount = 0 then
  begin
    try
      if Trim(Tipo) = 'CARGO' then
      begin
        if vgConGERAux.Connected then
          vgConGERAux.StartTransaction;

        if vgConSISTEMAAux.Connected then
          vgConSISTEMAAux.StartTransaction;

        QryI := dmGerencial.CriarFDQuery(nil, vgConGERAux);
      end
      else
      begin
        if vgConSISTEMAAux.Connected then
          vgConSISTEMAAux.StartTransaction;

        QryI := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);
      end;

      //Inclusao
      QryI.Close;
      QryI.SQL.Clear;

      case AnsiIndexStr(UpperCase(Tipo), ['CARGO',
                                          'CATEGORIA_DESPREC',
                                          'SUBCATEGORIA_DESPREC',
                                          'NATUREZA']) of
        0:  { CARGO }
        begin
          QryI.SQL.Text := 'INSERT INTO CARGO (ID_CARGO, DESCR_CARGO, COD_CARGO) ' +
                           '           VALUES (:ID_CARGO, :DESCR_CARGO, :COD_CARGO)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_CARGO', 'CARGO');

          QryI.Params.ParamByName('ID_CARGO').Value    := Id;
          QryI.Params.ParamByName('DESCR_CARGO').Value := Descricao;
          QryI.Params.ParamByName('COD_CARGO').Value   := Codigo;
        end;
        1:  { CATEGORIA }
        begin
          QryI.SQL.Text := 'INSERT INTO CATEGORIA_DESPREC (ID_CATEGORIA_DESPREC, ' +
                           '                               DESCR_CATEGORIA_DESPREC, ' +
                           '                               ID_NATUREZA_FK, ' +
                           '                               TIPO) ' +
                           '           VALUES (:ID_CATEGORIA_DESPREC, ' +
                           '                   :DESCR_CATEGORIA_DESPREC, ' +
                           '                   :ID_NATUREZA, ' +
                           '                   :TIPO)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_CATEGORIA_DESPREC', 'CATEGORIA_DESPREC');

          QryI.Params.ParamByName('ID_CATEGORIA_DESPREC').Value    := Id;
          QryI.Params.ParamByName('DESCR_CATEGORIA_DESPREC').Value := Descricao;
          QryI.Params.ParamByName('ID_NATUREZA').Value             := IdFK;
          QryI.Params.ParamByName('TIPO').Value                    := IfThen((Codigo = 1), 'D', 'R');
        end;
        2:  { SUBCATEGORIA }
        begin
          QryI.SQL.Text := 'INSERT INTO SUBCATEGORIA_DESPREC (ID_SUBCATEGORIA_DESPREC, ' +
                           '                                  DESCR_SUBCATEGORIA_DESPREC, ' +
                           '                                  ID_CATEGORIA_DESPREC_FK, ' +
                           '                                  TIPO) ' +
                           '                          VALUES (:ID_SUBCATEGORIA_DESPREC, ' +
                           '                                  :DESCR_SUBCATEGORIA_DESPREC, ' +
                           '                                  :ID_CATEGORIA_DESPREC, ' +
                           '                                  :TIPO)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_SUBCATEGORIA_DESPREC', 'SUBCATEGORIA_DESPREC');

          QryI.Params.ParamByName('ID_SUBCATEGORIA_DESPREC').Value    := Id;
          QryI.Params.ParamByName('DESCR_SUBCATEGORIA_DESPREC').Value := Descricao;
          QryI.Params.ParamByName('ID_CATEGORIA_DESPREC').Value       := IdFK;
          QryI.Params.ParamByName('TIPO').Value                       := IfThen((Codigo = 1), 'D', 'R');
        end;
        3:  { NATUREZA }
        begin
          QryI.SQL.Text := 'INSERT INTO NATUREZA (ID_NATUREZA, DESCR_NATUREZA, ' +
                           '                      TIPO_NATUREZA, FLG_EDITAVEL) ' +
                           '              VALUES (:ID_NATUREZA, :DESCR_NATUREZA, ' +
                           '                      :TIPO_NATUREZA, :FLG_EDITAVEL)' ;

          if Id = 0 then
            Id := BS.ProximoId('ID_NATUREZA', 'NATUREZA');

          QryI.Params.ParamByName('ID_NATUREZA').Value    := Id;
          QryI.Params.ParamByName('DESCR_NATUREZA').Value := Descricao;
          QryI.Params.ParamByName('TIPO_NATUREZA').Value  := IfThen((Codigo = 1), 'D', 'R');
          QryI.Params.ParamByName('FLG_EDITAVEL').Value   := 'S';
        end;
      end;

      QryI.ExecSQL;

      if Trim(Tipo) = 'CARGO' then
      begin
        QryIMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

        QryIMon.Close;
        QryIMon.SQL.Clear;
        QryIMon.SQL.Text := 'INSERT INTO CARGO (ID_CARGO, DESCR_CARGO, COD_CARGO) ' +
                            '           VALUES (:ID_CARGO, :DESCR_CARGO, :COD_CARGO)';
        QryIMon.Params.ParamByName('ID_CARGO').Value    := Id;
        QryIMon.Params.ParamByName('DESCR_CARGO').Value := Descricao;
        QryIMon.Params.ParamByName('COD_CARGO').Value   := Codigo;

        QryIMon.ExecSQL;

        FreeAndNil(QryIMon);
      end;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao, Id, Tipo);
      vgOperacao := Op;

      FreeAndNil(QryI);

      if Trim(Tipo) = 'CARGO' then
      begin
        if vgConGERAux.InTransaction then
          vgConGERAux.Commit;

        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Commit;
      end
      else
      begin
        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Commit;
      end;
    except
      if Trim(Tipo) = 'CARGO' then
      begin
        if vgConGERAux.InTransaction then
          vgConGERAux.Rollback;

        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Rollback;
      end
      else
      begin
        if vgConSISTEMAAux.InTransaction then
          vgConSISTEMAAux.Rollback;
      end;
    end;
  end
  else
  begin
    case AnsiIndexStr(UpperCase(Tipo), ['CARGO',
                                        'CATEGORIA_DESPREC',
                                        'SUBCATEGORIA_DESPREC',
                                        'NATUREZA']) of
      0:  { CARGO }
      begin
        Id        := QryGen.FieldByName('ID_CARGO').AsInteger;
        Codigo    := QryGen.FieldByName('COD_CARGO').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_CARGO').AsString;
      end;
      1:  { CATEGORIA }
      begin
        Id        := QryGen.FieldByName('ID_CATEGORIA_DESPREC').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_CATEGORIA_DESPREC').AsString;
      end;
      2:  { SUBCATEGORIA }
      begin
        Id        := QryGen.FieldByName('ID_SUBCATEGORIA_DESPREC').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_SUBCATEGORIA_DESPREC').AsString;
      end;
      3:  { NATUREZA }
      begin
        Id        := QryGen.FieldByName('ID_NATUREZA').AsInteger;
        Descricao := QryGen.FieldByName('DESCR_NATUREZA').AsString;
      end;
    end;
  end;

  FreeAndNil(QryGen);
end;

procedure TdmPrincipal.RetornarItem(Tipo: String; var Id: Integer;
  var Descricao, Abreviacao, Observacao, UMedida: String; var Medida: Real);
var
  QryItem, QryI: TFDQuery;
  Op: TOperacao;
begin
  QryItem := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

  //Consulta
  QryItem.Close;
  QryItem.SQL.Clear;

  QryItem.SQL.Text := 'SELECT ID_ITEM ' +
                      '       DESCR_ITEM, ' +
                      '       ABREV_ITEM' +
                      '  FROM ITEM ' +
                      ' WHERE DESCR_ITEM = :DESCR_ITEM';

  QryItem.Params.ParamByName('DESCR_ITEM').Value := Descricao;

  if Trim(Tipo) = '' then
    QryItem.SQL.Add(' AND TIPO_ITEM IS NULL')
  else
  begin
    QryItem.SQL.Add(' AND TIPO_ITEM = :TIPO_ITEM');
    QryItem.Params.ParamByName('TIPO_ITEM').Value := Tipo;
  end;

  QryItem.Open;

  if QryItem.RecordCount = 0 then
  begin
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryI.Close;
      QryI.SQL.Clear;
      QryI.SQL.Text := 'INSERT INTO ITEM (ID_ITEM, TIPO_ITEM, DESCR_ITEM, ' +
                       '                  ABREV_ITEM, ULT_VALOR_CUSTO, ' +
                       '                  ESTOQUE_ATUAL, MEDIDA_PADRAO, ' +
                       '                  DESCR_UNIDADEMEDIDA, DATA_CADASTRO, ' +
                       '                  OBS_ITEM, FLG_ATIVO, DATA_INATIVACAO) ' +
                       '          VALUES (:ID_ITEM, :TIPO_ITEM, :DESCR_ITEM, ' +
                       '                  :ABREV_ITEM, :ULT_VALOR_CUSTO, ' +
                       '                  :ESTOQUE_ATUAL, :MEDIDA_PADRAO, ' +
                       '                  :DESCR_UNIDADEMEDIDA, :DATA_CADASTRO, ' +
                       '                  :OBS_ITEM, :FLG_ATIVO, :DATA_INATIVACAO)' ;

      if Id = 0 then
        Id := BS.ProximoId('ID_ITEM', 'ITEM');

      QryI.Params.ParamByName('ID_ITEM').Value             := Id;
      QryI.Params.ParamByName('TIPO_ITEM').Value           := Tipo;
      QryI.Params.ParamByName('DESCR_ITEM').Value          := Descricao;

      if Trim(Abreviacao) = '' then
        QryI.Params.ParamByName('ABREV_ITEM').Value        := Null
      else
        QryI.Params.ParamByName('ABREV_ITEM').Value        := Abreviacao;

      QryI.Params.ParamByName('ULT_VALOR_CUSTO').Value     := Null;
      QryI.Params.ParamByName('ESTOQUE_ATUAL').Value       := 0;
      QryI.Params.ParamByName('MEDIDA_PADRAO').Value       := Medida;
      QryI.Params.ParamByName('DESCR_UNIDADEMEDIDA').Value := UMedida;
      QryI.Params.ParamByName('DATA_CADASTRO').Value       := Date;
      QryI.Params.ParamByName('OBS_ITEM').Value            := Observacao;
      QryI.Params.ParamByName('FLG_ATIVO').Value           := 'S';
      QryI.Params.ParamByName('DATA_INATIVACAO').Value     := Null;

      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;
      BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Item',
                          Id, 'ITEM');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id         := QryItem.FieldByName('ID_ITEM').AsInteger;
    Descricao  := QryItem.FieldByName('DESCR_ITEM').AsString;
    Abreviacao := QryItem.FieldByName('ABREV_ITEM').AsString;
  end;

  FreeAndNil(QryItem);
end;

procedure TdmPrincipal.RetornarUsuario(var Id, IdFuncionario: Integer; var Nome,
  CPF, Matricula, Cargo, Senha, Observacao: String);
var
  QryUsu, QryI, QryIMon: TFDQuery;
  Op: TOperacao;
  sCPF: String;
begin
  QryUsu := dmGerencial.CriarFDQuery(nil, vgConGERAux);

  //Consulta
  QryUsu.Close;
  QryUsu.SQL.Clear;
  QryUsu.SQL.Text := 'SELECT * ' +
                     '  FROM USUARIO ';

  if Trim(CPF) <> '' then
  begin
    QryUsu.SQL.Add(' WHERE REPLACE(REPLACE(CPF, ' +
                                           QuotedStr('-') + ', ' +
                                           QuotedStr('') + '), ' +
                                   QuotedStr('.') + ', ' +
                                   QuotedStr('') + ') = :CPF ');
    QryUsu.Params.ParamByName('CPF').Value  := dmGerencial.PegarNumeroTexto(CPF);
  end
  else if Trim(Nome) <> '' then
  begin
    QryUsu.SQL.Add(' WHERE NOME = :NOME');
    QryUsu.Params.ParamByName('NOME').Value := Nome;
  end;

  QryUsu.Open;

  if QryUsu.RecordCount = 0 then
  begin
    //Gerencial
    try
      if vgConGERAux.Connected then
        vgConGERAux.StartTransaction;

      //Inclusao
      QryI := dmGerencial.CriarFDQuery(nil, vgConGERAux);

      QryI.Close;
      QryI.SQL.Clear;
      QryI.SQL.Text := 'INSERT INTO USUARIO (ID_USUARIO, NOME, CPF, SENHA, QUALIFICACAO, ' +
                       '                     MATRICULA, ID_FUNCIONARIO_FK, FLG_MASTER, ' +
                       '                     FLG_SUPORTE, FLG_VISUALIZOU_ATU, FLG_ATIVO, ' +
                       '                     DATA_CADASTRO) ' +
                       '             VALUES (:ID_USUARIO, :NOME, :CPF, :SENHA, :QUALIFICACAO, ' +
                       '                     :MATRICULA, :ID_FUNCIONARIO, :FLG_MASTER, ' +
                       '                     :FLG_SUPORTE, :FLG_VISUALIZOU_ATU, :FLG_ATIVO, ' +
                       '                     :DATA_CADASTRO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      if Id = 0 then
        Id := BS.ProximoId('ID_USUARIO', 'USUARIO');

      QryI.Params.ParamByName('ID_USUARIO').Value         := Id;
      QryI.Params.ParamByName('NOME').Value               := Nome;
      QryI.Params.ParamByName('CPF').Value                := sCPF;
      QryI.Params.ParamByName('SENHA').Value              := Senha;
      QryI.Params.ParamByName('QUALIFICACAO').Value       := Cargo;

      if Trim(Matricula) = '' then
        QryI.Params.ParamByName('MATRICULA').Value        := Null
      else
        QryI.Params.ParamByName('MATRICULA').Value        := Matricula;

      QryI.Params.ParamByName('ID_FUNCIONARIO').Value     := IdFuncionario;
      QryI.Params.ParamByName('FLG_MASTER').Value         := 'N';
      QryI.Params.ParamByName('FLG_SUPORTE').Value        := 'N';
      QryI.Params.ParamByName('FLG_VISUALIZOU_ATU').Value := 'S';
      QryI.Params.ParamByName('FLG_ATIVO').Value          := 'S';
      QryI.Params.ParamByName('DATA_CADASTRO').Value      := Date;
      QryI.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'USUARIO');
      vgOperacao := Op;

      FreeAndNil(QryI);

      if vgConGERAux.InTransaction then
        vgConGERAux.Commit;
    except
      if vgConGERAux.InTransaction then
        vgConGERAux.Rollback;
    end;

    //Monitoramento
    try
      if vgConSISTEMAAux.Connected then
        vgConSISTEMAAux.StartTransaction;

      //Inclusao
      QryIMon := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryIMon.Close;
      QryIMon.SQL.Clear;
      QryIMon.SQL.Text := 'INSERT INTO USUARIO (ID_USUARIO, NOME, CPF, SENHA, QUALIFICACAO, ' +
                       '                     MATRICULA, ID_FUNCIONARIO_FK, FLG_MASTER, ' +
                       '                     FLG_SUPORTE, FLG_VISUALIZOU_ATU, FLG_ATIVO, ' +
                       '                     DATA_CADASTRO) ' +
                       '             VALUES (:ID_USUARIO, :NOME, :CPF, :SENHA, :QUALIFICACAO, ' +
                       '                     :MATRICULA, :ID_FUNCIONARIO, :FLG_MASTER, ' +
                       '                     :FLG_SUPORTE, :FLG_VISUALIZOU_ATU, :FLG_ATIVO, ' +
                       '                     :DATA_CADASTRO)' ;

      sCPF := Copy(dmGerencial.PegarNumeroTexto(CPF), 1, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 4, 3) + '.' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 7, 3) + '-' +
              Copy(dmGerencial.PegarNumeroTexto(CPF), 10, 2);

      QryIMon.Params.ParamByName('ID_USUARIO').Value         := Id;
      QryIMon.Params.ParamByName('NOME').Value               := Nome;
      QryIMon.Params.ParamByName('CPF').Value                := sCPF;
      QryIMon.Params.ParamByName('SENHA').Value              := Senha;
      QryIMon.Params.ParamByName('QUALIFICACAO').Value       := Cargo;
      QryIMon.Params.ParamByName('MATRICULA').Value          := Matricula;
      QryIMon.Params.ParamByName('ID_FUNCIONARIO').Value     := IdFuncionario;
      QryIMon.Params.ParamByName('FLG_MASTER').Value         := 'N';
      QryIMon.Params.ParamByName('FLG_SUPORTE').Value        := 'N';
      QryIMon.Params.ParamByName('FLG_VISUALIZOU_ATU').Value := 'S';
      QryIMon.Params.ParamByName('FLG_ATIVO').Value          := 'S';
      QryIMon.Params.ParamByName('DATA_CADASTRO').Value      := Date;
      QryIMon.ExecSQL;

      Op := vgOperacao;
      vgOperacao := I;

      BS.GravarUsuarioLog(80, '', Observacao,
                          Id, 'USUARIO');
      vgOperacao := Op;

      FreeAndNil(QryIMon);

      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Commit;
    except
      if vgConSISTEMAAux.InTransaction then
        vgConSISTEMAAux.Rollback;
    end;
  end
  else
  begin
    Id            := QryUsu.FieldByName('ID_USUARIO').AsInteger;
    IdFuncionario := QryUsu.FieldByName('ID_FUNCIONARIO_FK').AsInteger;
    CPF           := QryUsu.FieldByName('CPF').AsString;
    Nome          := QryUsu.FieldByName('NOME').AsString;
    Matricula     := QryUsu.FieldByName('MATRICULA').AsString;
    Cargo         := QryUsu.FieldByName('QUALIFICACAO').AsString;
    Senha         := QryUsu.FieldByName('SENHA').AsString;
  end;

  FreeAndNil(QryUsu);
end;

procedure TdmPrincipal.RetornaSubCategoria(var Id, IdSubCategoria, Codigo: Integer;
  var Descricao, Observacao: String);
begin
  RetornarGenerico('SUBCATEGORIA_DESPREC', Id, IdSubCategoria, Codigo, Descricao, Observacao);
end;

procedure TdmPrincipal.SincronizarDadosSistemaGerencialMonitoramento(REC, FIR: Boolean);
var
  sLstCancel: String;
begin
  sLstCancel := '';

  { Receitas - Recibo }
  if vgImport_Rec and REC then
  begin
    //Atualiza recibos do RECIBO no MONITORAMENTO
    //Incluidos - Talao
    iQtdParc := 0;
    iQtdTot  := ExistemRecibosPendentesTalao;

    if iQtdTot > 0 then
    begin
      repeat
        Application.CreateForm(TdmRecibo, dmRecibo);
        dmRecibo.IncluirRecibosPendentesTalao;
        FreeAndNil(dmRecibo);
      until ExistemRecibosPendentesTalao = 0;

      iQtdParc := 0;
      iQtdTot  := 0;
    end;

    //Cancelados - Talao
    if (vgContagemImportacao = 1) or
    (vgContagemImportacao mod 5 = 0) then
    begin
      sLstCancel := ExistemRecibosCanceladosTalao;

      if Trim(sLstCancel) <> '' then
      begin
        Application.CreateForm(TdmRecibo, dmRecibo);
        dmRecibo.AtualizacoesRecibosPendentes('T', sLstCancel);
        FreeAndNil(dmRecibo);
      end;
    end;

    //Incluidos - Balcao
    iQtdParc := 0;
    iQtdTot  := ExistemRecibosPendentesBalcao;

    if iQtdTot > 0 then
    begin
      repeat
        Application.CreateForm(TdmRecibo, dmRecibo);
        dmRecibo.IncluirRecibosPendentesBalcao;
        FreeAndNil(dmRecibo);
      until ExistemRecibosPendentesBalcao = 0;

      iQtdParc := 0;
      iQtdTot  := 0;
    end;

    //Cancelados - Balcao
    if (vgContagemImportacao = 1) or
    (vgContagemImportacao mod 5 = 0) then
    begin
      sLstCancel := ExistemRecibosCanceladosBalcao;

      if Trim(sLstCancel) <> '' then
      begin
        Application.CreateForm(TdmRecibo, dmRecibo);
        dmRecibo.AtualizacoesRecibosPendentes('B' ,sLstCancel);
        FreeAndNil(dmRecibo);
      end;
    end;
  end;

  sLstCancel := '';

  { Firmas }
  //Receitas - Firmas
  if vgImport_Fir and FIR then
  begin
    //Atualiza recibos do FIRMAS no MONITORAMENTO
    //Incluidos
    iQtdParc := 0;
    iQtdTot  := ExistemFirmasPendentes;

    if iQtdTot > 0 then
    begin
      repeat
        Application.CreateForm(TdmFirmas, dmFirmas);
        dmFirmas.IncluirFirmasPendentes;
        FreeAndNil(dmFirmas);
      until ExistemFirmasPendentes = 0;

      iQtdParc := 0;
      iQtdTot  := 0;
    end;

    //Recibos Cancelados
    if (vgContagemImportacao = 1) or
    (vgContagemImportacao mod 5 = 0) then
    begin
      sLstCancel := ExistemFirmasCanceladas;

      if Trim(sLstCancel) <> '' then
      begin
        Application.CreateForm(TdmFirmas, dmFirmas);
        dmFirmas.AtualizacoesFirmasPendentes(sLstCancel);
        FreeAndNil(dmFirmas);
      end;
    end;
  end;

  //Receitas - Firmas Antigo
  if vgImport_FirA and FIR then
  begin
    //Atualiza tabelas do FIRMAS ANTIGO no MONITORAMENTO
    Application.CreateForm(TdmFirmas, dmFirmas);
    dmFirmas.AtualizaTabelasFirmasAntigoMonitoramento;
    FreeAndNil(dmFirmas);

    //Atualiza recibos do FIRMAS ANTIGO no MONITORAMENTO
    iQtdParc := 0;
    iQtdTot  := ExistemFirmasAntigasPendentes;

    if iQtdTot > 0 then
    begin
      repeat
        Application.CreateForm(TdmFirmas, dmFirmas);
        dmFirmas.IncluirFirmasAntigoPendentes;
        FreeAndNil(dmFirmas);
      until ExistemFirmasAntigasPendentes = 0;

      iQtdParc := 0;
      iQtdTot  := 0;
    end;
  end;
end;

procedure TdmPrincipal.UpdateLancamento(Indice: Integer);
var
  QryLanc: TFDQuery;
  sQry: String;
begin
  QryLanc := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  sQry := '';

  with QryLanc, SQL do
  begin
    { LANCAMENTO }
    Close;
    Clear;

    Text := 'UPDATE LANCAMENTO SET ';

    if ListaLancamentos[Indice].DataLancamento <> 0 then
      sQry := 'DATA_LANCAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentos[Indice].DataLancamento));

    if ListaLancamentos[Indice].IdCategoria <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_CATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdCategoria)
      else
        sQry := sQry + ', ID_CATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdCategoria);
    end;

    if ListaLancamentos[Indice].IdSubCategoria <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_SUBCATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdSubCategoria)
      else
        sQry := sQry + ', ID_SUBCATEGORIA_DESPREC_FK = ' + IntToStr(ListaLancamentos[Indice].IdSubCategoria);
    end;

    if ListaLancamentos[Indice].FlgIR <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_IMPOSTORENDA = ' + QuotedStr(ListaLancamentos[Indice].FlgIR)
      else
        sQry := sQry + ', FLG_IMPOSTORENDA = ' + QuotedStr(ListaLancamentos[Indice].FlgIR);
    end;

    if ListaLancamentos[Indice].FlgPessoal <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_PESSOAL = ' + QuotedStr(ListaLancamentos[Indice].FlgPessoal)
      else
        sQry := sQry + ', FLG_PESSOAL = ' + QuotedStr(ListaLancamentos[Indice].FlgPessoal);
    end;

    if ListaLancamentos[Indice].FlgAuxiliar <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_AUXILIAR = ' + QuotedStr(ListaLancamentos[Indice].FlgAuxiliar)
      else
        sQry := sQry + ', FLG_AUXILIAR = ' + QuotedStr(ListaLancamentos[Indice].FlgAuxiliar);
    end;

    if ListaLancamentos[Indice].FlgForaFechCaixa <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_FORAFECHCAIXA = ' + QuotedStr(ListaLancamentos[Indice].FlgForaFechCaixa)
      else
        sQry := sQry + ', FLG_FORAFECHCAIXA = ' + QuotedStr(ListaLancamentos[Indice].FlgForaFechCaixa);
    end;

    if ListaLancamentos[Indice].IdNatureza <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_NATUREZA_FK = ' + IntToStr(ListaLancamentos[Indice].IdNatureza)
      else
        sQry := sQry + ', ID_NATUREZA_FK = ' + IntToStr(ListaLancamentos[Indice].IdNatureza);
    end;

    if ListaLancamentos[Indice].QtdParcelas <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'QTD_PARCELAS = ' + IntToStr(ListaLancamentos[Indice].QtdParcelas)
      else
        sQry := sQry + ', QTD_PARCELAS = ' + IntToStr(ListaLancamentos[Indice].QtdParcelas);
    end;

    if ListaLancamentos[Indice].VlrTotalPrev <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_PREV = :VR_TOTAL_PREV '
      else
        sQry := sQry + ', VR_TOTAL_PREV = :VR_TOTAL_PREV ';
    end;

    if ListaLancamentos[Indice].VlrTotalJuros <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_JUROS = :VR_TOTAL_JUROS '
      else
        sQry := sQry + ', VR_TOTAL_JUROS = :VR_TOTAL_JUROS ';
    end;

    if ListaLancamentos[Indice].VlrTotalDesconto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_DESCONTO = :VR_TOTAL_DESCONTO '
      else
        sQry := sQry + ', VR_TOTAL_DESCONTO = :VR_TOTAL_DESCONTO ';
    end;

    if ListaLancamentos[Indice].VlrTotalPago <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL_PAGO = :VR_TOTAL_PAGO '
      else
        sQry := sQry + ', VR_TOTAL_PAGO = :VR_TOTAL_PAGO ';
    end;

    if ListaLancamentos[Indice].CadIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CAD_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CadIdUsuario)
      else
        sQry := sQry + ', CAD_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CadIdUsuario);
    end;

    if ListaLancamentos[Indice].DataCadastro <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD hh:mm:ss', ListaLancamentos[Indice].DataCadastro))
      else
        sQry := sQry + ', DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD hh:mm:ss', ListaLancamentos[Indice].DataCadastro));
    end;

    if ListaLancamentos[Indice].FlgStatus <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_STATUS = ' + QuotedStr(ListaLancamentos[Indice].FlgStatus)
      else
        sQry := sQry + ', FLG_STATUS = ' + QuotedStr(ListaLancamentos[Indice].FlgStatus);
    end;

    if ListaLancamentos[Indice].FlgCancelado <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_CANCELADO = ' + QuotedStr(ListaLancamentos[Indice].FlgCancelado)
      else
        sQry := sQry + ', FLG_CANCELADO = ' + QuotedStr(ListaLancamentos[Indice].FlgCancelado);
    end;

    if ListaLancamentos[Indice].DataCancelamento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentos[Indice].DataCancelamento))
      else
        sQry := sQry + ', DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentos[Indice].DataCancelamento));
    end;

    if ListaLancamentos[Indice].CancelIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CancelIdUsuario)
      else
        sQry := sQry + ', CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentos[Indice].CancelIdUsuario);
    end;

    if ListaLancamentos[Indice].FlgRecorrente <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_RECORRENTE = ' + QuotedStr(ListaLancamentos[Indice].FlgRecorrente)
      else
        sQry := sQry + ', FLG_RECORRENTE = ' + QuotedStr(ListaLancamentos[Indice].FlgRecorrente);
    end;

    if ListaLancamentos[Indice].FlgReplicado <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_REPLICADO = ' + QuotedStr(ListaLancamentos[Indice].FlgReplicado)
      else
        sQry := sQry + ', FLG_REPLICADO = ' + QuotedStr(ListaLancamentos[Indice].FlgReplicado);
    end;

    if ListaLancamentos[Indice].NumRecibo > -1 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'NUM_RECIBO = ' + IntToStr(ListaLancamentos[Indice].NumRecibo)
      else
        sQry := sQry + ', NUM_RECIBO = ' + IntToStr(ListaLancamentos[Indice].NumRecibo);
    end;

    if ListaLancamentos[Indice].Observacao <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'OBSERVACAO = ' + QuotedStr(ListaLancamentos[Indice].Observacao)
      else
        sQry := sQry + ', OBSERVACAO = ' + QuotedStr(ListaLancamentos[Indice].Observacao);
    end;

    if ListaLancamentos[Indice].IdEquivalencia <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_CARNELEAO_EQUIVALENCIA_FK = ' + IntToStr(ListaLancamentos[Indice].IdEquivalencia)
      else
        sQry := sQry + ', ID_CARNELEAO_EQUIVALENCIA_FK = ' + IntToStr(ListaLancamentos[Indice].IdEquivalencia);
    end;

    if Trim(sQry) = '' then
      Exit;

    Add(sQry +
        ' WHERE COD_LANCAMENTO = :COD_LANCAMENTO ' +
        '   AND ANO_LANCAMENTO = :ANO_LANCAMENTO');

    if ListaLancamentos[Indice].VlrTotalPrev <> 0 then
      Params.ParamByName('VR_TOTAL_PREV').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalPrev), 2);

    if ListaLancamentos[Indice].VlrTotalJuros <> 0 then
      Params.ParamByName('VR_TOTAL_JUROS').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalJuros), 2);

    if ListaLancamentos[Indice].VlrTotalDesconto <> 0 then
      Params.ParamByName('VR_TOTAL_DESCONTO').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalDesconto), 2);

    if ListaLancamentos[Indice].VlrTotalPago <> 0 then
      Params.ParamByName('VR_TOTAL_PAGO').Value := dmGerencial.NoRound((ListaLancamentos[Indice].VlrTotalPago), 2);

    Params.ParamByName('COD_LANCAMENTO').Value := ListaLancamentos[Indice].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value := ListaLancamentos[Indice].AnoLancamento;

    ExecSQL;
  end;

  FreeAndNil(QryLanc);
end;

procedure TdmPrincipal.UpdateLancamentoDet(Indice, IndiceL: Integer);
var
  QryLancD: TFDQuery;
  sQry: String;
begin
  QryLancD := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  sQry := '';

  with QryLancD, SQL do
  begin
    { LANCAMENTO - DETALHE }
    Close;
    Clear;

    Text := 'UPDATE LANCAMENTO_DET SET ';

    if ListaLancamentoDets[Indice].VlrUnitario <> 0 then
      sQry := 'VR_UNITARIO = :VR_UNITARIO ';

    if ListaLancamentoDets[Indice].Quantidade <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'QUANTIDADE = ' + CurrToStr(ListaLancamentoDets[Indice].Quantidade)
      else
        sQry := sQry + ', QUANTIDADE = ' + CurrToStr(ListaLancamentoDets[Indice].Quantidade);
    end;

    if ListaLancamentoDets[Indice].VlrTotal <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_TOTAL = :VR_TOTAL '
      else
        sQry := sQry + ', VR_TOTAL = :VR_TOTAL ';
    end;

    if ListaLancamentoDets[Indice].FlgCancelado <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_CANCELADO = ' + QuotedStr(ListaLancamentoDets[Indice].FlgCancelado)
      else
        sQry := sQry + ', FLG_CANCELADO = ' + QuotedStr(ListaLancamentoDets[Indice].FlgCancelado);
    end;

    if Trim(sQry) = '' then
      Exit;

    Add(sQry +
        ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
        '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ');

    if ListaLancamentoDets[Indice].VlrUnitario <> 0 then
      Params.ParamByName('VR_UNITARIO').Value := dmGerencial.NoRound((ListaLancamentoDets[Indice].VlrUnitario), 2);

    if ListaLancamentoDets[Indice].VlrTotal <> 0 then
      Params.ParamByName('VR_TOTAL').Value := dmGerencial.NoRound((ListaLancamentoDets[Indice].VlrTotal), 2);

    Params.ParamByName('COD_LANCAMENTO').Value := ListaLancamentos[IndiceL].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value := ListaLancamentos[IndiceL].AnoLancamento;

    if ListaLancamentoDets[Indice].IdItem > 0 then
    begin
      Add(' AND ID_ITEM_FK = :ID_ITEM');
      Params.ParamByName('ID_ITEM').Value      := ListaLancamentoDets[Indice].IdItem;
    end;

    if ListaLancamentoDets[Indice].IdLancamentoDet > 0 then
    begin
      Add(' AND ID_LANCAMENTO_DET = :ID_LANCAMENTO_DET');
      Params.ParamByName('ID_LANCAMENTO_DET').Value := ListaLancamentoDets[Indice].IdLancamentoDet;
    end;

    ExecSQL;
  end;

  FreeAndNil(QryLancD);
end;

procedure TdmPrincipal.UpdateLancamentoParc(Indice, IndiceL: Integer);
var
  QryLancP: TFDQuery;
  sQry: String;
begin
  QryLancP := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  sQry := '';

  with QryLancP, SQL do
  begin
    { LANCAMENTO - PARCELA }
    Close;
    Clear;

    Text := 'UPDATE LANCAMENTO_PARC SET ';

    if ListaLancamentoParcs[Indice].NumParcela <> 0 then
      sQry := 'NUM_PARCELA = ' + IntToStr(ListaLancamentoParcs[Indice].NumParcela);

    if ListaLancamentoParcs[Indice].IdFormaPagto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_CLIENTE_FORNECEDOR_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdFormaPagto)
      else
        sQry := sQry + ', ID_CLIENTE_FORNECEDOR_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdFormaPagto);
    end;

    if ListaLancamentoParcs[Indice].DataLancParc <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_LANCAMENTO_PARC = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataLancParc))
      else
        sQry := sQry + ', DATA_LANCAMENTO_PARC = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataLancParc));
    end;

    if ListaLancamentoParcs[Indice].DataVencimento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_VENCIMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataVencimento))
      else
        sQry := sQry + ', DATA_VENCIMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataVencimento));
    end;

    if ListaLancamentoParcs[Indice].VlrPrevisto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_PREV = :VR_PARCELA_PREV '
      else
        sQry := sQry + ', VR_PARCELA_PREV = :VR_PARCELA_PREV ';
    end;

    if ListaLancamentoParcs[Indice].VlrJuros <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_JUROS = :VR_PARCELA_JUROS '
      else
        sQry := sQry + ', VR_PARCELA_JUROS = :VR_PARCELA_JUROS ';
    end;

    if ListaLancamentoParcs[Indice].VlrDesconto <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_DESCONTO = :VR_PARCELA_DESCONTO '
      else
        sQry := sQry + ', VR_PARCELA_DESCONTO = :VR_PARCELA_DESCONTO ';
    end;

    if ListaLancamentoParcs[Indice].VlrPago <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'VR_PARCELA_PAGO = :VR_PARCELA_PAGO '
      else
        sQry := sQry + ', VR_PARCELA_PAGO = :VR_PARCELA_PAGO ';
    end;

    if ListaLancamentoParcs[Indice].Agencia <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'AGENCIA = ' + QuotedStr(ListaLancamentoParcs[Indice].Agencia)
      else
        sQry := sQry + ', AGENCIA = ' + QuotedStr(ListaLancamentoParcs[Indice].Agencia);
    end;

    if ListaLancamentoParcs[Indice].Conta <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CONTA = ' + QuotedStr(ListaLancamentoParcs[Indice].Conta)
      else
        sQry := sQry + ', FLG_ICONTAMPOSTORENDA = ' + QuotedStr(ListaLancamentoParcs[Indice].Conta);
    end;

    if ListaLancamentoParcs[Indice].IdBanco <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'ID_BANCO_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdBanco)
      else
        sQry := sQry + ', ID_BANCO_FK = ' + IntToStr(ListaLancamentoParcs[Indice].IdBanco);
    end;

    if ListaLancamentoParcs[Indice].NumCheque <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'NUM_CHEQUE = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCheque)
      else
        sQry := sQry + ', NUM_CHEQUE = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCheque);
    end;

    if ListaLancamentoParcs[Indice].NumCodDeposito <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'NUM_COD_DEPOSITO = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCodDeposito)
      else
        sQry := sQry + ', NUM_COD_DEPOSITO = ' + QuotedStr(ListaLancamentoParcs[Indice].NumCodDeposito);
    end;

    if ListaLancamentoParcs[Indice].FlgStatus <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'FLG_STATUS = ' + QuotedStr(ListaLancamentoParcs[Indice].FlgStatus)
      else
        sQry := sQry + ', FLG_STATUS = ' + QuotedStr(ListaLancamentoParcs[Indice].FlgStatus);
    end;

    if ListaLancamentoParcs[Indice].DataCancelamento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCancelamento))
      else
        sQry := sQry + ', DATA_CANCELAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCancelamento));

      sQry := sQry + ', DATA_PAGAMENTO = NULL';
      sQry := sQry + ', PAG_ID_USUARIO = NULL';
    end;

    if ListaLancamentoParcs[Indice].CancelIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CancelIdUsuario)
      else
        sQry := sQry + ', CANCEL_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CancelIdUsuario);
    end;

    if ListaLancamentoParcs[Indice].DataPagamento <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_PAGAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataPagamento))
      else
        sQry := sQry + ', DATA_PAGAMENTO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataPagamento));
    end;

    if ListaLancamentoParcs[Indice].PagIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'PAG_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].PagIdUsuario)
      else
        sQry := sQry + ', PAG_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].PagIdUsuario);
    end;

    if ListaLancamentoParcs[Indice].Observacao <> '' then
    begin
      if  Trim(sQry) = '' then
        sQry := 'OBS_LANCAMENTO_PARC = ' + QuotedStr(ListaLancamentoParcs[Indice].Observacao)
      else
        sQry := sQry + ', OBS_LANCAMENTO_PARC = ' + QuotedStr(ListaLancamentoParcs[Indice].Observacao);
    end;

    if ListaLancamentoParcs[Indice].DataCadastro <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCadastro))
      else
        sQry := sQry + ', DATA_CADASTRO = ' + QuotedStr(FormatDateTime('YYYY-MM-DD', ListaLancamentoParcs[Indice].DataCadastro));
    end;

    if ListaLancamentoParcs[Indice].CadIdUsuario <> 0 then
    begin
      if  Trim(sQry) = '' then
        sQry := 'CAD_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CadIdUsuario)
      else
        sQry := sQry + ', CAD_ID_USUARIO = ' + IntToStr(ListaLancamentoParcs[Indice].CadIdUsuario);
    end;

    if Trim(sQry) = '' then
      Exit;

    Add(sQry +
        ' WHERE COD_LANCAMENTO_FK = :COD_LANCAMENTO ' +
        '   AND ANO_LANCAMENTO_FK = :ANO_LANCAMENTO ');

    if ListaLancamentoParcs[Indice].VlrPrevisto <> 0 then
      Params.ParamByName('VR_PARCELA_PREV').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrPrevisto), 2);

    if ListaLancamentoParcs[Indice].VlrJuros <> 0 then
      Params.ParamByName('VR_PARCELA_JUROS').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrJuros), 2);

    if ListaLancamentoParcs[Indice].VlrDesconto <> 0 then
      Params.ParamByName('VR_PARCELA_DESCONTO').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrDesconto), 2);

    if ListaLancamentoParcs[Indice].VlrPago <> 0 then
      Params.ParamByName('VR_PARCELA_PAGO').Value := dmGerencial.NoRound((ListaLancamentoParcs[Indice].VlrPago), 2);

    Params.ParamByName('COD_LANCAMENTO').Value := ListaLancamentos[IndiceL].CodLancamento;
    Params.ParamByName('ANO_LANCAMENTO').Value := ListaLancamentos[IndiceL].AnoLancamento;

    if ListaLancamentoParcs[Indice].IdLancamentoParc > 0 then
    begin
      Add(' AND ID_LANCAMENTO_PARC = :ID_LANCAMENTO_PARC');
      Params.ParamByName('ID_LANCAMENTO_PARC').Value := ListaLancamentoParcs[Indice].IdLancamentoParc;
    end;

    ExecSQL;
  end;

  FreeAndNil(QryLancP);
end;

procedure TdmPrincipal.VerificarSituacaoCaixa;
var
  QryCx: TFDQuery;
begin
  //Atualiza as variaveis de Configuracao do Sistema
  dmGerencial.AtualizarVariaveisConfiguracaoSistema;

  //CAIXA
  vgSituacaoCaixa := '';

  vgDataUltAb   := 0;
  vgHoraUltAb   := 0;
  vgDataProxAb  := 0;
  vgVlrNotasAb  := 0;
  vgVlrMoedasAb := 0;
  vgVlrContaAb  := 0;
  vgVlrTotalAb  := 0;
  vgIdMovAb     := 0;

  vgDataUltFech   := 0;
  vgHoraUltFech   := 0;
  vgDataProxFech  := 0;
  vgVlrNotasFech  := 0;
  vgVlrMoedasFech := 0;
  vgVlrContaFech  := 0;
  vgVlrTotalFech  := 0;
  vgIdMovFech     := 0;

  vgDataLancVigente := 0;

  QryCx := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  //Verifica a Situacao atual do Caixa
  QryCx.Close;
  QryCx.SQL.Clear;
  QryCx.SQL.Text := 'SELECT FIRST 1 CX.* ' +
                    '  FROM (SELECT A.DATA_ABERTURA AS DATA, ' +
                    '               A.HORA_ABERTURA AS HORA, ' +
                    '               ' + QuotedStr('A') + ' AS TIPO ' +
                    '          FROM CAIXA_ABERTURA A ' +
                    '         WHERE FLG_CANCELADO = ' + QuotedStr('N') +
                    '        UNION ' +
                    '        SELECT F.DATA_FECHAMENTO AS DATA, ' +
                    '               F.HORA_FECHAMENTO AS HORA, ' +
                    '               ' + QuotedStr('F') + ' AS TIPO ' +
                    '          FROM CAIXA_FECHAMENTO F ' +
                    '         WHERE FLG_CANCELADO = ' + QuotedStr('N') + ') CX ' +
                    'ORDER BY CX.DATA DESC, ' +
                    '         CX.HORA DESC';
  QryCx.Open;

  vgSituacaoCaixa := IfThen(QryCx.FieldByName('TIPO').IsNull,
                            'N',
                            QryCx.FieldByName('TIPO').AsString);

  //Abertura de Caixa
  QryCx.Close;
  QryCx.SQL.Clear;
  QryCx.SQL.Text := 'SELECT A.VR_TOTAL_ABERTURA AS VR_TOTAL, ' +
                    '       A.DATA_ABERTURA AS DATA, ' +
                    '       A.HORA_ABERTURA AS HORA, ' +
                    '       A.VR_NOTAS, ' +
                    '       A.VR_MOEDAS, ' +
                    '       A.VR_CONTACORRENTE, ' +
                    '       (SELECT M.ID_MOVIMENTO ' +
                    '          FROM MOVIMENTO M ' +
                    '         WHERE M.ID_ORIGEM = A.ID_CAIXA_ABERTURA ' +
                    '           AND M.TIPO_ORIGEM = ' + QuotedStr('A') + ') ID_ULT_MOV ' +
                    '  FROM CAIXA_ABERTURA A ' +
                    ' WHERE A.FLG_CANCELADO = ' + QuotedStr('N') +
                    '   AND A.DATA_ABERTURA = (SELECT MAX(CA.DATA_ABERTURA) ' +
                    '                            FROM CAIXA_ABERTURA CA ' +
                    '                           WHERE CA.FLG_CANCELADO = ' + QuotedStr('N') + ')';
  QryCx.Open;

  if QryCx.RecordCount > 0 then
  begin
    vgDataUltAb     := QryCx.FieldByName('DATA').AsDateTime;
    vgHoraUltAb     := StrToTime(QryCx.FieldByName('HORA').AsString);
    vgVlrNotasAb    := QryCx.FieldByName('VR_NOTAS').AsFloat;
    vgVlrMoedasAb   := QryCx.FieldByName('VR_MOEDAS').AsFloat;
    vgVlrContaAb    := QryCx.FieldByName('VR_CONTACORRENTE').AsFloat;
    vgVlrTotalAb    := QryCx.FieldByName('VR_TOTAL').AsFloat;
    vgIdMovAb       := QryCx.FieldByName('ID_ULT_MOV').AsInteger;
  end;

  //Fechamento de Caixa
  QryCx.Close;
  QryCx.SQL.Clear;
  QryCx.SQL.Text := 'SELECT F.VR_TOTAL_FECHAMENTO AS VR_TOTAL, ' +
                    '       F.DATA_FECHAMENTO AS DATA, ' +
                    '       F.HORA_FECHAMENTO AS HORA, ' +
                    '       F.VR_NOTAS, ' +
                    '       F.VR_MOEDAS, ' +
                    '       F.VR_CONTACORRENTE, ' +
                    '       (SELECT M.ID_MOVIMENTO ' +
                    '          FROM MOVIMENTO M ' +
                    '         WHERE M.ID_ORIGEM = F.ID_CAIXA_FECHAMENTO ' +
                    '           AND M.TIPO_ORIGEM = ' + QuotedStr('F') + ') ID_ULT_MOV ' +
                    '  FROM CAIXA_FECHAMENTO F ' +
                    ' WHERE F.FLG_CANCELADO = ' + QuotedStr('N') +
                    '   AND F.DATA_FECHAMENTO = (SELECT MAX(CF.DATA_FECHAMENTO) ' +
                    '                              FROM CAIXA_FECHAMENTO CF ' +
                    '                             WHERE CF.FLG_CANCELADO = ' + QuotedStr('N') + ')';
  QryCx.Open;

  if QryCx.RecordCount > 0 then
  begin
    vgDataUltFech   := QryCx.FieldByName('DATA').AsDateTime;
    vgHoraUltFech   := StrToTime(QryCx.FieldByName('HORA').AsString);
    vgVlrNotasFech  := QryCx.FieldByName('VR_NOTAS').AsFloat;
    vgVlrMoedasFech := QryCx.FieldByName('VR_MOEDAS').AsFloat;
    vgVlrContaFech  := QryCx.FieldByName('VR_CONTACORRENTE').AsFloat;
    vgVlrTotalFech  := QryCx.FieldByName('VR_TOTAL').AsFloat;
    vgIdMovAb       := QryCx.FieldByName('ID_ULT_MOV').AsInteger;
  end;

  case AnsiIndexStr(UpperCase(vgSituacaoCaixa), ['N', 'A', 'F']) of
    0:
    begin
      if vgConf_DataIniImportacao = 0 then
      begin
        vgDataProxAb      := Date;  //Data prevista
        vgDataProxFech    := Date;  //Data prevista
        vgDataLancVigente := Date;  //Data prevista
      end
      else
      begin
        vgDataProxAb      := vgConf_DataIniImportacao;  //Data prevista
        vgDataProxFech    := vgConf_DataIniImportacao;  //Data prevista
        vgDataLancVigente := vgConf_DataIniImportacao;  //Data prevista
      end;
    end;
    1:
    begin
      vgDataProxAb      := dmGerencial.ProximoDiaUtil(vgDataUltAb);  //Data prevista
      vgDataProxFech    := vgDataUltAb;
      vgDataLancVigente := vgDataUltAb;  //Data prevista
    end;
    2:
    begin
      vgDataProxAb      := dmGerencial.ProximoDiaUtil(vgDataUltAb);  //Data prevista
      vgDataProxFech    := dmGerencial.ProximoDiaUtil(vgDataUltFech);  //Data prevista
      vgDataLancVigente := vgDataProxFech;  //Data prevista
    end;
  end;

  FreeAndNil(QryCx);
end;

{ TDadosClasseLancamento }

constructor TDadosClasseLancamento.Create;
begin
  CodLancamento     := 0;
  AnoLancamento     := 0;
  DataLancamento    := 0;
  TipoLancamento    := '';
  TipoCadastro      := '';
  IdCategoria       := 0;
  DescrCategoria    := '';
  IdSubCategoria    := 0;
  DescrSubCategoria := '';
  IdNatureza        := 0;
  DescrNatureza     := '';
  IdCliFor          := 0;
  NomeCliFor        := '';
  FlgIR             := '';
  FlgPessoal        := '';
  FlgAuxiliar       := '';
  FlgForaFechCaixa  := '';
  FlgReal           := 'S';
  FlgFlutuante      := '';
  QtdParcelas       := 0;
  VlrTotalPrev      := 0;
  VlrTotalJuros     := 0;
  VlrTotalDesconto  := 0;
  VlrTotalPago      := 0;
  CadIdUsuario      := 0;
  DataCadastro      := 0;
  FlgStatus         := 'P';
  FlgCancelado      := 'N';
  DataCancelamento  := 0;
  CancelIdUsuario   := 0;
  FlgRecorrente     := '';
  FlgReplicado      := '';
  NumRecibo         := -1;
  IdOrigem          := 0;
  IdSistema         := 0;
  NomeSistema       := '';
  Observacao        := '';
  IdEquivalencia    := 0;
end;

destructor TDadosClasseLancamento.Destroy;
begin

  inherited;
end;

{ TDadosClasseLancamentoDet }

constructor TDadosClasseLancamentoDet.Create;
begin
  IdLancamentoDet    := 0;
  CodLancamento      := 0;
  AnoLancamento      := 0;
  IdItem             := 0;
  DescrItem          := '';
  VlrUnitario        := 0;
  Quantidade         := 0;
  VlrTotal           := 0;
  SeloOrigem         := '';
  AleatorioOrigem    := '';
  TipoCobranca       := '';
  CodAdicional       := 0;
  Emolumentos        := 0;
  FETJ               := 0;
  FUNDPERJ           := 0;
  FUNPERJ            := 0;
  FUNARPEN           := 0;
  PMCMV              := 0;
  ISS                := 0;
  Mutua              := 0;
  Acoterj            := 0;
  NumProtocolo       := 0;
  FlgCancelado       := 'N';
end;

destructor TDadosClasseLancamentoDet.Destroy;
begin

  inherited;
end;

{ TDadosClasseLancamentoParc }

constructor TDadosClasseLancamentoParc.Create;
begin
  IdLancamentoParc := 0;
  CodLancamento    := 0;
  AnoLancamento    := 0;
  DataLancParc     := 0;
  NumParcela       := 0;
  DataVencimento   := 0;
  VlrPrevisto      := 0;
  VlrJuros         := 0;
  VlrDesconto      := 0;
  VlrPago          := 0;
  IdFormaPagtoOrig := 0;
  IdFormaPagto     := 0;
  Agencia          := '';
  Conta            := '';
  IdBanco          := 0;
  NumCheque        := '';
  NumCodDeposito   := '';
  FlgStatus        := 'P';
  CadIdUsuario     := 0;
  DataCadastro     := 0;
  CancelIdUsuario  := 0;
  DataCancelamento := 0;
  PagIdUsuario     := 0;
  DataPagamento    := 0;
  Observacao       := '';
end;

destructor TDadosClasseLancamentoParc.Destroy;
begin

  inherited;
end;

end.
