{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UBibliotecaSistema.pas
  Descricao:   Biblioteca de Procedures e Funcoes comuns no Sistema
  Author   :   Cristina
  Date:        12-out-2016
  Last Update: 04-jan-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UBibliotecaSistema;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
     System.Classes, Vcl.Forms, Vcl.Dialogs, FireDAC.Comp.Client, System.DateUtils,
     Vcl.StdCtrls, Vcl.Controls, System.StrUtils;

type
  TBibliotecaSistema = class(TForm)

    procedure GravarUsuarioLog(Modulo: Integer = 0; Selo: String = '';
                               Observacao: String = ''; Id: Integer = 0;
                               Tabela: String = '');
    procedure PreencherVariaveisSistema;

    function ProximoId(Campo, Tabela: String): Integer;
end;

var
  BS: TBibliotecaSistema;

implementation

{ TBibliotecaSistema }

uses UGDM, UDM, UVariaveisGlobais;

procedure TBibliotecaSistema.GravarUsuarioLog(Modulo: Integer = 0; Selo: String = '';
  Observacao: String = ''; Id: Integer = 0; Tabela: String = '');
var
  QryLog: TFDQuery;
begin
  { MODULO
    1  - Atalho / 2  - Cadastros / 3  - Caixa /
    50  - Relatórios / 80 - Configurações / 90  - Suporte / 100 - Login }

  QryLog := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryLog, SQL do
  begin
    Close;
    Clear;
    Text := 'INSERT INTO USUARIO_LOGSYNC (ID_USUARIO_LOGSYNC, ID_USUARIO, ID_MODULO_FK, ' +
            '                             TIPO_LANCAMENTO, CODIGO_LAN, ANO_LAN, ID_ORIGEM, ' +
            '                             TABELA_ORIGEM, TIPO_ACAO, OBS_USUARIO_LOGSYNC) ' +
            '                     VALUES (:ID_USUARIO_LOGSYNC, :ID_USUARIO, :ID_MODULO, ' +
            '                             :TIPO_LANCAMENTO, :CODIGO_LAN, :ANO_LAN, :ID_ORIGEM, ' +
            '                             :TABELA_ORIGEM, :TIPO_ACAO, :OBS_USUARIO_LOGSYNC)';

    Params.ParamByName('ID_USUARIO_LOGSYNC').Value := ProximoId('ID_USUARIO_LOGSYNC', 'USUARIO_LOGSYNC');
    Params.ParamByName('ID_USUARIO').Value         := vgUsu_Id;

    if Modulo = 0 then
      Params.ParamByName('ID_MODULO').Value := Null
    else
      Params.ParamByName('ID_MODULO').Value := Modulo;

    if Trim(vgLanc_TpLanc) = '' then
      Params.ParamByName('TIPO_LANCAMENTO').Value := Null
    else
      Params.ParamByName('TIPO_LANCAMENTO').Value := vgLanc_TpLanc;

    if vgLanc_Codigo = 0 then
      Params.ParamByName('CODIGO_LAN').Value := Null
    else
      Params.ParamByName('CODIGO_LAN').Value := vgLanc_Codigo;

    if vgLanc_Ano = 0 then
      Params.ParamByName('ANO_LAN').Value := Null
    else
      Params.ParamByName('ANO_LAN').Value := vgLanc_Ano;

    if Id = 0 then
      Params.ParamByName('ID_ORIGEM').Value := Null
    else
      Params.ParamByName('ID_ORIGEM').Value := Id;

    if Trim(Tabela) = '' then
      Params.ParamByName('TABELA_ORIGEM').Value := Null
    else
      Params.ParamByName('TABELA_ORIGEM').Value := Trim(Tabela);

    if vgOperacao = VAZIO then
      Params.ParamByName('TIPO_ACAO').Value := Null
    else
      Params.ParamByName('TIPO_ACAO').Value := vgOperacao;

    if Trim(Observacao) = '' then
      Params.ParamByName('OBS_USUARIO_LOGSYNC').Value := Null
    else
      Params.ParamByName('OBS_USUARIO_LOGSYNC').Value := Trim(Observacao);

    try
      ExecSQL;
    except
      GravarUsuarioLog(Modulo, Selo, Observacao, Id, Tabela);
    end;
  end;

  FreeAndNil(QryLog);
end;

procedure TBibliotecaSistema.PreencherVariaveisSistema;
var
  QryVars: TFDQuery;
  sQueryPerm, sModulos: String;
begin
  QryVars := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with QryVars, SQL do
  begin
    { Preencher as variaveis do Sistema }
    //CONFIGURACOES
    Close;
    Clear;
    Text := 'SELECT * FROM CONFIGURACAO';
    Open;

    vgConf_Atribuicao           := FieldByName('ATRIBUICAO').AsInteger;
    vgConf_Ambiente             := FieldByName('AMBIENTE').AsString;
    vgConf_CodServentia         := FieldByName('COD_SERVENTIA').AsString;
    vgConf_DiretorioDatabase    := FieldByName('DIR_DATABASE').AsString;
    vgConf_DiretorioImagens     := FieldByName('DIR_ARQ_IMAGENS').AsString;
    vgConf_DiretorioRelatorios  := FieldByName('DIR_ARQ_RELATORIOS').AsString;
    vgConf_FlgExibeAtualizacao  := FieldByName('FLG_EXB_ATUALIZACAO').AsString;
    vgConf_FlgTrabalhaComissao  := FieldByName('FLG_COMISSAO').AsString;
    vgConf_FlgExpedienteSabado  := FieldByName('FLG_EXPEDIENTESABADO').AsString;
    vgConf_FlgExpedienteDomingo := FieldByName('FLG_EXPEDIENTEDOMINGO').AsString;
    vgConf_VersaoSync           := FieldByName('VERSAO_SYNC').AsString;
    vgConf_IntervaloSync        := FieldByName('INTERVALO_SYNC').AsInteger;

    if FieldByName('DATA_INI_IMPORTACAO').IsNull then
    begin
      vgConf_DataIniImportacao := 0;
      vgImport_Prim := True;
    end
    else
    begin
      vgConf_DataIniImportacao := FieldByName('DATA_INI_IMPORTACAO').AsDateTime;
      vgImport_Prim := False;
    end;

    //SERVENTIA
    Close;
    Clear;
    Text := 'SELECT * ' +
            '  FROM SERVENTIA ' +
            ' WHERE COD_SERVENTIA = :CodServentia';
    Params.ParamByName('CodServentia').Value := vgConf_CodServentia;
    Open;

    vgSrvn_IdServentia       := FieldByName('ID_SERVENTIA').AsInteger;
    vgSrvn_CodServentia      := FieldByName('COD_SERVENTIA').AsString;
    vgSrvn_NomeServentia     := FieldByName('NOME_SERVENTIA').AsString;
    vgSrvn_EndServentia      := FieldByName('ENDERECO').AsString;
    vgSrvn_CidadeServentia   := FieldByName('CIDADE').AsString;
    vgSrvn_EstadoServentia   := FieldByName('UF').AsString;
    vgSrvn_CEPServentia      := FieldByName('CEP').AsString;
    vgSrvn_TelServentia      := FieldByName('TELEFONE').AsString;
    vgSrvn_EmailServentia    := FieldByName('EMAIL').AsString;
    vgSrvn_NomeTabeliao      := FieldByName('NOME_TABELIAO').AsString;
    vgSrvn_MatriculaTabeliao := FieldByName('MATRICULA_TABELIAO').AsString;
    vgSrvn_NomeTabeliaoSubst := FieldByName('NOME_SUBSTITUTO').AsString;
    vgSrvn_CNPJServentia     := FieldByName('CNPJ').AsString;
    vgSrvn_CodMunicipioServ  := FieldByName('COD_MUN').AsInteger;
    vgSrvn_TituloServentia   := FieldByName('TITULO').AsString;
  end;

  dmPrincipal.VerificarSituacaoCaixa;

  FreeAndNil(QryVars);
end;

function TBibliotecaSistema.ProximoId(Campo, Tabela: String): Integer;
var
  Qry: TFDQuery;
  Id: Integer;
begin
  if (Tabela = 'MUNICIPIO') or
    (Tabela = 'ESTADO') or
    (Tabela = 'PAIS') or
    (Tabela = 'FUNCIONARIO') or
    (Tabela = 'FUNCIONARIO_COMISSAO') or
    (Tabela = 'OCUPACAO') or
    (Tabela = 'SERV') or
    (Tabela = 'CARGO') or
    (Tabela = 'ESTADOCIVIL') or
    (Tabela = 'ESCOLARIDADE') or
    (Tabela = 'TIPOFILIACAO') or
    (Tabela = 'TIPODOCUMENTO') or
    (Tabela = 'JUSTIFICATIVA_AUSENC_CPF') or
    (Tabela = 'BANCO') or
    (Tabela = 'SELO_CCT') or
    (Tabela = 'SERIE') or
    (Tabela = 'COMPRA') or
    (Tabela = 'USUARIO') or
    (Tabela = 'PESSOA') or
    (Tabela = 'PESSOADOCUMENTO') or
    (Tabela = 'PESSOAFILIACAO') or
    (Tabela = 'PESSOAGEMEO') or
    (Tabela = 'HISTORICO_PESSOA')or
    (Tabela = 'SISTEMA') or
    (Tabela = 'TIPO_ATO') then
    Qry := dmGerencial.CriarFDQuery(nil, vgConGER)
  else
    Qry := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  with Qry, SQL do
  begin
    Id := 0;

    Close;
    Clear;
    Text := 'SELECT MAX(' + Campo + ') AS ID ' +
            '  FROM ' + Tabela;
    Open;

    if Qry.FieldByName('ID').IsNull then
      Id := 1
    else
      Id := Qry.FieldByName('ID').AsInteger + 1;
  end;

  Result := Id;
end;

end.
