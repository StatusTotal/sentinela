unit URecibo;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Phys.FBDef, FireDAC.UI.Intf,
  FireDAC.VCLUI.Wait, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, Data.DB,
  FireDAC.Comp.Client, FireDAC.Comp.UI, FireDAC.Phys.IBBase, FireDAC.Stan.Param,
  FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet,
  Vcl.Dialogs, JvBaseDlg, JvProgressDialog, Datasnap.DBClient, Datasnap.Provider,
  System.DateUtils, System.Variants, System.StrUtils, Vcl.Forms, Winapi.Windows,
  Winapi.Messages;

type
  TReciboThread = class(TThread)
  protected
    procedure Execute; override;
  private
  public
    constructor Create;
    destructor Destroy;
  end;

implementation

{ TReciboThread }

uses UBibliotecaSistema, UDM, UGDM, UVariaveisGlobais;

constructor TReciboThread.Create;
begin
  inherited Create(True);
end;

destructor TReciboThread.Destroy;
begin

end;

procedure TReciboThread.Execute;
var
  Msg, sQtd, sCPF_CNPJ, sTpPessoa, sTextoUp: String;
  Op: TOperacao;
  iId: Integer;
  QryCli01, QryCli02: TFDQuery;
begin
  { Essa procedure compara algumas tabelas do banco de dados do RECIBO e do
    MONITORAMENTO e atualiza as tabelas no banco de dados do MONITORAMENTO. }
//  QryCli01 := dmGerencial.CriarFDQuery(nil, vgConRECIBO);
//  QryCli02 := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

  QryCli01 := TFDQuery.Create(nil);
  QryCli01.Connection := vgConRECIBO;

  Op := vgOperacao;

  Msg       := '';
  sQtd      := '';
  sCPF_CNPJ := '';
  sTpPessoa := '';
  sTextoUp  := '';

  iId := 0;

  { CLIENTE }
  QryCli01.Close;
  QryCli01.SQL.Clear;
  QryCli01.SQL.Text := 'SELECT ID_CLIENTE, NOME, DOCUMENTO ' +
                       '  FROM CLIENTES ' +
                       'ORDER BY ID_CLIENTE';
  QryCli01.Open;
  QryCli01.Last;

  sQtd := IntToStr(QryCli01.RecordCount);
  QryCli01.First;

  QryCli01.DisableControls;

  { Verifica se o registro existe no MONITORAMENTO e inclui, caso nao exista }
  while not QryCli01.Eof do
  begin
    if QryCli01.RecNo = 1 then
      vgTextoMemo := 'Sincronizando tabelas de clientes do RECIBO... ' +
                     IntToStr(QryCli01.RecNo) + '/' + sQtd
    else
      vgTextoMemo := vgTextoMemo + #13#10 +
                     'Sincronizando tabelas de clientes do RECIBO... ' +
                     IntToStr(QryCli01.RecNo) + '/' + sQtd;

    QryCli02 := TFDQuery.Create(nil);
    QryCli02.Connection := dmPrincipal.conSISTEMA;

    QryCli02.Close;
    QryCli02.SQL.Clear;
    QryCli02.SQL.Text := 'SELECT ID_CLIENTE_FORNECEDOR, ' +
                         '       NOME_FANTASIA,  ' +
                         '       RAZAO_SOCIAL,  ' +
                         '       CPF_CNPJ, ' +
                         '       DOCUMENTO ' +
                         '  FROM CLIENTE_FORNECEDOR ' +
                         ' WHERE DOCUMENTO = ' + QuotedStr(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString));
    QryCli02.Open;

    QryCli02.DisableControls;

    if QryCli02.RecordCount = 0 then  //INCLUIR
    begin
      if Length(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString)) = 11 then
      begin
        sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 1, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 4, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 7, 3) + '-' +
                     Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 10, 2);

        sTpPessoa := 'F';
      end
      else if Length(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString)) = 14 then
      begin
        sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 1, 2) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 3, 3) + '.' +
                     Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 6, 3) + '/' +
                     Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 9, 4) + '-' +
                     Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 13, 2);

        sTpPessoa := 'J';
      end
      else
        sCPF_CNPJ := '';

      iId := QryCli01.FieldByName('ID_CLIENTE').AsInteger;

      try
        dmGerencial.DMLInsertFD('CLIENTE_FORNECEDOR',
                                'ID_CLIENTE_FORNECEDOR, ' +
                                'TIPO_CLIENTE_FORNECEDOR, NOME_FANTASIA, ' +
                                'RAZAO_SOCIAL, DOCUMENTO, ' +
                                'DATA_CADASTRO, CAD_ID_USUARIO, ' +
                                'FLG_ATIVO, TIPO_PESSOA, CPF_CNPJ',
                                IntToStr(BS.ProximoId('ID_CLIENTE_FORNECEDOR', 'CLIENTE_FORNECEDOR')) + ', ' +
                                QuotedStr('C') + ', ' + QuotedStr(QryCli01.FieldByName('NOME').AsString) + ', ' +
                                QuotedStr(QryCli01.FieldByName('NOME').AsString) + ', ' + QuotedStr(QryCli01.FieldByName('DOCUMENTO').AsString) +  ', ' +
                                QuotedStr(FormatDateTime('YYYY-MM-DD', Date)) + ', ' + IntToStr(vgUsu_Id) + ', ' +
                                QuotedStr('S') + ', ' + QuotedStr(sTpPessoa) + ', ' + QuotedStr(sCPF_CNPJ),
                                vgConSISTEMA);

        Op := vgOperacao;
        vgOperacao := I;
        BS.GravarUsuarioLog(80, '', 'Inclus�o AUTOM�TICA de Cliente', iId, 'CLIENTE_FORNECEDOR');
        vgOperacao := Op;
      except
        on E: Exception do
        begin
          GravarLogErro(E.Message);

          Msg := 'Erro ao incluir o ID ' + IntToStr(iId) + ' da tabela CLIENTES do RECIBO ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end
    else  //ALTERAR
    begin
      try
        sTextoUp := '';

        if QryCli01.FieldByName('NOME').AsString <> QryCli02.FieldByName('NOME_FANTASIA').AsString then
          sTextoUp := 'NOME_FANTASIA = ' + QuotedStr(QryCli01.FieldByName('NOME').AsString);

        if QryCli01.FieldByName('NOME').AsString <> QryCli02.FieldByName('RAZAO_SOCIAL').AsString then
        begin
          if Trim(sTextoUp) = '' then
            sTextoUp := 'RAZAO_SOCIAL = ' + QuotedStr(QryCli01.FieldByName('NOME').AsString)
          else
            sTextoUp := sTextoUp + ', RAZAO_SOCIAL = ' + QuotedStr(QryCli01.FieldByName('NOME').AsString);
        end;

        if QryCli01.FieldByName('DOCUMENTO').AsString <> QryCli02.FieldByName('DOCUMENTO').AsString then
        begin
          if Trim(sTextoUp) = '' then
            sTextoUp := 'DOCUMENTO = ' + QuotedStr(QryCli01.FieldByName('DOCUMENTO').AsString)
          else
            sTextoUp := sTextoUp + ', DOCUMENTO = ' + QuotedStr(QryCli01.FieldByName('DOCUMENTO').AsString);
        end;

        if dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString) <> dmGerencial.PegarNumeroTexto(QryCli02.FieldByName('CPF_CNPJ').AsString) then
        begin
          if Length(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString)) = 11 then
          begin
            sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 1, 3) + '.' +
                         Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 4, 3) + '.' +
                         Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 7, 3) + '-' +
                         Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 10, 2);
          end
          else if Length(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString)) = 14 then
          begin
            sCPF_CNPJ := Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 1, 2) + '.' +
                         Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 3, 3) + '.' +
                         Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 6, 3) + '/' +
                         Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 9, 4) + '-' +
                         Copy(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString), 13, 2);
          end
          else
            sCPF_CNPJ := '';

          if Trim(sTextoUp) = '' then
            sTextoUp := 'CPF_CNPJ = ' + QuotedStr(sCPF_CNPJ)
          else
            sTextoUp := sTextoUp + ', CPF_CNPJ = ' + QuotedStr(sCPF_CNPJ);
        end;

        if Trim(sTextoUp) <> '' then
        begin
          iId := QryCli02.FieldByName('ID_CLIENTE_FORNECEDOR').AsInteger;

          dmGerencial.DMLUpdateFD('CLIENTE_FORNECEDOR',
                                  sTextoUp,
                                  vgConSISTEMA,
                                  'DOCUMENTO = ' + QuotedStr(dmGerencial.PegarNumeroTexto(QryCli01.FieldByName('DOCUMENTO').AsString)));

          Op := vgOperacao;
          vgOperacao := E;
          BS.GravarUsuarioLog(80, '', 'Edi��o AUTOM�TICA de Cliente', iId, 'CLIENTE_FORNECEDOR');
          vgOperacao := Op;
        end;
      except
        on E: Exception do
        begin
          GravarLogErro(E.Message);

          Msg := 'Erro ao alterar os dados do registro de ID ' + IntToStr(iId) + ' da tabela CLIENTES do RECIBO ' +
                 'para o MONITORAMENTO. ' + #13#10 +
                 'Por favor, entre em contato com o Suporte. ' + #13#10 +
                 E.Message;

          Application.MessageBox(PChar(Msg), 'Erro', MB_OK + MB_ICONERROR);
          Application.ProcessMessages;
        end;
      end;
    end;

    QryCli02.EnableControls;
    FreeAndNil(QryCli02);

    QryCli01.Next;
  end;

  QryCli01.EnableControls;

  vgOperacao := Op;

  FreeAndNil(QryCli01);
//  FreeAndNil(QryCli02);
end;

end.
