object dmPrincipal: TdmPrincipal
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 412
  Width = 898
  object conSISTEMA: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Database=C:\Total\MONITORAMENTO\MONITORAMENTO.FDB'
      'Server=localhost'
      'Protocol=TCPIP'
      'DriverID=FB')
    LoginPrompt = False
    Left = 48
    Top = 24
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    VendorLib = 'gds32.dll'
    Left = 48
    Top = 72
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 48
    Top = 120
  end
  object FDPhysFBDriverLink2: TFDPhysFBDriverLink
    VendorLib = 'gds32.dll'
    Left = 304
    Top = 72
  end
  object FDGUIxWaitCursor2: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 304
    Top = 120
  end
  object conFIRMAS: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Database=C:\Total\Firmas\TOTAL_ASSINA.FDB'
      'Server=localhost'
      'DriverID=FB')
    LoginPrompt = False
    Left = 304
    Top = 24
  end
  object FDPhysFBDriverLink3: TFDPhysFBDriverLink
    VendorLib = 'gds32.dll'
    Left = 416
    Top = 72
  end
  object FDGUIxWaitCursor3: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 416
    Top = 120
  end
  object conFIRMAS_ANTIGO: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Database=C:\Total\Total_Assinaturas\TOTAL_ASSINA.FDB'
      'Server=localhost'
      'DriverID=FB')
    LoginPrompt = False
    Left = 416
    Top = 24
  end
  object FDPhysFBDriverLink4: TFDPhysFBDriverLink
    VendorLib = 'gds32.dll'
    Left = 184
    Top = 72
  end
  object FDGUIxWaitCursor4: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 184
    Top = 120
  end
  object conRECIBO: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Database=C:\Total\BANCO\RECIBO\BDRECIBO.FDB'
      'Server=localhost'
      'DriverID=FB')
    LoginPrompt = False
    Left = 184
    Top = 24
  end
  object qryFunc1: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO'
      'ORDER BY ID_FUNCIONARIO')
    Left = 280
    Top = 192
  end
  object qryFunc2: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO'
      'ORDER BY ID_FUNCIONARIO')
    Left = 344
    Top = 192
  end
  object dspFunc2: TDataSetProvider
    DataSet = qryFunc2
    Left = 344
    Top = 240
  end
  object cdsFunc2: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFunc2'
    Left = 344
    Top = 288
    object cdsFunc2ID_FUNCIONARIO: TIntegerField
      FieldName = 'ID_FUNCIONARIO'
      Required = True
    end
    object cdsFunc2NOME_FUNCIONARIO: TStringField
      FieldName = 'NOME_FUNCIONARIO'
      Size = 250
    end
    object cdsFunc2DATA_NASCIMENTO: TDateField
      FieldName = 'DATA_NASCIMENTO'
    end
    object cdsFunc2NUM_IDENTIDADE: TStringField
      FieldName = 'NUM_IDENTIDADE'
      Size = 25
    end
    object cdsFunc2ORGAO_EMISSOR: TStringField
      FieldName = 'ORGAO_EMISSOR'
      Size = 70
    end
    object cdsFunc2DATA_EMISSAO_IDENTIDADE: TDateField
      FieldName = 'DATA_EMISSAO_IDENTIDADE'
    end
    object cdsFunc2CPF: TStringField
      FieldName = 'CPF'
      Size = 14
    end
    object cdsFunc2NUM_CTPS: TStringField
      FieldName = 'NUM_CTPS'
      Size = 7
    end
    object cdsFunc2SERIE_CTPS: TStringField
      FieldName = 'SERIE_CTPS'
      Size = 5
    end
    object cdsFunc2DATA_CTPS: TDateField
      FieldName = 'DATA_CTPS'
    end
    object cdsFunc2NUM_PIS: TStringField
      FieldName = 'NUM_PIS'
      Size = 14
    end
    object cdsFunc2SIGLA_ESTADO_CTPS: TStringField
      FieldName = 'SIGLA_ESTADO_CTPS'
      FixedChar = True
      Size = 2
    end
    object cdsFunc2NUM_TITULO_ELEITOR: TStringField
      FieldName = 'NUM_TITULO_ELEITOR'
      Size = 14
    end
    object cdsFunc2ZONA_TE: TStringField
      FieldName = 'ZONA_TE'
      Size = 3
    end
    object cdsFunc2SECAO_TE: TStringField
      FieldName = 'SECAO_TE'
      Size = 4
    end
    object cdsFunc2COD_MUNICIPIO_TE: TStringField
      FieldName = 'COD_MUNICIPIO_TE'
      Size = 7
    end
    object cdsFunc2SIGLA_ESTADO_TE: TStringField
      FieldName = 'SIGLA_ESTADO_TE'
      FixedChar = True
      Size = 2
    end
    object cdsFunc2DATA_EMISSAO_TE: TDateField
      FieldName = 'DATA_EMISSAO_TE'
    end
    object cdsFunc2TIPO_SANGUINEO: TStringField
      FieldName = 'TIPO_SANGUINEO'
      Size = 2
    end
    object cdsFunc2FATOR_RH: TStringField
      FieldName = 'FATOR_RH'
      Size = 8
    end
    object cdsFunc2LOGRADOURO: TStringField
      FieldName = 'LOGRADOURO'
      Size = 250
    end
    object cdsFunc2NUM_LOGRADOURO: TStringField
      FieldName = 'NUM_LOGRADOURO'
      Size = 25
    end
    object cdsFunc2COMPLEMENTO_LOGRADOURO: TStringField
      FieldName = 'COMPLEMENTO_LOGRADOURO'
      Size = 100
    end
    object cdsFunc2BAIRRO_LOGRADOURO: TStringField
      FieldName = 'BAIRRO_LOGRADOURO'
      Size = 250
    end
    object cdsFunc2CEP_LOGR: TStringField
      FieldName = 'CEP_LOGR'
      Size = 9
    end
    object cdsFunc2COD_MUNICIPIO_LOGR: TStringField
      FieldName = 'COD_MUNICIPIO_LOGR'
      Size = 7
    end
    object cdsFunc2SIGLA_ESTADO_LOGR: TStringField
      FieldName = 'SIGLA_ESTADO_LOGR'
      FixedChar = True
      Size = 2
    end
    object cdsFunc2TELEFONE: TStringField
      FieldName = 'TELEFONE'
      Size = 13
    end
    object cdsFunc2CELULAR: TStringField
      FieldName = 'CELULAR'
      Size = 14
    end
    object cdsFunc2EMAIL: TStringField
      FieldName = 'EMAIL'
      Size = 250
    end
    object cdsFunc2ID_CARGO_FK: TIntegerField
      FieldName = 'ID_CARGO_FK'
    end
    object cdsFunc2COD_ESCOLARIDADE: TIntegerField
      FieldName = 'COD_ESCOLARIDADE'
    end
    object cdsFunc2COD_ESTADOCIVIL: TIntegerField
      FieldName = 'COD_ESTADOCIVIL'
    end
    object cdsFunc2QTD_FILHOS: TIntegerField
      FieldName = 'QTD_FILHOS'
    end
    object cdsFunc2QTD_DEPENDENTES: TIntegerField
      FieldName = 'QTD_DEPENDENTES'
    end
    object cdsFunc2HORA_ENT_TRAB: TTimeField
      FieldName = 'HORA_ENT_TRAB'
    end
    object cdsFunc2HORA_SAI_ALMOCO: TTimeField
      FieldName = 'HORA_SAI_ALMOCO'
    end
    object cdsFunc2HORA_ENT_ALMOCO: TTimeField
      FieldName = 'HORA_ENT_ALMOCO'
    end
    object cdsFunc2HORA_SAI_TRAB: TTimeField
      FieldName = 'HORA_SAI_TRAB'
    end
    object cdsFunc2VR_SALARIO: TBCDField
      FieldName = 'VR_SALARIO'
      Precision = 18
      Size = 2
    end
    object cdsFunc2VR_MAXIMO_VALE: TBCDField
      FieldName = 'VR_MAXIMO_VALE'
      Precision = 18
      Size = 2
    end
    object cdsFunc2SEXO: TStringField
      FieldName = 'SEXO'
      FixedChar = True
      Size = 1
    end
    object cdsFunc2DATA_ADMISSAO: TDateField
      FieldName = 'DATA_ADMISSAO'
    end
    object cdsFunc2DATA_CADASTRO: TDateField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsFunc2DATA_DESLIGAMENTO: TDateField
      FieldName = 'DATA_DESLIGAMENTO'
    end
    object cdsFunc2FLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
  end
  object dsFunc1: TDataSource
    DataSet = qryFunc1
    Left = 280
    Top = 240
  end
  object dsFunc2: TDataSource
    DataSet = cdsFunc2
    Left = 344
    Top = 336
  end
  object qryCar1: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM CARGO'
      'ORDER BY ID_CARGO')
    Left = 120
    Top = 192
  end
  object qryCar2: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CARGO'
      'ORDER BY ID_CARGO')
    Left = 176
    Top = 192
  end
  object qryFuncCom1: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO_COMISSAO'
      'ORDER BY ID_FUNCIONARIO_COMISSAO')
    Left = 456
    Top = 192
  end
  object qryFuncCom2: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM FUNCIONARIO_COMISSAO'
      'ORDER BY ID_FUNCIONARIO_COMISSAO')
    Left = 536
    Top = 192
  end
  object dspCar2: TDataSetProvider
    DataSet = qryCar2
    Left = 176
    Top = 240
  end
  object cdsCar2: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCar2'
    Left = 176
    Top = 288
    object cdsCar2ID_CARGO: TIntegerField
      FieldName = 'ID_CARGO'
      Required = True
    end
    object cdsCar2DESCR_CARGO: TStringField
      FieldName = 'DESCR_CARGO'
      Size = 250
    end
    object cdsCar2COD_CARGO: TIntegerField
      FieldName = 'COD_CARGO'
    end
  end
  object dsCar2: TDataSource
    DataSet = cdsCar2
    Left = 176
    Top = 336
  end
  object dspFuncCom2: TDataSetProvider
    DataSet = qryFuncCom2
    Left = 536
    Top = 240
  end
  object cdsFuncCom2: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspFuncCom2'
    Left = 536
    Top = 288
    object cdsFuncCom2ID_FUNCIONARIO_COMISSAO: TIntegerField
      FieldName = 'ID_FUNCIONARIO_COMISSAO'
      Required = True
    end
    object cdsFuncCom2ID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsFuncCom2PERCENT_COMISSAO: TBCDField
      FieldName = 'PERCENT_COMISSAO'
      Precision = 18
      Size = 2
    end
    object cdsFuncCom2ID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsFuncCom2COD_ADICIONAL: TIntegerField
      FieldName = 'COD_ADICIONAL'
    end
    object cdsFuncCom2ID_ITEM_FK: TIntegerField
      FieldName = 'ID_ITEM_FK'
    end
  end
  object dsFuncCom2: TDataSource
    DataSet = cdsFuncCom2
    Left = 536
    Top = 336
  end
  object dsCar1: TDataSource
    DataSet = qryCar1
    Left = 120
    Top = 240
  end
  object dsFuncCom1: TDataSource
    DataSet = qryFuncCom1
    Left = 456
    Top = 240
  end
  object qryUsu1: TFDQuery
    Connection = dmGerencial.conGERENCIAL
    SQL.Strings = (
      'SELECT *'
      '  FROM USUARIO'
      'ORDER BY ID_USUARIO')
    Left = 664
    Top = 192
  end
  object qryUsu2: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM USUARIO'
      'ORDER BY ID_USUARIO')
    Left = 712
    Top = 192
  end
  object dspUsu2: TDataSetProvider
    DataSet = qryUsu2
    Left = 712
    Top = 240
  end
  object cdsUsu2: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspUsu2'
    Left = 712
    Top = 288
    object cdsUsu2ID_USUARIO: TIntegerField
      FieldName = 'ID_USUARIO'
      Required = True
    end
    object cdsUsu2NOME: TStringField
      FieldName = 'NOME'
      Size = 30
    end
    object cdsUsu2CPF: TStringField
      FieldName = 'CPF'
      Required = True
      Size = 14
    end
    object cdsUsu2SENHA: TStringField
      FieldName = 'SENHA'
      Required = True
      Size = 300
    end
    object cdsUsu2QUALIFICACAO: TStringField
      FieldName = 'QUALIFICACAO'
      Size = 100
    end
    object cdsUsu2FLG_MASTER: TStringField
      FieldName = 'FLG_MASTER'
      FixedChar = True
      Size = 1
    end
    object cdsUsu2MATRICULA: TStringField
      FieldName = 'MATRICULA'
      Size = 15
    end
    object cdsUsu2FLG_SUPORTE: TStringField
      FieldName = 'FLG_SUPORTE'
      FixedChar = True
      Size = 1
    end
    object cdsUsu2FLG_VISUALIZOU_ATU: TStringField
      FieldName = 'FLG_VISUALIZOU_ATU'
      FixedChar = True
      Size = 1
    end
    object cdsUsu2FLG_SEXO: TStringField
      FieldName = 'FLG_SEXO'
      FixedChar = True
      Size = 1
    end
    object cdsUsu2ID_FUNCIONARIO_FK: TIntegerField
      FieldName = 'ID_FUNCIONARIO_FK'
    end
    object cdsUsu2FLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsUsu2DATA_CADASTRO: TDateField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsUsu2DATA_INATIVACAO: TDateField
      FieldName = 'DATA_INATIVACAO'
    end
  end
  object dsUsu1: TDataSource
    DataSet = qryUsu1
    Left = 664
    Top = 240
  end
  object dsUsu2: TDataSource
    DataSet = cdsUsu2
    Left = 712
    Top = 336
  end
  object qryConfiguracao: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM CONFIGURACAO')
    Left = 624
    Top = 40
  end
  object dsConfiguracao: TDataSource
    DataSet = qryConfiguracao
    Left = 624
    Top = 88
  end
  object qryPermissao: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM PERMISSAO'
      ' WHERE ID_MODULO_FK IN (:MODULOS)'
      'ORDER BY ID_MODULO_FK,'
      '         ID_PERMISSAO')
    Left = 708
    Top = 40
    ParamData = <
      item
        Name = 'MODULOS'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object dsPermissao: TDataSource
    DataSet = qryPermissao
    Left = 708
    Top = 88
  end
  object dsAuxLanc: TDataSource
    DataSet = cdsAuxLanc
    Left = 32
    Top = 336
  end
  object qryAuxLanc: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT L.*'
      '  FROM LANCAMENTO L'
      ' WHERE L.FLG_RECORRENTE = '#39'S'#39
      '   AND L.FLG_REPLICADO = '#39'N'#39
      '   AND L.FLG_CANCELADO = '#39'N'#39
      '   AND EXTRACT (MONTH FROM L.DATA_CADASTRO) = :MES_CADASTRO  '
      '   AND EXTRACT (YEAR FROM L.DATA_CADASTRO) = :ANO_CADASTRO'
      'ORDER BY COD_LANCAMENTO, ANO_LANCAMENTO')
    Left = 32
    Top = 192
    ParamData = <
      item
        Position = 4
        Name = 'MES_CADASTRO'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 5
        Name = 'ANO_CADASTRO'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object dspAuxLanc: TDataSetProvider
    DataSet = qryAuxLanc
    Left = 32
    Top = 240
  end
  object cdsAuxLanc: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'MES_CADASTRO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ANO_CADASTRO'
        ParamType = ptInput
      end>
    ProviderName = 'dspAuxLanc'
    Left = 32
    Top = 288
    object cdsAuxLancCOD_LANCAMENTO: TIntegerField
      FieldName = 'COD_LANCAMENTO'
      Required = True
    end
    object cdsAuxLancANO_LANCAMENTO: TIntegerField
      FieldName = 'ANO_LANCAMENTO'
      Required = True
    end
    object cdsAuxLancTIPO_LANCAMENTO: TStringField
      FieldName = 'TIPO_LANCAMENTO'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancTIPO_CADASTRO: TStringField
      FieldName = 'TIPO_CADASTRO'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancDATA_LANCAMENTO: TDateField
      FieldName = 'DATA_LANCAMENTO'
    end
    object cdsAuxLancID_CLIENTE_FORNECEDOR_FK: TIntegerField
      FieldName = 'ID_CLIENTE_FORNECEDOR_FK'
    end
    object cdsAuxLancID_CATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_CATEGORIA_DESPREC_FK'
    end
    object cdsAuxLancID_SUBCATEGORIA_DESPREC_FK: TIntegerField
      FieldName = 'ID_SUBCATEGORIA_DESPREC_FK'
    end
    object cdsAuxLancFLG_IMPOSTORENDA: TStringField
      FieldName = 'FLG_IMPOSTORENDA'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancFLG_PESSOAL: TStringField
      FieldName = 'FLG_PESSOAL'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancFLG_AUXILIAR: TStringField
      FieldName = 'FLG_AUXILIAR'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancFLG_REAL: TStringField
      FieldName = 'FLG_REAL'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancFLG_FLUTUANTE: TStringField
      FieldName = 'FLG_FLUTUANTE'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancID_NATUREZA_FK: TIntegerField
      FieldName = 'ID_NATUREZA_FK'
    end
    object cdsAuxLancQTD_PARCELAS: TIntegerField
      FieldName = 'QTD_PARCELAS'
    end
    object cdsAuxLancVR_TOTAL_PREV: TBCDField
      FieldName = 'VR_TOTAL_PREV'
      Precision = 18
      Size = 2
    end
    object cdsAuxLancVR_TOTAL_JUROS: TBCDField
      FieldName = 'VR_TOTAL_JUROS'
      Precision = 18
      Size = 2
    end
    object cdsAuxLancVR_TOTAL_DESCONTO: TBCDField
      FieldName = 'VR_TOTAL_DESCONTO'
      Precision = 18
      Size = 2
    end
    object cdsAuxLancVR_TOTAL_PAGO: TBCDField
      FieldName = 'VR_TOTAL_PAGO'
      Precision = 18
      Size = 2
    end
    object cdsAuxLancDATA_CADASTRO: TSQLTimeStampField
      FieldName = 'DATA_CADASTRO'
    end
    object cdsAuxLancCAD_ID_USUARIO: TIntegerField
      FieldName = 'CAD_ID_USUARIO'
    end
    object cdsAuxLancFLG_STATUS: TStringField
      FieldName = 'FLG_STATUS'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancFLG_CANCELADO: TStringField
      FieldName = 'FLG_CANCELADO'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancDATA_CANCELAMENTO: TDateField
      FieldName = 'DATA_CANCELAMENTO'
    end
    object cdsAuxLancCANCEL_ID_USUARIO: TIntegerField
      FieldName = 'CANCEL_ID_USUARIO'
    end
    object cdsAuxLancFLG_RECORRENTE: TStringField
      FieldName = 'FLG_RECORRENTE'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancFLG_REPLICADO: TStringField
      FieldName = 'FLG_REPLICADO'
      FixedChar = True
      Size = 1
    end
    object cdsAuxLancNUM_RECIBO: TIntegerField
      FieldName = 'NUM_RECIBO'
    end
    object cdsAuxLancOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 1000
    end
    object cdsAuxLancID_ORIGEM: TIntegerField
      FieldName = 'ID_ORIGEM'
    end
    object cdsAuxLancID_SISTEMA_FK: TIntegerField
      FieldName = 'ID_SISTEMA_FK'
    end
    object cdsAuxLancID_CARNELEAO_EQUIVALENCIA_FK: TIntegerField
      FieldName = 'ID_CARNELEAO_EQUIVALENCIA_FK'
    end
    object cdsAuxLancFLG_FORAFECHCAIXA: TStringField
      FieldName = 'FLG_FORAFECHCAIXA'
      FixedChar = True
      Size = 1
    end
  end
  object conFIRMAS_ACENTRAL: TFDConnection
    Params.Strings = (
      'Database=C:\Total\Total_Assinaturas\TOTAL_CENTRAL.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Server=localhost'
      'DriverID=FB')
    LoginPrompt = False
    Left = 528
    Top = 24
  end
  object FDPhysFBDriverLink5: TFDPhysFBDriverLink
    Left = 528
    Top = 72
  end
  object FDGUIxWaitCursor5: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 528
    Top = 120
  end
  object qryRemetente: TFDQuery
    Connection = conSISTEMA
    SQL.Strings = (
      'SELECT *'
      '  FROM EMAIL_REMETENTE')
    Left = 818
    Top = 20
  end
  object dsRemetente: TDataSource
    DataSet = cdsRemetente
    Left = 824
    Top = 168
  end
  object dspRemetente: TDataSetProvider
    DataSet = qryRemetente
    Left = 818
    Top = 68
  end
  object cdsRemetente: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspRemetente'
    Left = 820
    Top = 116
    object cdsRemetenteID_EMAIL_REMETENTE: TIntegerField
      FieldName = 'ID_EMAIL_REMETENTE'
      Required = True
    end
    object cdsRemetenteNOME_REMETENTE: TStringField
      FieldName = 'NOME_REMETENTE'
      Size = 250
    end
    object cdsRemetenteEMAIL_REMETENTE: TStringField
      FieldName = 'EMAIL_REMETENTE'
      Size = 250
    end
    object cdsRemetenteSENHA_REMETENTE: TStringField
      FieldName = 'SENHA_REMETENTE'
      Size = 300
    end
    object cdsRemetenteSERVIDOR_SMTP: TStringField
      FieldName = 'SERVIDOR_SMTP'
      Size = 150
    end
    object cdsRemetentePORTA_SMTP: TIntegerField
      FieldName = 'PORTA_SMTP'
    end
    object cdsRemetenteFLG_SSL_SMTP: TStringField
      FieldName = 'FLG_SSL_SMTP'
      FixedChar = True
      Size = 1
    end
    object cdsRemetenteFLG_ATIVO: TStringField
      FieldName = 'FLG_ATIVO'
      FixedChar = True
      Size = 1
    end
    object cdsRemetenteUSERNAME_REMETENTE: TStringField
      FieldName = 'USERNAME_REMETENTE'
      Size = 250
    end
  end
end
