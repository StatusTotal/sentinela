{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UVariaveisGlobais.pas
  Descricao:   Variaveis globais do Sistema
  Author:      Cristina
  Date:        13-out-2016
  Last Update: 29-ago-2018  (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UVariaveisGlobais;

interface

var
  { O preenchimento dessas variaveis ficam nas seguintes units:
    - UDM (procedure DataModuleCreate)
    - ULogin (procedure btnEntrarClick)
    - UBibliotecaSistema (procedure PreencherVariaveisSistema)

    Sempre que forem efetuadas alteracoes ou acrescimos nessas variaveis,
    as mesmas devem ser atualizadas nessas units }

  { VARIAVEIS DO SISTEMA }
  //USUARIO
  vgUsu_Id               : Integer;
  vgUsu_Nome             : String;
  vgUsu_CPF              : String;
  vgUsu_Qualificacao     : String;
  vgUsu_FlgMaster        : String;
  vgUsu_FlgOficial       : String;
  vgUsu_FlgJustifEdicao  : String;
  vgUsu_Matricula        : String;
  vgUsu_FlgSuporte       : String;
  vgUsu_FlgVisualizouAtu : String;
  vgUsu_FlgSexo          : String;
  vgUsu_IdFuncionario    : Integer;
  vgUsu_FlgAtivo         : String;
  vgUsu_DataCadastro     : TDatetime;
  vgUsu_DataInativacao   : TDatetime;


  //CONFIGURACOES
  vgConf_Atribuicao           : Integer;
  vgConf_Ambiente             : String;
  vgConf_CodServentia         : String;
  vgConf_NomeServentia        : String;
  vgConf_DiretorioDatabase    : String;
  vgConf_DiretorioImagens     : String;
  vgConf_DiretorioRelatorios  : String;
  vgConf_DiretorioCarneLeao   : String;
  vgConf_FlgExibeAtualizacao  : String;
  vgConf_FlgTrabalhaComissao  : String;
  vgConf_FlgBloqueiaFechPagto : String;
  vgConf_FlgExpedienteSabado  : String;
  vgConf_FlgExpedienteDomingo : String;
  vgConf_FlgExpedienteFeriado : String;
  vgConf_FlgZerarNumOficio    : String;
  vgConf_VersaoSync           : String;
  vgConf_IntervaloSync        : Integer;
  vgConf_DataIniImportacao    : TDatetime;
  vgConf_HoraFechamentoPadrao : String;
  vgConf_DiaFechamentoCaixa   : String;


  //SERVENTIA
  vgSrvn_IdServentia       : Integer;
  vgSrvn_CodServentia      : String;
  vgSrvn_NomeServentia     : String;
  vgSrvn_EndServentia      : String;
  vgSrvn_CidadeServentia   : String;
  vgSrvn_EstadoServentia   : String;
  vgSrvn_CEPServentia      : String;
  vgSrvn_TelServentia      : String;
  vgSrvn_EmailServentia    : String;
  vgSrvn_NomeTabeliao      : String;
  vgSrvn_MatriculaTabeliao : String;
  vgSrvn_NomeTabeliaoSubst : String;
  vgSrvn_CNPJServentia     : String;
  vgSrvn_CodMunicipioServ  : Integer;
  vgSrvn_TituloServentia   : String;

  //LANCAMENTOS (GERAL)
  vgLanc_Codigo : Integer;
  vgLanc_Ano    : Integer;
  vgLanc_TpLanc : String;

  { Configuracoes }
  //Pessoas
  vgPrm_ConsPes : Boolean;
  vgPrm_IncPes  : Boolean;
  vgPrm_EdPes   : Boolean;
  vgPrm_ExcPes  : Boolean;

  { Importacoes }
  vgImport_Rec   : Boolean;
  vgImport_Fir   : Boolean;
  vgImport_FirA  : Boolean;
  vgImport_Prim  : Boolean;
  vgTextoMemo    : String;

  //CAIXA
  vgSituacaoCaixa : String;

  vgDataUltAb   : TDatetime;
  vgHoraUltAb   : TTime;
  vgDataProxAb  : TDatetime;
  vgVlrNotasAb  : Currency;
  vgVlrMoedasAb : Currency;
  vgVlrContaAb  : Currency;
  vgVlrTotalAb  : Currency;
  vgIdMovAb     : Integer;

  vgDataUltFech   : TDatetime;
  vgHoraUltFech   : TTime;
  vgDataProxFech  : TDatetime;
  vgVlrNotasFech  : Currency;
  vgVlrMoedasFech : Currency;
  vgVlrContaFech  : Currency;
  vgVlrTotalFech  : Currency;
  vgIdMovFech     : Integer;

  vgDataLancVigente : TDateTime;


  //IMPORTACOES
  vgContagemImportacao  : Integer;
  vgDataImportacaoAtual : TDateTime;

implementation

end.
