{-----------------------------------------------------------------------------------------------------------------------
  Unit Name:   UPrincipal.pas
  Descricao:   Tela Principal do Sistema
  Author   :   Cristina
  Date:        11-out-2016
  Last Update: 15-fev-2018 (Cristina)
------------------------------------------------------------------------------------------------------------------------}

unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Winapi.ShellApi, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls,
  Vcl.StdCtrls, JvExControls, JvButton, JvTransparentButton, Vcl.Menus,
  Winapi.TlHelp32, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, System.DateUtils;

const
  wm_IconMessage = wm_User + $001;

type
  TFPrincipal = class(TForm)
    pnlPrincipal: TPanel;
    pnlTitulo: TPanel;
    lblTitulo: TLabel;
    pnlConteudo: TPanel;
    pnlBotoes: TPanel;
    btnMinimizar: TJvTransparentButton;
    btnSincronizar: TJvTransparentButton;
    pmPopMenu: TPopupMenu;
    pmFechar: TMenuItem;
    tTimer1: TTimer;
    tTimer2: TTimer;
    lblVersaoSentinela: TLabel;
    lblSituacao: TLabel;
    mmProcessos: TMemo;
    pmSincronizarTudo: TMenuItem;
    pmSincronizarFirmas: TMenuItem;
    pmSincronizarRecibo: TMenuItem;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure btnMinimizarClick(Sender: TObject);
    procedure btnSincronizarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure pmRestaurarClick(Sender: TObject);
    procedure pmFecharClick(Sender: TObject);
    procedure tTimer1Timer(Sender: TObject);
    procedure tTimer2Timer(Sender: TObject);
    procedure pmSincronizarTudoClick(Sender: TObject);
    procedure pmSincronizarFirmasClick(Sender: TObject);
    procedure pmSincronizarReciboClick(Sender: TObject);
  private
    { Private declarations }

    lForcado: Boolean;

    procedure CriarTrayIcon;
    procedure IconTray(var Msg: TMessage); message wm_IconMessage;
    procedure SincronizarTudo(REC, FIR: Boolean);
  public
    { Public declarations }

    NID: TNotifyIconData;
  end;

var
  FPrincipal: TFPrincipal;

implementation

{$R *.dfm}

uses UGDM, UBibliotecaSistema, UDM, UVariaveisGlobais, UDMRecibo, UDMFirmas,
  USolicitaData, URecibo;

procedure TFPrincipal.btnMinimizarClick(Sender: TObject);
begin
  Hide;
end;

procedure TFPrincipal.btnSincronizarClick(Sender: TObject);
begin
  SincronizarTudo(True, True);
end;

procedure TFPrincipal.CriarTrayIcon;
begin
  //Carrega o icone inicial
  Icon.Handle := LoadIcon(HInstance, 'MAINICON');

  //Preenche os dados da estrutura NotifyIcon
  NID.cbSize := SizeOf(NID);
  NID.Wnd := Handle;
  NID.uID := 10; // Identificador do icone
  NID.uCallbackMessage := wm_IconMessage;
  NID.hIcon := Icon.Handle;

  NID.uFlags := NIF_MESSAGE or NIF_ICON or NIF_TIP;
  Shell_NotifyIcon(NIM_ADD, @NID);
end;

procedure TFPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  NID.uFlags := 0;
  Winapi.ShellApi.Shell_NotifyIcon(NIM_DELETE, @NID);
end;

procedure TFPrincipal.FormCreate(Sender: TObject);
var
  QryDtImp: TFDQuery;
  iCount: Integer;
begin
  lForcado := False;

  CriarTrayIcon;

  vgContagemImportacao  := 0;
  vgDataImportacaoAtual := Date;

  tTimer2.Interval := (vgConf_IntervaloSync * (60 * 1000));  //(minutos * (segundos * milissegundos))

  vgImport_Prim := False;
  iCount := 0;

  tTimer1.Enabled := False;

  if Trim(vgConf_VersaoSync) <> '' then
    lblVersaoSentinela.Caption := 'Vers�o: ' + vgConf_VersaoSync  //Versao do sentinela SEMPRE aqui
  else
    lblVersaoSentinela.Caption := '';

  if vgConf_DataIniImportacao = 0 then
  begin
    vgImport_Prim := True;

    repeat
      try
        Application.CreateForm(TFSolicitaData, FSolicitaData);

        FSolicitaData.sCaptionLabel := 'Por favor, informe a Data Inicial da Importa��o:';

        FSolicitaData.ShowModal;
      finally
        vgConf_DataIniImportacao := FSolicitaData.dDataSolicitada;
        FSolicitaData.Free;
      end;

      Inc(iCount);
    until (iCount = 2) or (vgConf_DataIniImportacao > 0);

    if vgConf_DataIniImportacao > 0 then
    begin
      try
        if vgConSISTEMA.Connected then
          vgConSISTEMA.StartTransaction;

        QryDtImp := dmGerencial.CriarFDQuery(nil, vgConSISTEMA);

        QryDtImp.Close;
        QryDtImp.SQL.Clear;
        QryDtImp.SQL.Text := 'UPDATE CONFIGURACAO SET DATA_INI_IMPORTACAO = :DATA_INI_IMPORTACAO';
        QryDtImp.Params.ParamByName('DATA_INI_IMPORTACAO').Value := vgConf_DataIniImportacao;
        QryDtImp.ExecSQL;

        FreeAndNil(QryDtImp);

        if vgConSISTEMA.InTransaction then
          vgConSISTEMA.Commit;
      except
        on E: Exception do
        begin
          if vgConSISTEMA.InTransaction then
            vgConSISTEMA.Rollback;

          Application.Terminate;
        end;
      end;
    end
    else
      Application.Terminate;
  end;

  pmSincronizarFirmas.Visible := vgImport_Fir or vgImport_FirA;
  pmSincronizarRecibo.Visible := vgImport_Rec;

  tTimer1.Enabled := True;
end;

procedure TFPrincipal.FormHide(Sender: TObject);
begin
//  pmRestaurar.Enabled := True;
end;

procedure TFPrincipal.IconTray(var Msg: TMessage);
var
  Pt: TPoint;
begin
  if Msg.lParam = WM_RBUTTONDOWN then
  begin
    GetCursorPos(Pt);
    pmPopMenu.Popup(Pt.x, Pt.y);
  end;

{  if Msg.lParam = WM_LBUTTONDBLCLK then
    if pmRestaurar.Enabled then
      pmRestaurarClick(pmRestaurar);  }
end;

procedure TFPrincipal.pmFecharClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TFPrincipal.pmSincronizarFirmasClick(Sender: TObject);
begin
  lForcado := True;
  SincronizarTudo(False, True);
end;

procedure TFPrincipal.pmSincronizarReciboClick(Sender: TObject);
begin
  lForcado := True;
  SincronizarTudo(True, False);
end;

procedure TFPrincipal.pmSincronizarTudoClick(Sender: TObject);
begin
  lForcado := True;
  SincronizarTudo(True, True);
end;

procedure TFPrincipal.pmRestaurarClick(Sender: TObject);
begin
//  Show;
end;

procedure TFPrincipal.SincronizarTudo(REC, FIR: Boolean);
var
  QryAb, QryFech, QryDest: TFDQuery;
  dDataFechPrev: TDate;
  dDataHoraFechPrev: TDateTime;
  lEnviarEmail: Boolean;
  IdAbertura: Integer;
 sMsgEmail: String;
//  AtualizaTabelasReciboMonitoramento: TReciboThread;
begin
  lEnviarEmail := False;
  IdAbertura   := 0;
  sMsgEmail    := '';

  if vgConf_DataIniImportacao = 0 then
    Exit;

  if vgDataImportacaoAtual < Date then
  begin
    vgContagemImportacao  := 1;
    vgDataImportacaoAtual := Date;
  end
  else
    Inc(vgContagemImportacao);

  dmPrincipal.VerificarSituacaoCaixa;

  if vgSituacaoCaixa = 'F' then
    Exit;

  vgTextoMemo  := '';

  pmSincronizarTudo.Enabled   := False;
  pmSincronizarFirmas.Enabled := False and pmSincronizarFirmas.Visible;
  pmSincronizarRecibo.Enabled := False and pmSincronizarRecibo.Visible;

//  btnSincronizar.Enabled := False;

  { Tabelas do Sistema }
{  lblSituacao.Caption := 'Sincronizando, aguarde...';
  mmProcessos.Lines.Add('===============================================');
  mmProcessos.Lines.Add(' IN�CIO DA SINCRONIZA��O: ' + DateTimeToStr(Now));
  mmProcessos.Lines.Add('===============================================');

  pnlPrincipal.Enabled := False;
}

  if REC or
    FIR then
  begin
    //Gerencial
    if (vgContagemImportacao = 1) or
      (vgContagemImportacao mod 18 = 0) then
    begin
  //    lblSituacao.Caption := 'Sincronizando as tabelas gerais do GERENCIAL...';

  {    mmProcessos.Lines.Add('<<<<< TABELAS GERAIS DO GERENCIAL >>>>>');
      dmPrincipal.AtualizaTabelasGerencialMonitoramento;
      mmProcessos.Lines.Add('-----------------------------------------------');  }

      vgTextoMemo  := '<<<<< TABELAS GERAIS DO GERENCIAL >>>>>';
      dmPrincipal.AtualizaTabelasGerencialMonitoramento;
      vgTextoMemo  := vgTextoMemo + #13#10 + '-----------------------------------------------';
  //    mmProcessos.Lines.Add(vgTextoMemo);
    end;

    //Recibo
    if REC then
    begin
      if vgImport_Rec and
        ((vgContagemImportacao = 2) or
         (vgContagemImportacao mod 15 = 0)) and
        (not lForcado) then
      begin
        vgTextoMemo  := '';

    //    lblSituacao.Caption := 'Sincronizando tabelas de clientes do RECIBO...';
    //    mmProcessos.Lines.Add('<<<<< CLIENTES (RECIBO) >>>>>');

    {    AtualizaTabelasReciboMonitoramento := TReciboThread.Create;
        AtualizaTabelasReciboMonitoramento.FreeOnTerminate := True;
        AtualizaTabelasReciboMonitoramento.Priority := tpLower;  // set the priority to lower than normal
        AtualizaTabelasReciboMonitoramento.Start;
        mmProcessos.Lines.Add(vgTextoMemo);  }

    {    Application.CreateForm(TdmRecibo, dmRecibo);
        dmRecibo.AtualizaTabelasReciboMonitoramento;
        FreeAndNil(dmRecibo);

        mmProcessos.Lines.Add('-----------------------------------------------');  }

        vgTextoMemo  := '<<<<< CLIENTES (RECIBO) >>>>>';
        Application.CreateForm(TdmRecibo, dmRecibo);
        dmRecibo.AtualizaTabelasReciboMonitoramento;
        FreeAndNil(dmRecibo);
        vgTextoMemo  := vgTextoMemo + #13#10 + '-----------------------------------------------';
    //    mmProcessos.Lines.Add(vgTextoMemo);
      end;
    end;

    //Firmas
    if FIR then
    begin
      if (vgImport_Fir or vgImport_FirA) and
        ((vgContagemImportacao = 3) or
         (vgContagemImportacao mod 15 = 0)) and
        (not lForcado) then
      begin
        vgTextoMemo  := '';

    //    lblSituacao.Caption := 'Sincronizando tabelas de clientes de FIRMAS...';
    //    mmProcessos.Lines.Add('<<<<< CLIENTES (FIRMAS) >>>>>');

        vgTextoMemo  := '<<<<< CLIENTES (FIRMAS) >>>>>';

        Application.CreateForm(TdmFirmas, dmFirmas);

        if vgImport_Fir then
          dmFirmas.AtualizaTabelasFirmasMonitoramento;

        if vgImport_FirA then
          dmFirmas.AtualizaTabelasFirmasAntigoMonitoramento;

        FreeAndNil(dmFirmas);
    //    mmProcessos.Lines.Add('-----------------------------------------------');

        vgTextoMemo  := vgTextoMemo + #13#10 + '-----------------------------------------------';
    //    mmProcessos.Lines.Add(vgTextoMemo);
      end;
    end;
  end;

  { Recibos - Firmas - Firmas Antigo }
{  lblSituacao.Caption := 'Atualizando Lan�amentos (RECIBO e FIRMAS)...';
  mmProcessos.Lines.Add('<<<<< LAN�AMENTOS (RECIBO E FIRMAS) >>>>>');
  dmPrincipal.SincronizarDadosSistemaGerencialMonitoramento(REC, FIR, FIRA);
  mmProcessos.Lines.Add('-----------------------------------------------');  }

//  lblSituacao.Caption := 'Atualizando Lan�amentos (RECIBO e FIRMAS)...';
  vgTextoMemo  := '';
  vgTextoMemo  := '<<<<< LAN�AMENTOS (RECIBO E FIRMAS) >>>>>';
  dmPrincipal.SincronizarDadosSistemaGerencialMonitoramento(REC, FIR);
  vgTextoMemo  := vgTextoMemo + #13#10 + '-----------------------------------------------';

  { Despesas Recorrentes }
{  lblSituacao.Caption := 'Atualizando Despesas Recorrentes...';
  mmProcessos.Lines.Add('<<<<< DESPESAS RECORRENTES >>>>>');
  dmPrincipal.AtualizarDespesasRecorrentes;  }

  //Envio de e-mail par quando o Caixa nao foi fechado
  dmPrincipal.qryConfiguracao.Close;
  dmPrincipal.qryConfiguracao.Open;

  if Trim(dmPrincipal.qryConfiguracao.FieldByName('DIA_FECHAMENTOCAIXA').AsString) = 'A' then  //Fechamento de Caixa no mesmo dia
  begin
    dDataFechPrev     := vgDataUltAb;
    dDataHoraFechPrev := StrToDateTime(FormatDateTime('DD/MM/YYYY', vgDataUltAb) + ' ' + vgConf_HoraFechamentoPadrao);
  end
  else if Trim(dmPrincipal.qryConfiguracao.FieldByName('DIA_FECHAMENTOCAIXA').AsString) = 'P' then  //Fechamento de Caixa no dia seguinte
  begin
    dDataFechPrev     := dmGerencial.ProximoDiaUtil(vgDataUltAb);
    dDataHoraFechPrev := StrToDateTime(FormatDateTime('DD/MM/YYYY', dmGerencial.ProximoDiaUtil(vgDataUltAb)) + ' ' + vgConf_HoraFechamentoPadrao);
  end;

  if Now > dDataHoraFechPrev then
  begin
    QryAb   := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);
    QryFech := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

    QryAb.Close;
    QryAb.SQL.Clear;
    QryAb.SQL.Text := 'SELECT ID_CAIXA_ABERTURA, ' +
                      '       FLG_ATRASO_NOTIFICADO ' +
                      '  FROM CAIXA_ABERTURA ' +
                      ' WHERE DATA_ABERTURA = :DATA_ABERTURA ' +
                      '   AND FLG_ATRASO_NOTIFICADO = ' + QuotedStr('N') +
                      '   AND FLG_CANCELADO = ' + QuotedStr('N');
    QryAb.Params.ParamByName('DATA_ABERTURA').Value   := StrToDateTime(FormatDateTime('DD/MM/YYYY', vgDataUltAb));
    QryAb.Open;

    QryFech.Close;
    QryFech.SQL.Clear;
    QryFech.SQL.Text := 'SELECT ID_CAIXA_FECHAMENTO ' +
                        '  FROM CAIXA_FECHAMENTO ' +
                        ' WHERE DATA_FECHAMENTO = :DATA_FECHAMENTO ' +
                        '   AND FLG_CANCELADO = ' + QuotedStr('N');
    QryFech.Params.ParamByName('DATA_FECHAMENTO').Value := StrToDateTime(FormatDateTime('DD/MM/YYYY hh:mm:ss', dDataFechPrev));
    QryFech.Open;

    lEnviarEmail := ((Trim(QryAb.FieldByName('FLG_ATRASO_NOTIFICADO').AsString) = 'N') and
                     (QryFech.RecordCount = 0));

    if lEnviarEmail then
    begin
      IdAbertura := QryAb.FieldByName('ID_CAIXA_ABERTURA').AsInteger;

      QryDest := dmGerencial.CriarFDQuery(nil, vgConSISTEMAAux);

      QryDest.Close;
      QryDest.SQL.Clear;
      QryDest.SQL.Text := 'SELECT * ' +
                          '  FROM EMAIL_DESTINATARIO ' +
                          ' WHERE FLG_ATIVO = ' + QuotedStr('S') +
                          '   AND FLG_REL_FECHAMENTO = ' + QuotedStr('S');
      QryDest.Open;

      QryDest.First;

      while not QryDest.Eof do
      begin
        dmGerencial.EnviarEmail('Fechamento de Caixa',
                                QryDest.FieldByName('EMAIL_DESTINATARIO').AsString,
                                '',
                                '',
                                'O Fechamento de Caixa do dia ' + FormatDateTime('DD/MM/YYYY', dDataFechPrev) + ' n�o foi realizado dentro do per�odo estipulado.',
                                '',
                                dmPrincipal.cdsRemetente,
                                sMsgEmail);

        QryDest.Next;
      end;

      FreeAndNil(QryDest);

      QryAb.Close;
      QryAb.SQL.Clear;
      QryAb.SQL.Text := 'UPDATE CAIXA_ABERTURA ' +
                        '   SET FLG_ATRASO_NOTIFICADO = ' + QuotedStr('S') +
                        ' WHERE ID_CAIXA_ABERTURA = :ID_CAIXA_ABERTURA';
      QryAb.Params.ParamByName('ID_CAIXA_ABERTURA').Value := IdAbertura;
      QryAb.ExecSQL
    end;

    FreeAndNil(QryAb);
    FreeAndNil(QryFech);
  end;

{  pnlPrincipal.Enabled := True;

  lblSituacao.Caption := '...';

  mmProcessos.Lines.Add(vgTextoMemo);
  mmProcessos.Lines.Add('===============================================');
  mmProcessos.Lines.Add('   FIM DA SINCRONIZA��O: ' + DateTimeToStr(Now));
  mmProcessos.Lines.Add('===============================================');
}
  pmSincronizarTudo.Enabled   := True;
  pmSincronizarFirmas.Enabled := True and (vgImport_Fir or vgImport_FirA) and pmSincronizarFirmas.Visible;
  pmSincronizarRecibo.Enabled := True and vgImport_Rec and pmSincronizarRecibo.Visible;

//  btnSincronizar.Enabled := True;

  tTimer1.Enabled := False;
end;

procedure TFPrincipal.tTimer1Timer(Sender: TObject);
begin
  lForcado := False;

  SincronizarTudo(True, True);
end;

procedure TFPrincipal.tTimer2Timer(Sender: TObject);
var
  lMonitoramentoFechado: Boolean;
begin
  tTimer1.Enabled := True;

  { VERIFICA SE O SISTEMA MONITORAMENTO ESTA EM EXECUCAO }
  try
    lMonitoramentoFechado := not dmPrincipal.ProcessoSistemaAtivo('MONITORAMENTO.exe');
  except
    lMonitoramentoFechado := False;
  end;

  if lMonitoramentoFechado then
    Self.Close;
end;

end.
