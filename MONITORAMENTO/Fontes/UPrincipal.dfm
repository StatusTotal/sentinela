object FPrincipal: TFPrincipal
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'SYNC_MONITORAMENTO'
  ClientHeight = 309
  ClientWidth = 467
  Color = clGray
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnHide = FormHide
  PixelsPerInch = 96
  TextHeight = 14
  object pnlPrincipal: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 461
    Height = 303
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object lblSituacao: TLabel
      Left = 0
      Top = 39
      Width = 461
      Height = 16
      Align = alTop
      Alignment = taCenter
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 12
    end
    object pnlTitulo: TPanel
      Left = 0
      Top = 0
      Width = 461
      Height = 39
      Align = alTop
      BevelOuter = bvNone
      Color = clGray
      ParentBackground = False
      TabOrder = 0
      object lblTitulo: TLabel
        Left = 64
        Top = 7
        Width = 322
        Height = 21
        Alignment = taCenter
        Caption = 'SINCRONIZA'#199#195'O - MONITORAMENTO'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
      end
      object lblVersaoSentinela: TLabel
        Left = 392
        Top = 26
        Width = 64
        Height = 11
        Alignment = taRightJustify
        Caption = 'Vers'#227'o: 1.0.0.0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object pnlConteudo: TPanel
      Left = 0
      Top = 55
      Width = 461
      Height = 212
      Align = alClient
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 1
      object mmProcessos: TMemo
        Left = 0
        Top = 0
        Width = 461
        Height = 212
        Align = alClient
        BorderStyle = bsNone
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object pnlBotoes: TPanel
      Left = 0
      Top = 267
      Width = 461
      Height = 36
      Align = alBottom
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 2
      object btnMinimizar: TJvTransparentButton
        Left = 233
        Top = 6
        Width = 83
        Height = 26
        Hint = 'Clique para Minimizar a tela'
        Caption = '&Minimizar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGray
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Transparent = False
        OnClick = btnMinimizarClick
      end
      object btnSincronizar: TJvTransparentButton
        Left = 144
        Top = 6
        Width = 83
        Height = 26
        Hint = 'Clique para iniciar a Sincroniza'#231#227'o'
        Caption = '&Sincronizar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrackFont.Charset = DEFAULT_CHARSET
        HotTrackFont.Color = clWindowText
        HotTrackFont.Height = -12
        HotTrackFont.Name = 'Tahoma'
        HotTrackFont.Style = []
        FrameStyle = fsIndent
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        Transparent = False
        OnClick = btnSincronizarClick
      end
    end
  end
  object pmPopMenu: TPopupMenu
    Left = 392
    Top = 192
    object pmSincronizarTudo: TMenuItem
      Caption = 'Sincronizar &Tudo'
      OnClick = pmSincronizarTudoClick
    end
    object pmSincronizarFirmas: TMenuItem
      Caption = 'Sincronizar &Firmas'
      OnClick = pmSincronizarFirmasClick
    end
    object pmSincronizarRecibo: TMenuItem
      Caption = 'Sincronizar &Recibo'
      OnClick = pmSincronizarReciboClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object pmFechar: TMenuItem
      Caption = 'Fe&char...'
      OnClick = pmFecharClick
    end
  end
  object tTimer1: TTimer
    Interval = 10000
    OnTimer = tTimer1Timer
    Left = 200
    Top = 200
  end
  object tTimer2: TTimer
    Interval = 60000
    OnTimer = tTimer2Timer
    Left = 240
    Top = 200
  end
end
