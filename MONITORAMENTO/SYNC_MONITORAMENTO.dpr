program SYNC_MONITORAMENTO;

uses
  MidasLib,
  Vcl.Forms,
  UPrincipal in 'Fontes\UPrincipal.pas' {FPrincipal},
  UDM in 'Fontes\UDM.pas' {dmPrincipal: TDataModule},
  UGDM in '..\..\BIBLIOTECA\UGDM.pas' {dmGerencial: TDataModule},
  UBibliotecaSistema in 'Fontes\UBibliotecaSistema.pas',
  UVariaveisGlobais in 'Fontes\UVariaveisGlobais.pas',
  UFiltroSimplesPadrao in '..\..\BIBLIOTECA\UFiltroSimplesPadrao.pas' {FFiltroSimplesPadrao},
  UListaSimplesPadrao in '..\..\BIBLIOTECA\UListaSimplesPadrao.pas' {FListaSimplesPadrao},
  UDMPessoa in '..\..\BIBLIOTECA\UDMPessoa.pas' {dmPessoa: TDataModule},
  UFiltroPessoa in '..\..\BIBLIOTECA\UFiltroPessoa.pas' {FFiltroPessoa},
  UListaPessoas in '..\..\BIBLIOTECA\UListaPessoas.pas' {FListaPessoas},
  UCadastroGeralPadrao in '..\..\BIBLIOTECA\UCadastroGeralPadrao.pas' {FCadastroGeralPadrao},
  UCadastroPessoa in '..\..\BIBLIOTECA\UCadastroPessoa.pas' {FCadastroPessoa},
  UDMFirmas in '..\..\BIBLIOTECA\UDMFirmas.pas' {dmFirmas: TDataModule},
  UDMRecibo in '..\..\BIBLIOTECA\UDMRecibo.pas' {dmRecibo: TDataModule},
  USolicitaData in '..\..\BIBLIOTECA\USolicitaData.pas' {FSolicitaData},
  UProcessando in '..\..\BIBLIOTECA\UProcessando.pas' {FProcessando};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := 'SYNC_MONITORAMENTO';

  Application.MainFormOnTaskbar := False;
  Application.ShowMainForm := False;

  Application.CreateForm(TdmGerencial, dmGerencial);

  if vgLoginOk then
    Application.CreateForm(TFPrincipal, FPrincipal);

  Application.Run;
end.
